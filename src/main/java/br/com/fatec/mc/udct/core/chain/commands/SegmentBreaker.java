/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 04/08/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.geometry.StandardLine;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Command that breaks the segments. It's necessary to break a segment
 * in points where the line touches a circles or an ellipse. The break needs to
 * be done before trying to detect circles an ellipses do avoid data loss. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 04/08/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

//FIXME This class needs revision and some tests. There are two know problems
//1 - When the lines are parallel to axis x or y there are problems comparing the angles.
//There is also a problem considering the distance between the lines of different segments.

//2 - After the first processing, lines that should compose one segment can be separated.
//I.E: where a line touches a circle or a ellipse the segment of that forms will probably
//be split at two.
public class SegmentBreaker implements Command {
	private Logger logger = LoggerFactory.getLogger(SegmentBreaker.class);

	private final double MAX_ANGLE = 40;
	//private final double MAX_COORD_DISTANCE = 15;

	/** 
	 * Breaks the segments in places where exists the possibility of a line touching a
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		logger.info("Break the given segments in places where lines touches potential circles/ellipses");
		
		List<Line> lines = (List<Line>) context.get(UseCaseContext.LINES);
		List<List<Coordinate2D>> newSegments = new ArrayList<List<Coordinate2D>>();
		Map<Integer,List<Integer>> mapSegmentLine = new HashMap<Integer,List<Integer>>();
		int mapId = 0;
		int i = 0;
		int indexToNewSegment = i;
		while(i < lines.size()){
			for(;i < lines.size();i++){
				if(i + 1 < lines.size()){
					StandardLine firstLine = (StandardLine) lines.get(i); 
					StandardLine secondLine = (StandardLine) lines.get(i + 1); 

					double alfa = calculateAngle(firstLine, secondLine);					
					
					//Convert to degrees
					double angle = Math.abs(Math.toDegrees(Math.atan(alfa)));

					//Verifies if the angle between the lines are greater than the parameter. 
					//If affirmative, the angle between the lines represents that it should be broke here
					//creating separate segments.
					if(angle > MAX_ANGLE){
						List<Coordinate2D> newSegment = new ArrayList<Coordinate2D>();
						List<Integer> linesIndex = new ArrayList<Integer>();
						linesIndex.add(Integer.valueOf(indexToNewSegment));
						linesIndex.add(Integer.valueOf(i));

						for(;indexToNewSegment <= i ; indexToNewSegment++){
							newSegment.addAll(lines.get(indexToNewSegment).getLinePoints());
						}
						newSegments.add(newSegment);
						mapSegmentLine.put(mapId, linesIndex);
						mapId++;
						
					}
				}else{
					//It's the end of the lines list, so from the indexToNewSegment until the last
					//line there is a segment.
					if(indexToNewSegment == lines.size()){
						indexToNewSegment --;
					}
					List<Coordinate2D> newSegment = new ArrayList<Coordinate2D>();
					List<Integer> linesIndex = new ArrayList<Integer>();
					linesIndex.add(Integer.valueOf(indexToNewSegment));
					linesIndex.add(Integer.valueOf(lines.size() - 1));
					for(;indexToNewSegment < lines.size(); indexToNewSegment++){
						newSegment.addAll(lines.get(indexToNewSegment).getLinePoints());
					}
					newSegments.add(newSegment);
					mapSegmentLine.put(mapId++, linesIndex);
					mapId++;
				}
			}
		}
		
		//It's still necessary to do a secondary check, because two segments can complement themselves.
		//I.E: In a point where a circle/ellipse is touched by a line the full segment that represent
		//these forms will probably be split into two.
//		List<List<Coordinate2D>> finalSegments = new ArrayList<List<Coordinate2D>>();
//		mapId = 0;
//		for(int j = 0 ; j < newSegments.size(); j++){
//			List<Integer> linesIndex = mapSegmentLine.get(j);
//			StandardLine line1 = (StandardLine) lines.get(linesIndex.get(1));
//			List<Coordinate2D> newSegment = new ArrayList<Coordinate2D>(newSegments.get(j));
//			
//			for(int k = j + 1 ; k < newSegments.size(); k++){
//				List<Integer> secondLineIndex = mapSegmentLine.get(k);
//				StandardLine line2 = (StandardLine) lines.get(secondLineIndex.get(0));			
//				double alfa = calculateAngle(line1, line2);					
//				double angle = Math.toDegrees(Math.atan(alfa));
//				//Verifies the angle between the last line of the first segment and
//				//the first line of the second segment.
//				if(angle < MAX_ANGLE){
//					//Still needs to check the distance between the points to a fixed
//					//parameter that defines the lines are close enough. 
//					Coordinate2D c1 = line1.getLinePoints().get(line1.getLinePoints().size() - 1);
//					Coordinate2D c2 = line2.getLinePoints().get(0);
//					System.out.println(c1.distance2Coordinate(c2));
//					if(c1.distance2Coordinate(c2) <= MAX_COORD_DISTANCE){
//						newSegment.addAll(newSegments.get(k));
//					}
//				}
//			}
//			finalSegments.add(newSegment);
//
// 		}
		
		context.put(UseCaseContext.SEGMENTS, newSegments);
	
		return false;
	}
	
	/** 
	 * Calculate the angle between the two lines
	 * @param firstLine
	 * @param secondLine
	 * @return
	 */
	private double calculateAngle(StandardLine firstLine, StandardLine secondLine){
		double alfa = 0;

		//Find the appropriate way to calculate the angle
		if(firstLine.getAngularCoefficient().equals(Double.NaN) 
				&& !secondLine.getAngularCoefficient().equals(Double.NaN)){
			alfa = 1/secondLine.getAngularCoefficient();
		}else if(!firstLine.getAngularCoefficient().equals(Double.NaN) 
				&& secondLine.getAngularCoefficient().equals(Double.NaN)){
			alfa = 1/firstLine.getAngularCoefficient();						
		}else if (!firstLine.getAngularCoefficient().equals(Double.NaN) 
				&& !secondLine.getAngularCoefficient().equals(Double.NaN)){
			alfa = Math.abs((firstLine.getAngularCoefficient() - secondLine.getAngularCoefficient()) 
					/ (1 + (firstLine.getAngularCoefficient() * secondLine.getAngularCoefficient())));
		}else{
			Coordinate2D line1Start = firstLine.getLinePoints().get(0);
			Coordinate2D line1End = firstLine.getLinePoints().get(firstLine.getLinePoints().size() - 1);
			Coordinate2D line2Start = secondLine.getLinePoints().get(0);
			Coordinate2D line2End = secondLine.getLinePoints().get(secondLine.getLinePoints().size() - 1);
			
			double angle1 = Math.atan2(line1Start.getY() - line1End.getY(),
					line1Start.getX() - line1End.getX());
			double angle2 = Math.atan2(line2Start.getY() - line2End.getY(),
					line2Start.getX() - line2End.getX());
			alfa = angle1-angle2;
		}
		
		return alfa;
	}
}
