/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy.context;

import java.util.List;
import java.util.Map;

import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.strategy.IEllipseDetectionStrategy;
import br.com.fatec.mc.udct.domain.geometry.GeometryEntity;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Represents the context to execute ellipse detection strategies. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class EllipseDetectionContext {
	
	private IEllipseDetectionStrategy strategy;
	private Image image;
	private List<List<? extends GeometryEntity>> imageData;
	
	private Map<String,List<Geometry>> result;
	
	/** 
	 * Execute the detection
	 */
	public void executeDetection(){
		if(image != null)
			strategy.detectEllipses(image);
		else if(imageData != null)
			result = strategy.detectedEllipsesAndCircles(imageData);
	}
	
	/** 
	 * Class constructor
	 * @param strategy {@link IEllipseDetectionStrategy}: Ellipse Detection concrete strategy
	 */
	public EllipseDetectionContext(IEllipseDetectionStrategy strategy){
		this.strategy = strategy;
	}
	
	/**
	 * Sets the {@link Image} of the context
	 * @param image {@link Image}
	 */
	public void setImage(Image image) {
		this.image = image;
	}
	
	/**
	 * Setter method of imageData
	 *
	 * @param imageData Class attribute
	 */
	public void setImageData(List<List<? extends GeometryEntity>> imageData) {
		this.imageData = imageData;
	}
	
	/**
	 * Getter method of result
	 *
	 * @return result
	 */
	public Map<String, List<Geometry>> getResult() {
		return result;
	}
}
