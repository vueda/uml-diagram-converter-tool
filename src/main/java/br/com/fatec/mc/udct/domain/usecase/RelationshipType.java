/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.usecase;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Enum that represents the types of relationships. <br>
 * The types are: ASSOCIATION, GENERALIZATION, INCLUDE and EXTEND.<br>
 * <br>
 * CLASS VERSIONS: <br>
 * 28/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public enum RelationshipType {
	ASSOCIATION,
	GENERALIZATION,
	INCLUDE,
	EXTEND;
}
