/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 20/08/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.apache.commons.math3.stat.regression.RegressionResults;
import org.apache.commons.math3.stat.regression.SimpleRegression;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.geometry.StandardLine;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Command that validates the detected lines checking for duplicated
 * lines.<br>
 * <br>
 * CLASS VERSIONS: <br>
 * 20/08/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class RepeatedLinesValidator implements Command {

	private final double MAX_ANGLE = 45;
	private final double MAX_DISTANCE = 40;
	
	/** 
	 * Validates the detected lines removing the duplicates ones and merging into one line.
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		List<Line> lines = (List<Line>) context.get(UseCaseContext.LINES);
		List<Line> mergedLines = new ArrayList<Line>();
		StandardLine ln = null;
		Coordinate2D e1 = null;
		for(int i = 0 ; i < lines.size() - 1; i++){
			ln = (StandardLine) lines.get(i);
			e1 = ln.getLinePoints().get(ln.getLinePoints().size()-1);
			List<Coordinate2D> segment = new ArrayList<Coordinate2D>();
			segment.addAll(ln.getLinePoints());
			for(int j = i + 1; j <lines.size() ; j++){
				StandardLine ln2 = (StandardLine) lines.get(j);
				Coordinate2D s2 = ln2.getLinePoints().get(0);
				double alfa = calculateAngle(ln, ln2);		
				double angle = Math.abs(Math.toDegrees(Math.atan(alfa)));
				double distance = e1.distance2Coordinate(s2);		
				if(angle < MAX_ANGLE && distance < MAX_DISTANCE){
					segment.addAll(ln2.getLinePoints());
					Line tempLine = calculateLine(segment);
					ln = (StandardLine) tempLine;
					e1 = tempLine.getLinePoints().get(tempLine.getLinePoints().size()-1);
					lines.remove(j);
					j--;
				}
			}	
			Line newLine = calculateLine(segment);
			mergedLines.add(newLine);	
		}
		context.put(UseCaseContext.LINES, mergedLines);
		return false;
	}
	
	private Line calculateLine(List<Coordinate2D> segment){
		SimpleRegression reg = new SimpleRegression();
		for(Coordinate2D c:segment){
			reg.addData(c.getX(), c.getY());
		}
		RegressionResults res = reg.regress();
		StandardLine newLine = new StandardLine();
		newLine.setLinearCoefficient(res.getParameterEstimate(0));
		newLine.setAngularCoefficient(res.getParameterEstimate(1));	
		newLine.setLinePoints(segment);
		return newLine;
	}

	private double calculateAngle(StandardLine firstLine, StandardLine secondLine){
		double alfa = 0;

		//Find the appropriate way to calculate the angle
		if(firstLine.getAngularCoefficient().equals(Double.NaN) 
				&& !secondLine.getAngularCoefficient().equals(Double.NaN)){
			alfa = 1/secondLine.getAngularCoefficient();
		}else if(!firstLine.getAngularCoefficient().equals(Double.NaN) 
				&& secondLine.getAngularCoefficient().equals(Double.NaN)){
			alfa = 1/firstLine.getAngularCoefficient();						
		}else if (!firstLine.getAngularCoefficient().equals(Double.NaN) 
				&& !secondLine.getAngularCoefficient().equals(Double.NaN)){
			alfa = Math.abs((firstLine.getAngularCoefficient() - secondLine.getAngularCoefficient()) 
					/ (1 + (firstLine.getAngularCoefficient() * secondLine.getAngularCoefficient())));
		}else{
			Coordinate2D line1Start = firstLine.getLinePoints().get(0);
			Coordinate2D line1End = firstLine.getLinePoints().get(firstLine.getLinePoints().size() - 1);
			Coordinate2D line2Start = secondLine.getLinePoints().get(0);
			Coordinate2D line2End = secondLine.getLinePoints().get(secondLine.getLinePoints().size() - 1);

			double angle1 = Math.atan2(line1Start.getY() - line1End.getY(),
					line1Start.getX() - line1End.getX());
			double angle2 = Math.atan2(line2Start.getY() - line2End.getY(),
					line2Start.getX() - line2End.getX());
			alfa = angle1-angle2;
		}

		return alfa;
	}

}
