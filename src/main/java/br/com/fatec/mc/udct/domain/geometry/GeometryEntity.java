/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 02/06/2013<br>
 */

package br.com.fatec.mc.udct.domain.geometry;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Abstract class that represents every geometry entity related. <br>
 * Must be extended by all geometry entities.<br> 
 * <br>
 * CLASS VERSIONS: <br>
 * 02/06/2013 - @author Vinícius Oliveira Ueda<br>
 *<br>
 *<br>
 */

public abstract class GeometryEntity {
	private Long id;

	/** 
	 * Unique identification of the geometry form
	 * @return
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
