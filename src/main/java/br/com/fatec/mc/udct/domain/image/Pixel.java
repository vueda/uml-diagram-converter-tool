/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.image;

import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Domain class that represents each pixel that composes an image. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 28/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class Pixel extends Coordinate2D implements Comparable<Pixel>{
	
	private Integer gradient;

	/** 
	 * Class constructor
	 */
	public Pixel(){}
	
	/** 
	 * Class constructor
	 * @param x {@link Integer}: The x coordinate
	 * @param y {@link Integer}: The y coordinate
	 * @param gradient {@link Integer}: Gradient value of the pixel
	 */
	public Pixel(Integer x, Integer y, Integer gradient){
		setX(x);
		setY(y);
		this.gradient = gradient;
	}
	
	/** 
	 * The gradient value of this pixel in a grayscale.
	 * @return {@link Double}
	 */
	public Integer getGradient() {
		return gradient;
	}

	public void setGradient(Integer gradient) {
		this.gradient = gradient;
	}

	/** 
	 * Compare two pixel gradient values
	 * @param px {@link Pixel}: Pixel to be compared
	 * @return the value 0 if this Integer is equal to the argument Integer; 
	 * a value less than 0 if this Integer is numerically less than the argument Integer; 
	 * and a value greater than 0 if this Integer is numerically greater than the argument Integer (signed comparison).
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Pixel px) {
		return this.gradient.compareTo(px.getGradient());
	}
	
	
}
