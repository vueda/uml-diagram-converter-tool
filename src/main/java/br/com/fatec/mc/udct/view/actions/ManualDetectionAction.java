/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 30/05/2013<br>
 */

package br.com.fatec.mc.udct.view.actions;

import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.exceptions.DiagramParsingException;
import br.com.fatec.mc.udct.core.exceptions.XMIParsingException;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.domain.usecase.UseCaseDiagram;
import br.com.fatec.mc.udct.utils.preferences.Preferences;
import br.com.fatec.mc.udct.utils.preferences.PreferencesManager;
import br.com.fatec.mc.udct.view.ApplicationMain;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Action to manual use case detection. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 30/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

@SuppressWarnings("serial")
public class ManualDetectionAction extends AbstractAction {

	/**
	 * Logger
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * The directory last used to open a file
	 */
	private File fileDirectory = new File(PreferencesManager.getPreferences().get(Preferences.DIRECTORY, ""));

	/** 
	 * Action to manual use case detection.
	 * @param event {@link ActionEvent}
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent event){
		JFileChooser fileChooser = new JFileChooser(fileDirectory);
		fileChooser.setFileFilter(new FileNameExtensionFilter("Image files", "bmp", "png", "jpg"));  
		fileChooser.setAcceptAllFileFilterUsed(false); 
		fileChooser.setMultiSelectionEnabled(false);
		if(fileChooser.showOpenDialog(fileChooser) == JFileChooser.APPROVE_OPTION){
			BufferedImage selectedImage = null;
			try {
				File loadedFile = fileChooser.getSelectedFile();
				
				logger.info(loadedFile.getAbsolutePath());
				selectedImage =  ImageIO.read(loadedFile);
				Image image = new Image(fileChooser.getSelectedFile().getName(), selectedImage);
				PreferencesManager.getPreferences().put(Preferences.DIRECTORY, fileChooser.getCurrentDirectory().toString());
				UseCaseDiagram diagram = ApplicationMain.getInstance().getFacade().manualDetection(image);		
				ApplicationMain.getInstance().addImage(diagram.getImage());
				ApplicationMain.getInstance().getFacade().generateXMIFile(diagram);

				StringBuilder message = new StringBuilder();
				message.append(ApplicationMain.getInstance().getMessages().getString("app.success"));
				JOptionPane.showMessageDialog(fileChooser,message, ApplicationMain.getInstance().getMessages().getString("app.success.title"),
						JOptionPane.INFORMATION_MESSAGE);
			}catch(DiagramParsingException dpe){
				StringBuilder message = new StringBuilder();
				message.append(ApplicationMain.getInstance().getMessages().getString("app.error.diagram.parsing"));
				JOptionPane.showMessageDialog(fileChooser,message, ApplicationMain.getInstance().getMessages().getString("app.error.diagram.title"),
						JOptionPane.ERROR_MESSAGE);
			}catch (XMIParsingException xpe) {
				StringBuilder message = new StringBuilder();
				message.append(ApplicationMain.getInstance().getMessages().getString("app.error.xmi.parsing"));
				JOptionPane.showMessageDialog(fileChooser,message, ApplicationMain.getInstance().getMessages().getString("app.error.xmi.title"),
						JOptionPane.ERROR_MESSAGE);
			}
			catch (IOException ioEx){
				StringBuilder message = new StringBuilder();
				message.append(ApplicationMain.getInstance().getMessages().getString("app.image.load.error.message.one"));
				message.append(" ");
				message.append(fileChooser.getSelectedFile().getAbsolutePath());
				message.append(" ");
				message.append(ApplicationMain.getInstance().getMessages().getString("app.image.load.error.message.two"));
				JOptionPane.showMessageDialog(fileChooser,message, ApplicationMain.getInstance().getMessages().getString("app.image.load.error"),
						JOptionPane.ERROR_MESSAGE);
				logger.error(ioEx.getMessage());	
			}catch (Throwable ex) {//Utilized Throwable because of errors in third parties
				StringBuilder message = new StringBuilder();
				message.append(ApplicationMain.getInstance().getMessages().getString("app.error.try.log"));
				JOptionPane.showMessageDialog(fileChooser,message, ApplicationMain.getInstance().getMessages().getString("app.image.load.error"),
						JOptionPane.ERROR_MESSAGE);
				logger.error(ex.getMessage());		
			}
		}
	}

}
