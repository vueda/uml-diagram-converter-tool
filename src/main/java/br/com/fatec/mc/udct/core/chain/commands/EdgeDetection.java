/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.List;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.core.strategy.IEdgeDetectionStrategy;
import br.com.fatec.mc.udct.core.strategy.context.EdgeDetectionContext;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Command that detect the edges on the image. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda<br>
 *<br>
 *<br>
 */

public class EdgeDetection implements Command {
	private Logger logger = LoggerFactory.getLogger(EdgeDetection.class);
	
	private EdgeDetectionContext edContext;
		
	/** 
	 * Detect the edges on the image using an edge detection strategy
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {		
		Class<IEdgeDetectionStrategy> strategy = (Class<IEdgeDetectionStrategy>) context.get(UseCaseContext.EDGE_DETECTION_STRATEGY);
		edContext =  new EdgeDetectionContext(strategy.newInstance());
		Integer detailLevel = (Integer) context.get(UseCaseContext.DETAIL_LEVEL);
		edContext.setDetailLevel(detailLevel);
		Image image = (Image) context.get(UseCaseContext.IMAGE);
		edContext.setImage(image);
		
		logger.info("Detecting edges on the image using the " + strategy.getSimpleName() + " strategy, with detail level of " + detailLevel +".");

		List<List<Coordinate2D>> segments = edContext.executeDetection();

		if(segments.isEmpty() || segments == null){
			logger.error("There are no segments in the given image.");
		}
		context.put(UseCaseContext.SEGMENTS, segments);
		return false;
	}

}
