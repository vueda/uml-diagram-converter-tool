/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.utils.image;

import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Utility class to normalize the image matrix. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class NormalizationUtils {
	
	
	/** 
	 * Normalize the given image based on the data of pixels
	 * @param pixels - Values of image pixels 
	 * @param image - Image
	 */
	public static void normalizing(int[][] pixels, Image image) {
		int maxValue = Integer.MIN_VALUE;
		int minValue = Integer.MAX_VALUE;
		
		int width = pixels.length;
		int height = pixels[0].length;

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				maxValue = maxValue < pixels[x][y] ? pixels[x][y] : maxValue;
				minValue = minValue > pixels[x][y] ? pixels[x][y] : minValue;
			}
		}
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				int pixel = (int) (255 * (pixels[x][y] - minValue))
						/ (maxValue - minValue);

				image.setRGB(x, y, pixel, pixel, pixel);
			}
		}
	}

}
