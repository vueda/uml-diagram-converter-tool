/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Geometry;

import br.com.fatec.mc.udct.core.strategy.IEllipseDetectionStrategy;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.Ellipse;
import br.com.fatec.mc.udct.domain.geometry.GeometryEntity;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.image.ImageConverterUtils;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Strategy of ellipses detection proposed by Xie and Ji (2001) based on the Hough Transform.<br>
 * Implementation based on: <i>http://coding-experiments.blogspot.com.br/2011/05/ellipse-detection-in-image-by-using.html</i><br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */
@Deprecated
public class XieJiEllipseDetectionStrategy implements IEllipseDetectionStrategy{

	Logger logger = LoggerFactory.getLogger(XieJiEllipseDetectionStrategy.class);
	
	//Ellipse detection strategy parameters
	int MIN_MAJOR_AXIS;					//Min. size of major axis
	int MAX_MAJOR_AXIS;					//Max. size of major axis
	int MIN_MINOR_AXIS;					//Min. size of minor axis
	int MAX_MINOR_AXIS;					//Max. size of minor axis
	int MIN_FREQUENCY;					//Number of votes to be a possible ellipse
	int VALID_POINTS_DISTANCE; 			//Tolerance distance between detected ellipse points and image points 
	float PATH_PIXELS;					//Min. interest pixels to be considered as an ellipse 
	float ACCURACY_DETECTION; 			//Precision percentage
	float MAX_ANGLE;					//Max ellipse inclination angle	
	
	/** 
	 * Detect ellipses using the XieJi strategy
	 * @param diagramImage {@link Image}:Image where the ellipses will be searched
	 * @return {@link List} of {@link Ellipse}: Detected ellipses
	 * @see br.com.fatec.mc.udct.core.strategy.IEllipseDetectionStrategy#detectEllipses(br.com.fatec.mc.udct.domain.image.Image)
	 */
	public List<Ellipse> detectEllipses(Image diagramImage) {
		long startTime = System.currentTimeMillis();//Start time in milliseconds
		loadParametersConfiguration();
		//Extract the interest pixels from image	
		List<Coordinate2D> edgePixels = ImageConverterUtils.image2CoordinateWhiteEdge(diagramImage);
		//List of detected ellipses 
		List<Ellipse> detectedEllipses = new ArrayList<Ellipse>();
		//List that stores all the interest points for the verification of min. necessaries pixels
		//to be considered a valid ellipse
		List<Coordinate2D> pointsOfInterest = new ArrayList<Coordinate2D>();
		//Accumulator
		Double []accumulatorArray = new Double [MAX_MINOR_AXIS];
	
		for(int i = 0 ; i < edgePixels.size(); i++){
			Coordinate2D c1 = edgePixels.get(i);
			for(int j = 0 ; j < edgePixels.size() ; ++j){
				Coordinate2D c2 = edgePixels.get(j);
				//Verifies if the distance between the selected point is equals or major than the min
				//necessary size of major axis and less than the max size
				double distance = c1.distance2Coordinate(c2);
				if(distance >= MIN_MAJOR_AXIS && distance < MAX_MAJOR_AXIS){
					accumulatorArray = clearAccumulator(accumulatorArray);//Clear the accumulator
					//Ellipse center
					int centerX = ((c1.getX() + c2.getX()) /2);
					int centerY = ((c1.getY() + c2.getY()) /2);
					Coordinate2D centerCoordinate = new Coordinate2D(centerX, centerY);
					//Half of ellipse major axis
					double halfMajorAxis = c1.distance2Coordinate(c2) / 2;
					//Ellipse orientation
					double alfa = Math.atan2(c2.getY() - c1.getY(),c2.getX() - c1.getX());
					for(int k = 0; k < edgePixels.size() ; ++k){
						Coordinate2D c3 = edgePixels.get(k);
						double d = c3.distance2Coordinate(centerCoordinate);
						
						//Verifies if the distance between the third point and the center of the candidate ellipse
						//is greater than the half of the min axis parameter and less than the half of calculated major axis
						if(d >= MIN_MINOR_AXIS /2 && d < halfMajorAxis){
							//Distance between the third point and the extremity of major axis
							double f = c3.distance2Coordinate(c2);
							double cost = ((Math.pow(halfMajorAxis,2) + Math.pow(d,2) - Math.pow(f, 2)) / (2 * halfMajorAxis * d)); 
							//The half of ellipse minor axis
							double halfMinorAxis = Math.sqrt(nonNegative(
									(Math.pow(halfMajorAxis,2) * Math.pow(d,2) * (1 - Math.pow(cost,2))) /
									(Math.pow(halfMajorAxis,2) - Math.pow(d,2) * Math.pow(cost,2))));
							halfMinorAxis = (int)halfMinorAxis;
							//Increments the accumulator in +1 for the min axis ellipse size 
							if(halfMinorAxis < MAX_MINOR_AXIS)
								accumulatorArray[(int)halfMinorAxis] +=1;
						}
					}//Third pixel
					
					double minorAxis = Double.MIN_VALUE;
					//Recovers the min. axis size with most votes
					double maxFrequency = Double.MIN_VALUE;
					for(int m = 0 ; m < accumulatorArray.length ; ++m){
						if(accumulatorArray[m] > maxFrequency){
							maxFrequency = accumulatorArray[m];
							minorAxis = m;
						}
					}
					
					//Verifies:
					//1 - If the minor axis is greater or equals to the min axis parameter
					//2 - If the quantity of votes is greater than the min. necessary votes
					//3 - If the ellipse orientation is lower than the parameter defined
					if(minorAxis * 2 >= MIN_MINOR_AXIS && maxFrequency >= MIN_FREQUENCY && Math.abs(((180/Math.PI) * alfa)) < MAX_ANGLE){
						//Creates a new ellipse
						Ellipse ellipse = new Ellipse();
						ellipse.setCenter(centerCoordinate);
						ellipse.setMinorAxis(minorAxis * 2);
						ellipse.setMajorAxis(halfMajorAxis * 2);
						ellipse.setOrientation(alfa);
						float supportRatio = (float) 0.0; //Precision rate
						//PATH_PIXELS = (float) (2 *(ellipse.getMajorAxis() + ellipse.getMinorAxis()));

						//Recovers the points tha composes the ellipse. The quantity points are equals to PATH_PIXELS
						List<Coordinate2D> listEllipsesPoints = calculateEllipsesPoints(ellipse);
					
						//Verifies if the distance between the calculated ellipse points and the image points
						//are in the tolerance distance
						for(int n = 0 ; n < listEllipsesPoints.size() ; ++n){
							Coordinate2D p1 = listEllipsesPoints.get(n);
							boolean added = false;
							for(int o = 0 ; o < edgePixels.size() ; ++o){
								Coordinate2D p2 = edgePixels.get(o);
								if(p1.distance2Coordinate(p2) <= VALID_POINTS_DISTANCE){
									pointsOfInterest.add(p2);
									if(!added){
										//Calculates the precision
										supportRatio += 1.0/PATH_PIXELS;
										added = true;
									}
								}
							}
						}				
						//Verifies the precision
						if(supportRatio > ACCURACY_DETECTION){		
							//Remove the points of the detected ellipse from edgePixels
							for(Coordinate2D p3:pointsOfInterest){
								edgePixels.remove(p3);
							}
							//Clear the interest points list
							pointsOfInterest.clear();
							//Adds the ellipse 
							detectedEllipses.add(ellipse);
							logger.info("ELLIPSE ADDED!");
						}
						else{
							//If the detected ellipse doesn't have the min precision the interest points are cleared
							pointsOfInterest.clear();
						}
						
					}
					
				}
			}//Second Pixel
		}//First pixel
		//Logs information about the detection
		logger.info("Processing Time: {} minutes", (new SimpleDateFormat("mm:ss").format(new Date(System.currentTimeMillis() - startTime))));
		logger.info("Detected Ellipses: " + detectedEllipses.size());
		return detectedEllipses;
	}
	
	/** 
	 * Clear the accumulator
	 * @param accumulatorArray {@link Double}[]: The accumulator array to be cleared 
	 * @return {@link Double}[] : The cleared accumulator
	 */
	private Double [] clearAccumulator(Double []accumulatorArray){
		for(int i = 0; i < MAX_MINOR_AXIS; ++i){
			accumulatorArray[i] = 0.0;
		}
		return accumulatorArray;
	}
	
	/** 
	 * Guarantees a non negative value
	 * @param value {@link Double}: Value to be checked
	 * @return {@link Double}: Returns 0 if the value is negative, else return the value
	 */
	private Double nonNegative(Double value){
		return value < 0?0:value;
	}
	
	/** 
	 * Calculates the points that composes the ellipse
	 * @param ellipse {@link Ellipse}:Ellipse
	 * @return {@link List} of {@link Coordinate2D} that composes the ellipse
	 */
	private List<Coordinate2D> calculateEllipsesPoints(Ellipse ellipse){
		double [] ellipsePoint = new double[2];
		List<Coordinate2D> listEllipsePoints = new ArrayList<Coordinate2D>();
		for(int t = 0 ; t < PATH_PIXELS ; ++t){
			//elx
			ellipsePoint [0] = 
					(ellipse.getCenter().getX()) 
					+ (ellipse.getMajorAxis()/2)
					* (Math.cos((2 * Math.PI * t) / (PATH_PIXELS - 1)))
					* (Math.cos(ellipse.getOrientation())) 								
					- (ellipse.getMinorAxis()/2)
					* (Math.sin((2 * Math.PI * t) / (PATH_PIXELS - 1)))
					* (Math.sin(ellipse.getOrientation()))
					
					; 
			//ely
			ellipsePoint[1] =  
					(ellipse.getCenter().getY()) 
					+ (ellipse.getMajorAxis()/2)
					* (Math.cos((2 * Math.PI * t) / (PATH_PIXELS - 1)))
					* (Math.sin(ellipse.getOrientation()))	
					+ (ellipse.getMinorAxis()/2)
					* (Math.sin((2 * Math.PI * t) /(PATH_PIXELS - 1)))
					* (Math.cos(ellipse.getOrientation())) 
					; 
			
			listEllipsePoints.add(new Coordinate2D((int) ellipsePoint[0],(int) ellipsePoint[1]));
		}
		
		return listEllipsePoints;
	}
	
	/** 
	 * Load the default algorithm parameter values
	 */
	private void loadParametersConfiguration(){
		MIN_MAJOR_AXIS = 30;					
		MAX_MAJOR_AXIS = 100;
		MIN_MINOR_AXIS = 10;
		MAX_MINOR_AXIS = 40;
		MIN_FREQUENCY = 10;	
		VALID_POINTS_DISTANCE = 3;
		PATH_PIXELS = Float.valueOf(30); 
		ACCURACY_DETECTION =  Float.valueOf("0.8");
		MAX_ANGLE = 30;		
	}

	@Override
	public Map<String, List<Geometry>> detectedEllipsesAndCircles(
			List<List<? extends GeometryEntity>> imageData) {
		logger.warn("NOT IMPLEMENTED BY THIS STRATEGY");
		return null;
	}

}
