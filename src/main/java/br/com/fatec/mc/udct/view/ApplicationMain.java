/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 30/05/2013<br>
 */

package br.com.fatec.mc.udct.view;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.beans.PropertyVetoException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.facade.IApplicationFacade;
import br.com.fatec.mc.udct.core.facade.impl.ApplicationFacade;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.view.actions.InternationalizationAction;
import br.com.fatec.mc.udct.view.actions.ManualDetectionAction;
import br.com.fatec.mc.udct.view.actions.NeuralNetworkConfigWindowAction;
import br.com.fatec.mc.udct.view.actions.StartWebCamAction;
import br.com.fatec.mc.udct.view.actions.XMIConfigWindowAction;

/** 
 * DESCRIPTION <br>
 * Application Main interface. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 30/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

@SuppressWarnings("serial")
public class ApplicationMain extends JFrame {

	private static ApplicationMain instance = null;

	/**
	 * The Application facade
	 */
	private IApplicationFacade facade = ApplicationFacade.getInstance();
	/**
	 * Logger
	 */
	private Logger logger = LoggerFactory.getLogger(ApplicationMain.class);

	/**
	 * Locale for Brazil
	 */
	private Locale BRAZIL = new Locale("pt", "BR");

	/**
	 * Application Locale
	 */
	private Locale locale = BRAZIL;

	/**
	 * ResourceBundle for application messages
	 */
	private ResourceBundle messages = ResourceBundle.getBundle("br.com.fatec.mc.udct.resources.ApplicationBundle", locale);

	private JDesktopPane desktopPane;
	private JComboBox<ImageIcon> comboBox;
	private JMenuBar menuBar;

	private JMenu mnDetection;
	private JMenu mnConfiguration;

	private JMenuItem mniManualDetection;
	private JMenuItem mniRealTimeDetection;
	private JMenuItem mniNeuralNetworkConfig;
	private JMenuItem mniXMIConfig;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ApplicationMain window = getInstance(); 
					window.setVisible(true);
				} catch (Exception e) {
					LoggerFactory.getLogger(getClass()).error(e.getMessage());
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	private ApplicationMain() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() { 
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		menuBar = new JMenuBar();
		menuBar.setAlignmentX(Component.LEFT_ALIGNMENT);
		setJMenuBar(menuBar);

		mnDetection = new JMenu();
		menuBar.add(mnDetection);
		mniManualDetection = new JMenuItem();
		mniManualDetection.setAction(new ManualDetectionAction());

		mniRealTimeDetection = new JMenuItem();
		mniRealTimeDetection.setVisible(false);
		mniRealTimeDetection.setAction(new StartWebCamAction());

		mnDetection.add(mniManualDetection);
		mnDetection.add(mniRealTimeDetection);

		ImageIcon brFlag = new ImageIcon(this.getClass().getResource("/br/com/fatec/mc/udct/resources/images/pt_BR.png"));
		ImageIcon usFlag = new ImageIcon(this.getClass().getResource("/br/com/fatec/mc/udct/resources/images/en_US.png"));
		brFlag.setDescription(messages.getString("menu.combobox.internationalization.ptBR"));
		usFlag.setDescription(messages.getString("menu.combobox.internationalization.enUS"));

		mnConfiguration = new JMenu(); 
		menuBar.add(mnConfiguration);

		mniNeuralNetworkConfig = new JMenuItem();
		mniNeuralNetworkConfig.setAction(new NeuralNetworkConfigWindowAction());
		mnConfiguration.add(mniNeuralNetworkConfig);

		mniXMIConfig = new JMenuItem();
		mniXMIConfig.setAction(new XMIConfigWindowAction());
		mnConfiguration.add(mniXMIConfig);

		Component horizontalGlue = Box.createHorizontalGlue();
		horizontalGlue.setMaximumSize(new Dimension(700000, 0));
		horizontalGlue.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		menuBar.add(horizontalGlue);

		comboBox = new JComboBox<ImageIcon>();
		comboBox.setPreferredSize(new Dimension(25, 25));
		comboBox.setAlignmentX(Component.RIGHT_ALIGNMENT);
		comboBox.addItem(brFlag);
		comboBox.addItem(usFlag);
		comboBox.setAction(new InternationalizationAction());
		menuBar.add(comboBox);
		
		textLocaleRefresh();

		desktopPane = new JDesktopPane();
		desktopPane.setMaximumSize(new Dimension(30, 30));
		getContentPane().add(desktopPane);

		GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(desktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 705, Short.MAX_VALUE)
				);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(desktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
				);
	}

	/** 
	 * Sets the text values of interface components.
	 * Refresh the values when the Locale changes.
	 */
	public void textLocaleRefresh(){
		ResourceBundle.clearCache();
		setTitle(messages.getString("application.name"));
		mnDetection.setText(messages.getString("menu.detection"));
		mniManualDetection.setText(messages.getString("menu.item.manual.detection"));
		mniRealTimeDetection.setText(messages.getString("menu.item.real.time.detection"));

		mnConfiguration.setText(messages.getString("menu.configuration"));
		mniNeuralNetworkConfig.setText(messages.getString("menu.neural.network"));
		mniXMIConfig.setText(messages.getString("menu.xmi.files"));
	}

	public void addImage(Image image){
		ImageWindow imgWindow = new ImageWindow(image);
		desktopPane.add(imgWindow);
		imgWindow.setVisible(true);
		try {
			imgWindow.setSelected(true);
		} catch (PropertyVetoException e) {
			logger.error(e.getMessage());
		}
		desktopPane.repaint();
	}

	public Image getSelectedImage() {
		ImageWindow selectedImage = (ImageWindow) desktopPane.getSelectedFrame();
		if(selectedImage == null) 
			return null;
		return selectedImage.getImage();
	}

	/**
	 * Getter method of instance
	 *
	 * @return instance
	 */
	public static ApplicationMain getInstance() {
		if(instance == null){
			instance = new ApplicationMain();
		}
		return instance;
	}

	/**
	 * Getter method of facade
	 *
	 * @return facade
	 */
	public IApplicationFacade getFacade() {
		return facade;
	}
	/**
	 * Getter method of messages
	 *
	 * @return messages
	 */
	public ResourceBundle getMessages() {
		return messages;
	}

	/**
	 * Setter method of messages
	 *
	 * @param messages Class attribute
	 */
	public void setMessages(ResourceBundle messages) {
		this.messages = messages;
	}

	/**
	 * Getter method of locale
	 * @return locale {@link Locale}
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Setter method of locale
	 * @param locale {@link Locale}
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * Getter method of comboBox
	 * @return comboBox {@link JComboBox}
	 */
	public JComboBox<ImageIcon> getComboBox() {
		return comboBox;
	}

	/**
	 * Getter method of desktopPane
	 * @return desktopPane {@link JDesktopPane}
	 */
	public JDesktopPane getDesktopPane() {
		return desktopPane;
	}
	
}
