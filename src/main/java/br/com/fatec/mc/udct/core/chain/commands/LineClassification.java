/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 08/09/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.List;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.geometry.LineType;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Classifies the detected lines in either Dashed or Full. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 08/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class LineClassification implements Command {

	/** 
	 * Classifies the lines in Dashed or Full
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		List<Line> lines = (List<Line>) context.get(UseCaseContext.LINES);
		for(Line line:lines){
			double distance = 0;
			for(int i = 0 ; i < line.getLinePoints().size() - 1 ; i++){
				Coordinate2D coord = line.getLinePoints().get(i);
				Coordinate2D nextCoord = line.getLinePoints().get(i + 1);
				distance += coord.distance2Coordinate(nextCoord);
			}
			
			if(distance/line.getLinePoints().size() > 2){
				line.setLineType(LineType.DASHED);
			}else{
				line.setLineType(LineType.FULL);		
			}
		}
		return false;
	}

}
