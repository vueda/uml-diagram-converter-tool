/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.utils.image;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;

import web.unbc.ca.Ellipse;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.domain.geometry.Circle;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.geometry.LineType;
import br.com.fatec.mc.udct.domain.geometry.StandardLine;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Provide methods to print lines, circles and ellipses on the image. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class ImageFormPrintUtils {
	
	/** 
	 * Print the lines in the image
	 * @param lines {@link List} of {@link Line}: Lines to be printed
	 * @param image {@link Image}: Image where lines will be printed
	 */
	public static void printLines(List<Line> lines, Image image){

		Graphics grafico = (Graphics) image.getImage().getGraphics();
		grafico.setColor(Color.BLUE);

		for(Line l:lines){
/*			Random randomGenerator = new Random();
			int red = randomGenerator.nextInt(255);
			int green = randomGenerator.nextInt(255);
			int blue = randomGenerator.nextInt(255);*/

/*			Color randomColour = new Color(red,green,blue);
 * 
*/			
			if(l.getLineType().equals(LineType.DASHED)){
				grafico.setColor(Color.ORANGE);
			}else{
				grafico.setColor(Color.BLUE);
			}
			StandardLine li = (StandardLine) l;
			Coordinate2D c1 = li.getFirstPoint();
			Coordinate2D c2 = li.getLastPoint();

			grafico.drawLine(c1.getX(), c1.getY() , c2.getX(), c2.getY());
			
		}
	}
    
	/** 
	 * Print the circles in the image
	 * @param circles {@link List} of {@link Circle}: Circles to be printed
	 * @param image {@link Image}: Image where circles will be printed
	 */
    public static void printCircles(List<Geometry> circles, Image image){
    	Graphics grafico = (Graphics) image.getImage().getGraphics();
    	grafico.setColor(Color.GREEN);

    	for(Geometry g:circles){
    		web.unbc.ca.Circle c = (web.unbc.ca.Circle) g;
    		grafico.drawOval((int)(c.getCenter().getX().intValue() - c.getRadius()),
    				(int)(c.getCenter().getY().intValue() - c.getRadius()),
    				(int)c.getRadius() * 2, 
    				(int)c.getRadius() * 2);
    	}
    }
    
    /** 
	 * Print the circles in the image
	 * @param circles {@link List} of {@link Circle}: Circles to be printed
	 * @param image {@link Image}: Image where circles will be printed
	 */
    public static void printEllipses(List<Geometry> ellipses, Image image){
    	Graphics2D grafico = (Graphics2D) image.getImage().getGraphics();
    	grafico.setColor(Color.RED);

    	for(Geometry g:ellipses){
    		web.unbc.ca.Ellipse e = (web.unbc.ca.Ellipse) g;
			double alfa = Math.atan2(e.getAxisB().getY1() - e.getAxisB().getY2(),e.getAxisB().getX1() -e.getAxisB().getX2());

    		grafico.rotate(alfa, e.getCenter().getX(), e.getCenter().getY());

    		grafico.drawArc((int)(e.getCenter().getX().intValue() - Ellipse.getLength(e.getAxisB()) / 2), 
    				(int)(e.getCenter().getY().intValue() - Ellipse.getLength(e.getAxisA())/2), 
    				(int)Ellipse.getLength(e.getAxisB()), 
    				(int)Ellipse.getLength(e.getAxisA()), 0, 360);
    		grafico.rotate(-alfa, e.getCenter().getX(), e.getCenter().getY());
    	}
    }
}
