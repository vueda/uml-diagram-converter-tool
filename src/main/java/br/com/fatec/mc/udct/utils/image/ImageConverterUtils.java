/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 29/05/2013<br>
 */

package br.com.fatec.mc.udct.utils.image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Image utility class. Provides recurrent methods used in image conversions <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 29/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class ImageConverterUtils {
	
	/** 
	 * Clones the image provided
	 * @param source - Image to be cloned
	 * @return - {@link Image}: Cloned image
	 */
	public static Image cloneImage(BufferedImage source){
		Image target = new Image(source.getWidth(), source.getHeight());

		for (int x = 0; x < source.getWidth(); x++){
			for (int y = 0; y < source.getHeight(); y++) {
				Color color = new Color(source.getRGB(x, y));
				source.getRGB(x, y);
				target.setRGB(x, y, color.getRed(), color.getGreen(), color.getBlue());
			}
		}
		return target;
	}
	
	/** 
	 * Draws the given segments into the given image
	 * @param segments - List of coordinates (segments)
	 * @param image - Source image
	 * @return image with segments drawn
	 */
	public static Image coordinates2Image(List<List<Coordinate2D>> segments, Image image){

		for(List<Coordinate2D> segment:segments){
			Random rd = new Random();
			int color1 = rd.nextInt(256);
			int color2 = rd.nextInt(256);
			int color3 = rd.nextInt(256);
			for(Coordinate2D c:segment){
				image.setRGB(c.getX(), c.getY(), color1, color2, color3);
			}
		}
		return image;
	}

	/** 
	 * Converts an image to a {@link List} of {@link Coordinate2D}. It will extract all the black points 
	 * from the image.
	 * @param image {@link Image}: The image to be converted
	 * @return {@link List} of {@link Coordinate2D}: List with coordinates that composes the image
	 */
	public static List<Coordinate2D> image2CoordinatesBlackEdge(Image image){
		List<Coordinate2D> edgePixels = new ArrayList<Coordinate2D>();    

		for(int y = 0 ; y <  image.getImage().getHeight() ; y++){
			for(int x = 0 ; x < image.getImage().getWidth() ; x++){
				if(image.getG(x, y) < 10 && image.getR(x, y) < 10 && image.getB(x, y) < 10 &&
						(image.getG(x, y) == image.getG(x, y) && image.getG(x, y) == image.getG(x, y)))
					edgePixels.add(new Coordinate2D(x,y));
			}
		}
		return edgePixels;

	}
	
	/** 
	 * Converts an image to a {@link List} of {@link Coordinate2D}. It will extract all the white points 
	 * from the image.
	 * @param image {@link Image}: The image to be converted
	 * @return {@link List} of {@link Coordinate2D}: List with coordinates that composes the image
	 */
	public static List<Coordinate2D> image2CoordinateWhiteEdge(Image image){
		List<Coordinate2D> edgePixels = new ArrayList<Coordinate2D>();    
  
        for(int y = 0 ; y <  image.getImage().getHeight() ; y++){
            for(int x = 0 ; x < image.getImage().getWidth() ; x++){
               if(image.getG(x, y) == 255)
            	   edgePixels.add(new Coordinate2D(x,y));
            }
        }
		return edgePixels;
    }
	
	/** 
	 * Converts a 2D matrix to an {@link Image}.
	 * @param imageMatrix: 2D matrix that represents an image
	 * @return {@link Image}:Image corresponding to the matrix
	 */
	public static Image matrix2Image(Integer[][] imageMatrix){
		Image output = new Image(imageMatrix.length, imageMatrix[0].length);

		for (int x = 0; x < imageMatrix.length; x++){
			for (int y = 0; y < imageMatrix[0].length; y++) {
				int pixel = imageMatrix[x][y];
				output.setRGB(x, y, pixel, pixel, pixel);
			}
		}

		return output;	
	}
	
	/** 
	 * Converts a 2D matrix to an {@link Image}.
	 * @param imageMatrix: 2D matrix that represents an image
	 * @return {@link Image}:Image corresponding to the matrix
	 */
	public static Image matrix2Image(int[][] imageMatrix){
		Image output = new Image(imageMatrix.length, imageMatrix[0].length);

		for (int x = 0; x < imageMatrix.length; x++){
			for (int y = 0; y < imageMatrix[0].length; y++) {
				int pixel = imageMatrix[x][y];
				output.setRGB(x, y, pixel, pixel, pixel);
			}
		}

		return output;	
	}
	/** 
	 * Converts an {@link Image} to a 2D matrix
	 * @param image {@link Image}: Image to be converted
	 * @return {@link Integer}[][] - matrix corresponding to th image
	 */
	public static Integer[][] image2Matrix(Image image){
		Integer[][] imageMatrix = new Integer [image.getWidth()][image.getHeight()];
	
    	for (int x = 0; x < imageMatrix.length; x++){
			for (int y = 0; y < imageMatrix[0].length; y++) {
				int pixel = image.getR(x, y);
				imageMatrix[x][y] = pixel;
			}
		}
    	
    	return imageMatrix;	
    }
	
	/** 
	 * Converts the image to a binary image. Pixels with gradient lower than the
	 * threshold will be black and the higher will be white. 
	 * @param image {@link Image}:The image to be converted
	 * @param threshold {@link Integer}:Threshold to classify the pixels
	 * @return {@link Image}:Binary image
	 */
	public static Image image2BinaryWhiteEdge(Image image, Integer threshold) {
		int BLACK = Color.BLACK.getRGB();
		int WHITE = Color.WHITE.getRGB();

		BufferedImage output = new BufferedImage(image.getWidth(),
				image.getHeight(), BufferedImage.TYPE_INT_RGB);

		for (int y = 0; y < image.getHeight(); y++){
			for (int x = 0; x < image.getWidth(); x++) {
				Color pixel = new Color(image.getImage().getRGB(x, y));
				output.setRGB(x, y, pixel.getRed() < threshold ? BLACK : WHITE);
			}
		}

		return new Image("", output);
	}
	
	/** 
	 * Converts the image to a binary image. Pixels with gradient lower than the
	 * threshold will be white and the higher will be black. 
	 * @param image {@link Image}:The image to be converted
	 * @param threshold {@link Integer}:Threshold to classify the pixels
	 * @return {@link Image}:Binary image
	 */
	public static Image image2BinaryBlackEdge(Image image, Integer threshold) {
		int BLACK = Color.BLACK.getRGB();
		int WHITE = Color.WHITE.getRGB();

		BufferedImage output = new BufferedImage(image.getWidth(),
				image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);

		for (int y = 0; y < image.getHeight(); y++){
			for (int x = 0; x < image.getWidth(); x++) {
				Color pixel = new Color(image.getImage().getRGB(x, y));
				output.setRGB(x, y, pixel.getRed() < threshold ? WHITE : BLACK);
			}
		}

		return new Image("",output);
	}
}
