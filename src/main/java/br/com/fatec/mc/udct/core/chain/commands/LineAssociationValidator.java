/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 08/09/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import web.unbc.ca.Circle;
import web.unbc.ca.Coordinate;
import web.unbc.ca.Ellipse;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.StandardLine;
import br.com.fatec.mc.udct.utils.adapter.CoordinateAdapter;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Validates if lines have two different associations. For example a line
 * needs to be associated with different circles and/or ellipses. If it's 
 * associated with the same element it can be discarded <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 08/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class LineAssociationValidator implements Command {

	/** 
	 * Validates if the lines have two different association
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		List<StandardLine> lines = (List<StandardLine>) context.get(UseCaseContext.LINES);
		Map<String,List<Geometry>> circlesAndEllipses = (Map<String, List<Geometry>>) context.get(UseCaseContext.CIRCLES_ELLIPSES);;
		List<Geometry> ellipses = circlesAndEllipses.get(Ellipse.class.getSimpleName());
		List<Geometry> circles = circlesAndEllipses.get(Circle.class.getSimpleName());
		List<StandardLine> lines2Remove = new ArrayList<StandardLine>();
		
		for(StandardLine line: lines){		
			Coordinate coord = CoordinateAdapter.adapt2Coordinate(line.getFirstPoint());
			Geometry useCaseElement = getConnectedElement(coord, ellipses, circles);
	
			coord = CoordinateAdapter.adapt2Coordinate(line.getLastPoint());
			Geometry useCaseElement2 = getConnectedElement(coord, ellipses, circles);
			//If there is no associated element or if the elements are the same the line is excluded
			if(useCaseElement == null || useCaseElement2 == null || useCaseElement.equals(useCaseElement2)){
				lines2Remove.add(line);
			}
		}
		lines.removeAll(lines2Remove);
		return false;
	}

	private Geometry getConnectedElement(Coordinate coordinate, List<Geometry> ellipses, List<Geometry> circles){
		Geometry useCaseElement = null;
		double distance = Double.MAX_VALUE;

		for(Geometry g: circles){
			Circle c= (Circle) g;
			if(useCaseElement == null){
				useCaseElement = c;
				distance = c.getCenter().distance2Coordinate(coordinate);
			}else if(c.getCenter().distance2Coordinate(coordinate) < distance){
				distance = c.getCenter().distance2Coordinate(coordinate);
				useCaseElement = c;
			}
		}

		for(Geometry g: ellipses){
			Ellipse e= (Ellipse) g;
			if(useCaseElement == null){
				useCaseElement = e;
				distance = e.getCenter().distance2Coordinate(coordinate);
			}else if(e.getCenter().distance2Coordinate(coordinate) < distance){
				distance = e.getCenter().distance2Coordinate(coordinate);
				useCaseElement = e;
			}
		}
		return useCaseElement;
	}
}
