/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 19/10/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.usecase.Relationship;
import br.com.fatec.mc.udct.domain.usecase.UseCaseDiagram;
import br.com.fatec.mc.udct.domain.usecase.UseCaseEntity;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Validates duplicated relationships. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 19/10/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class DuplicatedRelationshipValidator implements Command {

	private Logger logger = LoggerFactory.getLogger(DuplicatedRelationshipValidator.class);

	/** 
	 * Validates duplicated relationships
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@Override
	public boolean execute(Context context) throws Exception {
		logger.info("Validating duplicated relationships.");

		UseCaseDiagram diagram = (UseCaseDiagram) context.get(UseCaseContext.DIAGRAM);
		List<Relationship> relationships = new ArrayList<Relationship>();
		List<UseCaseEntity> rels2Remove = new ArrayList<UseCaseEntity>();
		for(UseCaseEntity en : diagram.getUseCaseElements()){
			if(en instanceof Relationship){
				relationships.add((Relationship) en);
			}
		}
		
		for(int i = 0 ; i < relationships.size() ; i ++){
			Relationship current = relationships.get(i);
			for(int j = i + 1 ; j < relationships.size() ; j ++){
				Relationship next = relationships.get(j);
				if(isSameElementAssociation(current, next)){
					rels2Remove.add(next);
					relationships.remove(j);
					j--;
				}
				
			}	
		}
		
		diagram.getUseCaseElements().removeAll(rels2Remove);
		logger.info("Number of use case diagram elements: "+ diagram.getUseCaseElements().size() +".");

		return false;
	}
	
	private boolean isSameElementAssociation(Relationship current, Relationship next){
		UseCaseEntity currentEl1 = current.getElements().get(0);
		UseCaseEntity currentEl2 = current.getElements().get(1);
		UseCaseEntity nextEl1 = next.getElements().get(0);
		UseCaseEntity nextEl2 = next.getElements().get(1);
	
		boolean firstElement = currentEl1.equals(nextEl1) || currentEl1.equals(nextEl2);
		boolean secondElement = currentEl2.equals(nextEl1) || currentEl2.equals(nextEl2);
		if(firstElement && secondElement){
			return true;
		}
		return false;
	}

}
