/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 08/09/2013<br>
 */

package br.com.fatec.mc.udct.domain.geometry;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Represents an arrowed line. It means the line has an arrow in it's
 * start or end<br>
 * <br>
 * CLASS VERSIONS: <br>
 * 08/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class ArrowedLine extends StandardLine{
	private ArrowType arrowType;
	private Coordinate2D arrowPosition;
	
	/** 
	 * Class constructor
	 * @param line - {@link StandardLine} 
	 */
	public ArrowedLine(StandardLine line){
		setAngularCoefficient(line.getAngularCoefficient());
		setLinearCoefficient(line.getLinearCoefficient());
		setLinePoints(line.getLinePoints());
		setId(line.getId());
	}
	
	/** 
	 * The arrow type of the line
	 * @return {@link ArrowType}: The type of the arrow
	 */
	public ArrowType getArrowType() {
		return arrowType;
	}
	
	/** 
	 * Setter for arrow type
	 * @param arrowType - {@link ArrowType}
	 */
	public void setArrowType(ArrowType arrowType) {
		this.arrowType = arrowType;
	}
	
	/** 
	 * The place where the arrow is positioned. It can be the start or the end
	 * of the line
	 * @return {@link Coordinate2D}: Start or end of the line where the arrow is positioned
	 */
	public Coordinate2D getArrowPosition() {
		return arrowPosition;
	}
	
	/** 
	 * Setter method for the arrow position
	 * @param arrowPosition - {@link Coordinate2D}: Coordinate where the arrow is close to
	 */
	public void setArrowPosition(Coordinate2D arrowPosition) {
		this.arrowPosition = arrowPosition;
	}
	
	
}
