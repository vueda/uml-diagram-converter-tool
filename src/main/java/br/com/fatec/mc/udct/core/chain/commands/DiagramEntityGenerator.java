/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 25/08/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Circle;
import web.unbc.ca.Coordinate;
import web.unbc.ca.Ellipse;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.geometry.LineType;
import br.com.fatec.mc.udct.domain.geometry.StandardLine;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.domain.usecase.Actor;
import br.com.fatec.mc.udct.domain.usecase.Relationship;
import br.com.fatec.mc.udct.domain.usecase.RelationshipType;
import br.com.fatec.mc.udct.domain.usecase.UseCase;
import br.com.fatec.mc.udct.domain.usecase.UseCaseDiagram;
import br.com.fatec.mc.udct.domain.usecase.UseCaseEntity;
import br.com.fatec.mc.udct.utils.DistanceCalculatorUtil;
import br.com.fatec.mc.udct.utils.adapter.CoordinateAdapter;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Generate the UseCase entities based on the detected information. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 25/08/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class DiagramEntityGenerator implements Command {

	private Logger logger = LoggerFactory.getLogger(DiagramEntityGenerator.class);

	private final double POINT_DISTANCE = 3;
	private final double MIN_POINTS = 30;

	private Map<Object,UseCaseEntity> elements;
	private Map<Geometry, String> strings;
	private Map<Line, String> linesString;

	/** 
	 * Iterates over the detected forms and convert them to UseCase entities. 
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		UseCaseDiagram useCaseDiagram = new UseCaseDiagram();
		Image image = (Image) context.get(UseCaseContext.IMAGE);
		useCaseDiagram.setImage(image);

		elements = new HashMap<Object,UseCaseEntity>();
		strings = (Map<Geometry, String>) context.get(UseCaseContext.STRINGS);
		linesString = (Map<Line, String>) context.get(UseCaseContext.LINES_STRINGS);

		List<Object> elements2Remove = new ArrayList<Object>();
		List<StandardLine> lines = (List<StandardLine>) context.get(UseCaseContext.LINES);
		Map<String,List<Geometry>> circlesAndEllipses = (Map<String, List<Geometry>>) context.get(UseCaseContext.CIRCLES_ELLIPSES);;
		List<Geometry> ellipses = circlesAndEllipses.get(Ellipse.class.getSimpleName());
		List<Geometry> circles = circlesAndEllipses.get(Circle.class.getSimpleName());

		logger.info("Assigning unique id's for the elements.");
		assignIdForElements(lines, circles, ellipses);

		logger.info("Associating elements and relationships.");
		for(StandardLine line: lines){
			Coordinate coord = CoordinateAdapter.adapt2Coordinate(line.getFirstPoint());
			Geometry useCaseElement = getConnectedElement(coord, ellipses, circles);

			coord = CoordinateAdapter.adapt2Coordinate(line.getLastPoint());
			Geometry useCaseElement2 = getConnectedElement(coord, ellipses, circles);
			if(!useCaseElement.equals(useCaseElement2)){
				generateUseCaseElement((Relationship)elements.get(line), useCaseElement, useCaseElement2);
			}else{
				elements2Remove.add(line);
			}

		}
		lines.removeAll(elements2Remove);
		for(Object key : elements.keySet()){
			if(!elements2Remove.contains(key)){
				useCaseDiagram.getUseCaseElements().add(elements.get(key));
			}
		}

		logger.info("Number of use case diagram elements detected: "+ useCaseDiagram.getUseCaseElements().size() +".");
		context.put(UseCaseContext.DIAGRAM, useCaseDiagram);
		return false;
	}

	/** 
	 * Return the closest element to the given {@link Coordinate}
	 * @param coordinate - {@link Coordinate}
	 * @param ellipses - All Ellipses
	 * @param circles - All circles
	 * @return {@link Geometry} - Element closest to the {@link Coordinate}
	 */
	private Geometry getConnectedElement(Coordinate coordinate, List<Geometry> ellipses, List<Geometry> circles){
		Geometry useCaseElement = null;
		double distance = Double.MAX_VALUE;

		for(Geometry g: circles){
			Circle c= (Circle) g;
			if(useCaseElement == null){
				useCaseElement = c;
				distance = c.getCenter().distance2Coordinate(coordinate);
			}else if(c.getCenter().distance2Coordinate(coordinate) < distance){
				distance = c.getCenter().distance2Coordinate(coordinate);
				useCaseElement = c;
			}
		}

		for(Geometry g: ellipses){
			Ellipse e= (Ellipse) g;
			if(useCaseElement == null){
				useCaseElement = e;
				distance = e.getCenter().distance2Coordinate(coordinate);
			}else if(e.getCenter().distance2Coordinate(coordinate) < distance){
				distance = e.getCenter().distance2Coordinate(coordinate);
				useCaseElement = e;
			}
		}
		return useCaseElement;
	}

	/** 
	 * Generate the {@link UseCaseEntity} related to this {@link Relationship}. It will create the {@link Actor} and {@link UseCase}
	 * @param relationship - Current relationship
	 * @param element1 - Element 1 associated to the relationship
	 * @param element2 - Element 2 associated to the relationship
	 */
	private void generateUseCaseElement(Relationship relationship, Geometry element1, Geometry element2){
		UseCaseEntity el1 = null;
		UseCaseEntity el2 = null;

		if(element1 instanceof Circle){
			Actor actor = (Actor) elements.get(element1);
			relationship.getElements().add(actor);
			el1 = actor;
		}else{
			UseCase useCase = (UseCase) elements.get(element1);
			relationship.getElements().add(useCase);
			el1 = useCase;
		}

		if(element2 instanceof Circle){
			Actor actor = (Actor) elements.get(element2);
			relationship.getElements().add(actor);
			el2 = actor;
		}else{
			UseCase useCase = (UseCase) elements.get(element2);
			relationship.getElements().add(useCase);
			el2 = useCase;
		}

		//If one of the elements is an actor but it's relationship is extend or include it needs to be corrected
		boolean el1IsActor = el1 instanceof Actor;
		boolean el2IsActor = el2 instanceof Actor;
		if((el1IsActor || el2IsActor) && (el1IsActor != el2IsActor) && (relationship.getType().equals(RelationshipType.EXTEND) ||
				relationship.getType().equals(RelationshipType.INCLUDE))){
			relationship.setType(RelationshipType.ASSOCIATION);
		}

		//If the elements are not of the same type and the relationship is generalization corrects it
		if((el1IsActor != el2IsActor) && relationship.getType().equals(RelationshipType.GENERALIZATION)){
			relationship.setType(RelationshipType.ASSOCIATION);			
		}

		if(relationship.getType().equals(RelationshipType.GENERALIZATION)){
			Coordinate2D coord = CoordinateAdapter.adapt2Coordinate2D(element1.getCenter());
			Coordinate2D coord2 = CoordinateAdapter.adapt2Coordinate2D(element2.getCenter());
			double distEl1 = relationship.getArrowedElementPosition().distance2Coordinate(coord);
			double distEl2 = relationship.getArrowedElementPosition().distance2Coordinate(coord2);
			if(distEl1 < distEl2){
				relationship.setArrowedElement(el1);
				if(el1 instanceof Actor){
					((Actor)el2).getGeneralizations().add(relationship);
				}else{
					((UseCase)el2).getGeneralizations().add(relationship);
				}
			}else{
				relationship.setArrowedElement(el2);
				if(el2 instanceof Actor){
					((Actor)el1).getGeneralizations().add(relationship);
				}else{
					((UseCase)el1).getGeneralizations().add(relationship);
				}
			}
		}else if(relationship.getType().equals(RelationshipType.EXTEND)){
			Coordinate2D coord = CoordinateAdapter.adapt2Coordinate2D(element1.getCenter());
			Coordinate2D coord2 = CoordinateAdapter.adapt2Coordinate2D(element2.getCenter());
			if(relationship.getArrowedElementPosition() != null){
				double distEl1 = relationship.getArrowedElementPosition().distance2Coordinate(coord);
				double distEl2 = relationship.getArrowedElementPosition().distance2Coordinate(coord2);
				if(distEl1 < distEl2){
					relationship.setArrowedElement(el1);
					if(el1 instanceof UseCase && el2 instanceof UseCase){
						((UseCase)el1).getExtendedByCases().add(relationship);
						((UseCase)el2).getExtendsCases().add(relationship);
					}
	
				}else{
					relationship.setArrowedElement(el2);
					if(el1 instanceof UseCase && el2 instanceof UseCase){
						((UseCase)el2).getExtendedByCases().add(relationship);
						((UseCase)el1).getExtendsCases().add(relationship);				
					}
				}
			}

		}else if(relationship.getType().equals(RelationshipType.INCLUDE)){
			Coordinate2D coord = CoordinateAdapter.adapt2Coordinate2D(element1.getCenter());
			Coordinate2D coord2 = CoordinateAdapter.adapt2Coordinate2D(element2.getCenter());
			double distEl1 = relationship.getArrowedElementPosition().distance2Coordinate(coord);
			double distEl2 = relationship.getArrowedElementPosition().distance2Coordinate(coord2);
			if(distEl1 < distEl2){
				relationship.setArrowedElement(el1);
				((UseCase)el2).getIncludeCases().add(relationship);

			}else{
				relationship.setArrowedElement(el2);
				((UseCase)el1).getIncludeCases().add(relationship);		
			}
		}

	}

	/** 
	 * Assign unique id's for all elements
	 * @param lines
	 * @param circles
	 * @param ellipses
	 */
	private void assignIdForElements(List<StandardLine> lines, List<Geometry> circles, List<Geometry> ellipses){
		int elementId = 0;
		for(StandardLine line:lines){
			Relationship relationship = generateRelationship(line);
			relationship.setId(elementId);
			elementId++;
			elements.put(line,relationship);
		}		
		for(Geometry form:circles){
			Circle circle = (Circle) form;
			Actor actor = new Actor();
			actor.setId(elementId);
			elementId++;
			actor.setName(strings.get(form));
			elements.put(circle, actor);
		}
		for(Geometry form:ellipses){
			Ellipse ellipse = (Ellipse) form;
			UseCase useCase = new UseCase();
			useCase.setId(elementId);
			useCase.setDescription(strings.get(form));

			elementId++;
			elements.put(ellipse, useCase);
		}
	}

	/** 
	 * Generate a relationship based on the {@link StandardLine}
	 * @param line - {@link StandardLine}
	 * @return {@link Relationship}
	 */
	private Relationship generateRelationship(StandardLine line){
		Relationship rel = new Relationship();
		Coordinate2D start = line.getFirstPoint();
		Coordinate2D end = line.getLastPoint();
		Coordinate2D refStart = null;
		Coordinate2D refEnd = null;
		int startCounter = 0;
		int finalCounter = 0;

		//Checks the initial part of the line
		for(int i = 0 ; i < line.getLinePoints().size() / 3 ; i ++){
			Coordinate2D c = line.getLinePoints().get(i);
			double distance = DistanceCalculatorUtil.getDistanceToSegment(start, end, c);
			if(distance > POINT_DISTANCE){
				startCounter++;
				refStart = c;
			}
		}

		//Checks the final part of the line
		for(int i = line.getLinePoints().size() -  (line.getLinePoints().size() / 3); i < line.getLinePoints().size(); i ++){
			Coordinate2D c = line.getLinePoints().get(i);
			double distance = DistanceCalculatorUtil.getDistanceToSegment(start, end, c);
			if(distance > POINT_DISTANCE){
				finalCounter++;
				refEnd = c;
			}
		}

		if(line.getLineType().equals(LineType.DASHED)){//Dashed lines are either <<Include>> or <<Extend>> relationship
			String lineType = linesString.get(line);
			lineType = lineType == null ? "" : lineType;
			//FIXME: This will probably not work!!!
			if(lineType.startsWith("E")){
				rel.setType(RelationshipType.EXTEND);
				rel.setDescription(lineType);
			}else if(lineType.startsWith("I")){
				rel.setType(RelationshipType.INCLUDE);
				rel.setDescription(lineType);
			}else{//If none matches put extend
				rel.setType(RelationshipType.EXTEND);
				rel.setDescription(lineType);
			}

			if(startCounter > finalCounter){
				if(refStart != null){
					double startDist = refStart.distance2Coordinate(start);
					double endDist = refStart.distance2Coordinate(end);
					if(startDist < endDist){
						rel.setArrowedElementPosition(start);
					}else{
						rel.setArrowedElementPosition(end);
					}
				}
			}else{
				if(refEnd != null){
					double startDist = refEnd.distance2Coordinate(start);
					double endDist = refEnd.distance2Coordinate(end);
					if(startDist < endDist){
						rel.setArrowedElementPosition(start);
					}else{
						rel.setArrowedElementPosition(end);
					}
				}
			}

		}else{//A full line is a generalization or a simple relationship
			if(startCounter > finalCounter && startCounter > MIN_POINTS){//Checks generalization arrow position 
				double startDist = refStart.distance2Coordinate(start);
				double endDist = refStart.distance2Coordinate(end);
				if(startDist < endDist){
					rel.setArrowedElementPosition(start);
					rel.setType(RelationshipType.GENERALIZATION);
				}else{
					rel.setArrowedElementPosition(end);
					rel.setType(RelationshipType.GENERALIZATION);
				}
			}else if(finalCounter > startCounter && finalCounter > MIN_POINTS){//Checks generalization arrow position 
				double startDist = refEnd.distance2Coordinate(start);
				double endDist = refEnd.distance2Coordinate(end);
				if(startDist < endDist){
					rel.setArrowedElementPosition(start);
					rel.setType(RelationshipType.GENERALIZATION);
				}else{
					rel.setArrowedElementPosition(end);
					rel.setType(RelationshipType.GENERALIZATION);
				}
			}else{//It's a normal association
				rel.setType(RelationshipType.ASSOCIATION);
			}
		}

		return rel;
	}
}
