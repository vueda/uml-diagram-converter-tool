/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 08/09/2013<br>
 */

package br.com.fatec.mc.udct.domain.geometry;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Enum that indicates if the line is Dashed or Full. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 08/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public enum LineType {
	DASHED,FULL;
}
