/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 25/08/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Changes the detail level for EdgeDrawing strategy. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 25/08/2013 - @author Vinícius Oliveira Ueda<br>
 *<br>
 *<br>
 */

public class DetailLevelAdjust implements Command {
	private Logger logger = LoggerFactory.getLogger(DetailLevelAdjust.class);

	private final int DETAIL_LEVEL = 1;
	/** 
	 * Changes the detail level for the EdgeDrawing strategy. This is necessary to get more data from
	 * image to line detection.
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		logger.info("Adjusting the detail level for edge drawing to " + DETAIL_LEVEL);
		Integer detailLevel = Integer.valueOf(DETAIL_LEVEL);
		context.put(UseCaseContext.DETAIL_LEVEL, detailLevel);
		return false;
	}

}
