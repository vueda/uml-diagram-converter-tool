/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy.context;

import java.util.List;

import br.com.fatec.mc.udct.core.strategy.IEdgeDetectionStrategy;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Represents the context to execute edge detection strategies. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */
public class EdgeDetectionContext {
	private IEdgeDetectionStrategy strategy;
	private Image image;
	private Integer detailLevel;
	/** 
	 * Execute the detection
	 * @return {@link List} of {@link Coordinate2D}: Segments extracted from image
	 */
	public List<List<Coordinate2D>> executeDetection(){
		return strategy.detectEdges(image, detailLevel);
	}
	
	/** 
	 * Class constructor
	 * @param strategy {@link IEdgeDetectionStrategy}: The concrete strategy to edge detection
	 */
	public EdgeDetectionContext(IEdgeDetectionStrategy strategy){
		this.strategy = strategy;
	}
	
	/**
	 * Sets the {@link Image} of the context
	 * @param image {@link Image}
	 */
	public void setImage(Image image) {
		this.image = image;
	}
	
	/**
	 * Sets the detail level for detection
	 * @param detailLevel Class attribute
	 */
	public void setDetailLevel(Integer detailLevel) {
		this.detailLevel = detailLevel;
	}
}
