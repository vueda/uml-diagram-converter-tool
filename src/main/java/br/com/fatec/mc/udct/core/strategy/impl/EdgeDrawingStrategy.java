/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy.impl;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.strategy.IEdgeDetectionStrategy;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.domain.image.Pixel;
import br.com.fatec.mc.udct.utils.image.MaskApplierUtils;
import br.com.fatec.mc.udct.utils.image.Masks;


/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Edge detection method proposed by Cuneyt Akinlar and Cihan Topal.<br>
 * Implementation based on: <i>http://ceng.anadolu.edu.tr/cv/EDPF/</i><br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class EdgeDrawingStrategy implements IEdgeDetectionStrategy {
	private Logger logger = LoggerFactory.getLogger(EdgeDrawingStrategy.class);

	/**
	 * Sobel operator threshold
	 */
	final int SOBEL_THRESHOLD = 36;
	
	/**
	 * Achor threshold
	 */
	final int ANCHOR_THRESHOLD = 150;
	
	/**
	 * Constant for horizontal edges
	 */
	final int HORIZONTAL = 0;
	
	/**
	 * Constant for vertical edges
	 */
	final int VERTICAL = 1;
	
	/**
	 * Detail level of anchor points step
	 */
	int DETAIL = 3;
	
	/**
	 * List with all the detected segments
	 */
	List<List<Coordinate2D>> listSegments = new ArrayList<List<Coordinate2D>>();
	
	/**
	 * Coordinates to be processed in the chaining method.
	 */
	List<Coordinate2D> chainingCoordinates = new ArrayList<Coordinate2D>();
	
	/**
	 * Each individual segment
	 */
	List<Coordinate2D> segment;
	
	/**
	 * Matrix with the direction of each pixel
	 */
	int[][] edgeDirection;
	
	/**
	 * Matrix with pixel gradients
	 */
	int[][] gradientMatrix;
	
	/**
	 * Matrix with anchor points
	 */
	int[][] anchorPoints;
	
	/**
	 * Three right pixels to check which has the most higher gradient
	 */
	Pixel[] gradientsRight = new Pixel[3];
	
	/**
	 * Three left pixels to check which has the most higher gradient
	 */
	Pixel[] gradientsLeft = new Pixel[3];
	
	/**
	 * Three right pixels to check which has the most higher gradient
	 */
	Pixel[] gradientsNorth = new Pixel[3];
	
	/**
	 * Three right pixels to check which has the most higher gradient
	 */
	Pixel[] gradientsSouth = new Pixel[3];
	
	/**
	 * Number of anchors that a segment has passed	
	 */
	int passedAnchors = 0;	
	
	/**
	 * Image width
	 */
	int width;
	
	/**
	 * Image height
	 */
	int height;
	
	
	/** 
	 * Detect edge segment using the edge drawing strategy 
	 * @param image {@link Image}: Image where the edges will be searched
	 * @return {@link List} of {@link Coordinate2D}: Segments extracted from image
	 * @see br.com.fatec.mc.udct.core.strategy.IEdgeDetectionStrategy#detectEdges(br.com.fatec.mc.udct.domain.image.Image)
	 */
	@Override
	public List<List<Coordinate2D>> detectEdges(Image image, Integer detailLevel) {		
		if(detailLevel != null && detailLevel != 0){
			logger.info("Detail level altered to " + detailLevel +".");
			DETAIL = detailLevel;
		}
		
		width = image.getWidth();
		height = image.getHeight();
		
		edgeDirection = new int [width][height];
		anchorPoints = new int [width][height];
        gradientMatrix = new int [width][height];
		
		logger.info("Applying gaussian mask");
        int[][] imageMatrix = MaskApplierUtils.applyMask(Masks.GAUSSIAN_MASK, image);
        
		logger.info("Calculating vertical and horizontal gradient map with sobel.");
        int[][] horizontalGradient = MaskApplierUtils.applyMask(Masks.SOBEL_HORIZONTAL, imageMatrix);
        int[][] verticalGradient = MaskApplierUtils.applyMask(Masks.SOBEL_VERTICAL, imageMatrix);

		logger.info("Build edge directon matrix and gradient matrix based on the data colected by sobel.");
        for(int x = 0 ; x < width ;x++){
        	for(int y = 0 ; y < height ; y++){
        		gradientMatrix[x][y] = Math.abs(horizontalGradient[x][y]) + Math.abs(verticalGradient[x][y]);
        		int gradient = gradientMatrix[x][y];	
        		if(gradient > SOBEL_THRESHOLD){
        			imageMatrix[x][y] = Color.WHITE.getRed();
        			if(Math.abs(horizontalGradient[x][y]) >= Math.abs(verticalGradient[x][y])){
            			edgeDirection[x][y] = HORIZONTAL;
            		}else{
            			edgeDirection[x][y] = VERTICAL;
            		}
        		}else{
        			imageMatrix[x][y] = Color.BLACK.getRed();
        		} 
        	}
        }
               
		logger.info("Compute the anchor points.");
        for(int x = 1 ; x < width - 1 ;x += DETAIL){
        	for(int y = 1 ; y < height - 1; y += DETAIL){
        		if(edgeDirection[x][y] == HORIZONTAL){
        			if(gradientMatrix[x][y] - gradientMatrix[x][y-1] >= ANCHOR_THRESHOLD &&
        					gradientMatrix[x][y] - gradientMatrix[x][y+1] >= ANCHOR_THRESHOLD){
        				anchorPoints[x][y] = 255;
        			}else{
        				anchorPoints[x][y] = 0;
        			}
        			
        		}else{
        			if(gradientMatrix[x][y] - gradientMatrix[x - 1][y] >= ANCHOR_THRESHOLD &&
        					gradientMatrix[x][y] - gradientMatrix[x + 1][y] >= ANCHOR_THRESHOLD){
        				anchorPoints[x][y] = 255;
        			}else{
        				anchorPoints[x][y] = 0;
        			}
        		}
        	}
        }
                
		logger.info("Conecting points to create segments starting by the anchors.");
        for(int x = 1 ; x < width - 1 ; x +=DETAIL){
        	for(int y = 1 ; y < height - 1 ; y+=DETAIL){
        		
        		if(anchorPoints[x][y] == 255){//Look if it is an anchor point
        			passedAnchors = 0;
        			segment = new ArrayList<Coordinate2D>();
        			segment.add(new Coordinate2D(x,y));
        			anchorPoints[x][y] = 0;
        			pixelChaining(segment.get(0));
        			while(chainingCoordinates.size() > 0){
        				Coordinate2D c1 = chainingCoordinates.get(0);
        				pixelChaining(c1);	
        			}
        			if(passedAnchors > 5)
        				listSegments.add(segment);
        		}
        	}
        }	

        return listSegments; 
	}
	
	public void pixelChaining(Coordinate2D c){
		int x = c.getX();
		int y = c.getY();
		boolean flagProcessed = false;
		if(x > 0 && x < width - 1 && y > 0 && y < height - 1){
			if(edgeDirection[x][y] == HORIZONTAL){
			
				gradientsRight[0] = (new Pixel(x + 1, y - 1, gradientMatrix[x + 1][y - 1]));
				gradientsRight[1] = (new Pixel(x + 1, y, gradientMatrix[x + 1][y]));
				gradientsRight[2] = (new Pixel(x + 1, y + 1, gradientMatrix[x + 1][y + 1]));
				Pixel maxGradRight = Collections.max(Arrays.asList(gradientsRight));
				
				gradientsLeft[0] = (new Pixel(x - 1, y - 1, gradientMatrix[x + 1][y - 1]));
				gradientsLeft[1] = (new Pixel(x - 1, y, gradientMatrix[x + 1][y]));
				gradientsLeft[2] = (new Pixel(x - 1, y + 1, gradientMatrix[x + 1][y + 1]));
				
				Pixel maxGradLeft = Collections.max(Arrays.asList(gradientsLeft));
					
				if(!segment.contains(gradientsRight[0]) 
						&& !segment.contains(gradientsRight[1])
						&& !segment.contains(gradientsRight[2])
						&& maxGradRight.getGradient() > 0){
					segment.add(maxGradRight);
					chainingCoordinates.add(0,maxGradRight);
					flagProcessed = true;
					if(anchorPoints[maxGradRight.getX()][maxGradRight.getY()] == 255){
						anchorPoints[maxGradRight.getX()][maxGradRight.getY()] = 0;
						passedAnchors++;
					}
				}
				
				if(!segment.contains(gradientsLeft[0]) 
						&& !segment.contains(gradientsLeft[1])
						&& !segment.contains(gradientsLeft[2]) 
						&& maxGradLeft.getGradient() > 0){
					segment.add(maxGradLeft);
					chainingCoordinates.add(0,maxGradLeft);
					flagProcessed = true;
					if(anchorPoints[maxGradLeft.getX()][maxGradLeft.getY()] == 255){
						anchorPoints[maxGradLeft.getX()][maxGradLeft.getY()] = 0;
						passedAnchors++;
					}
				}
							
			}else if(edgeDirection[x][y] == VERTICAL){	
				gradientsNorth[0] = (new Pixel(x - 1, y - 1, gradientMatrix[x - 1][y - 1]));
				gradientsNorth[1] = (new Pixel(x, y - 1, gradientMatrix[x][y - 1]));
				gradientsNorth[2] = (new Pixel(x + 1, y - 1, gradientMatrix[x + 1][y - 1]));
				Pixel maxGradNorth = Collections.max(Arrays.asList(gradientsNorth));
				
				gradientsSouth[0] = (new Pixel(x - 1, y + 1, gradientMatrix[x - 1][y + 1]));
				gradientsSouth[1] = (new Pixel(x, y + 1, gradientMatrix[x][y + 1]));
				gradientsSouth[2] = (new Pixel(x + 1, y + 1, gradientMatrix[x + 1][y + 1]));
				Pixel maxGradSouth = Collections.max(Arrays.asList(gradientsSouth));
				
				if(!segment.contains(gradientsNorth[0]) 
						&& !segment.contains(gradientsNorth[1])
						&& !segment.contains(gradientsNorth[2])
						&& maxGradNorth.getGradient() > 0){
					segment.add(maxGradNorth);
					chainingCoordinates.add(0,maxGradNorth);
					flagProcessed = true;
					if(anchorPoints[maxGradNorth.getX()][maxGradNorth.getY()] == 255){
						anchorPoints[maxGradNorth.getX()][maxGradNorth.getY()] = 0;
						passedAnchors++;
					}
				}
				
				if(!segment.contains(gradientsSouth[0]) 
						&& !segment.contains(gradientsSouth[1])
						&& !segment.contains(gradientsSouth[2])
						&& maxGradSouth.getGradient() > 0){
					segment.add(maxGradSouth);
					chainingCoordinates.add(0,maxGradSouth);
					flagProcessed = true;
					if(anchorPoints[maxGradSouth.getX()][maxGradSouth.getY()] == 255){
						anchorPoints[maxGradSouth.getX()][maxGradSouth.getY()] = 0;
						passedAnchors++;
					}
				}
				
			}
		}
		
		if(flagProcessed == false)
			chainingCoordinates.remove(c);	
	}

}
