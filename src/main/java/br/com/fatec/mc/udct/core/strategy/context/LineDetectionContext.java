/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy.context;

import java.util.List;

import br.com.fatec.mc.udct.core.strategy.ILineDetectionStrategy;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Represents the context to execute line detection strategies. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class LineDetectionContext {
	
	private ILineDetectionStrategy strategy;
	private Image image;
	private List<List<Coordinate2D>> imageSegments;
	
	/** 
	 * Execute the detection
	 * @return {@link List} of detected {@link Line}
	 */
	public List<Line> executeDetection(){
		if(imageSegments != null){
			return strategy.detectLines(imageSegments, image);
		}else{
			return strategy.detectLines(image);
		}
	}
	
	/** 
	 * Class constructor
	 * @param strategy {@link ILineDetectionStrategy}: The concrete line detection strategy
	 */
	public LineDetectionContext(ILineDetectionStrategy strategy){
		this.strategy = strategy;
	}
	
	/**
	 * Sets the {@link Image} of the context
	 * @param image {@link Image}
	 */
	public void setImage(Image image) {
		this.image = image;
	}

	/** 
	 * Sets the segments that composes the image
	 * @param imageSegments
	 */
	public void setImageSegments(List<List<Coordinate2D>> imageSegments) {
		this.imageSegments = imageSegments;
	}
	
	

}
