/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 29/05/2013<br>
 */

package br.com.fatec.mc.udct.utils.image;

import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Utility class that provides methods to apply masks on images. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 21/04/2013 - @author Carlos Alberto de Oliveira <br>
 * 29/05/2013 - @author Vinícius Oliveira Ueda - (Refactor) <br>
 *<br>
 *<br>
 */

public class MaskApplierUtils {
	
	/** 
	 * Applies a mask to the image.
	 * @param mask {@link Double}[][] - mask 
	 * @param image - {@link Image} the image with the mask applied
	 */
	public static int[][] applyMask(double[][] mask, Image image) {
		int height = image.getHeight();							
		int width = image.getWidth();	
				
		int row  = (mask.length - 1)/2;     					// delta x
		int column = (mask[0].length -1)/2;  					// delta y
		int[][] pixels = new int[width][height];
        for(int xColumn = 0; xColumn < width; xColumn++){
            for(int yRow = 0; yRow < height; yRow++){
                int wMask = 0, hMask = 0;
                double resultingPixel = 0;
                
                for(int x = (xColumn - column); x <= (xColumn + column); x++){
                    for(int y = (yRow - row); y <= (yRow + row); y++){
                        if((x >= 0) && (x < width) && (y >= 0) &&(y < height)){
                            resultingPixel += image.getR(x, y) * mask[wMask][hMask];
                        }
                        hMask++;
                    }
                    wMask++;
                    hMask = 0;
                }
                pixels[xColumn][yRow] = (int) resultingPixel;
            }
        }
        return pixels;
	}
	
	public static int[][] applyMask(double[][] mask, int [][] pixel) {
		int height = pixel[0].length;							// height
		int width = pixel.length;						// width
		int [][]processedMatrix = new int[width][height];
		int row  = (mask.length - 1)/2;     					// delta x
        int column = (mask[0].length -1)/2;  					// delta y
        
        for(int xColumn = 0; xColumn < width; xColumn++){
            for(int yRow = 0; yRow < height; yRow++){
                int wMask = 0, hMask = 0;
                double resultingPixel = 0;
                
                for(int x = (xColumn - column); x <= (xColumn + column); x++){
                    for(int y = (yRow - row); y <= (yRow + row); y++){
                        if((x >= 0) && (x < width) && (y >= 0) &&(y < height)){
                            resultingPixel += pixel[x][y] * mask[wMask][hMask];
                        }
                        hMask++;
                    }
                    wMask++;
                    hMask = 0;
                }

//                if(resultingPixel > 255){ resultingPixel = 255;}
//                if(resultingPixel < 0){ resultingPixel = 0;}
                
                processedMatrix[xColumn][yRow] = (int) resultingPixel;
            }
        }
        return processedMatrix;
	}

}
