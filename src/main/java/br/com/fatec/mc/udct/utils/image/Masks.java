/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 30/05/2013<br>
 */

package br.com.fatec.mc.udct.utils.image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Class with masks for image smoothing. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 30/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public abstract class Masks {
	public static final double [][] GAUSSIAN_MASK =  
	   	   {{1, 4, 7, 4, 1}, 
			{4, 16, 26, 16, 4}, 
			{7, 26, 41, 26, 7},
			{4, 16, 26, 16, 4},
			{1, 4, 7, 4, 1}};
	
	public static final double [][] SOBEL_HORIZONTAL = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
	public static final double [][] SOBEL_VERTICAL = {{1, 2, 1}, {0, 0, 0}, {-1, -2, -1}};

	public static final double [][] MEDIAN_MASK =  
	   	   {{1, 4, 7, 4, 1}, 
			{4, 16, 26, 16, 4}, 
			{7, 26, 41, 26, 7},
			{4, 16, 26, 16, 4},
			{1, 4, 7, 4, 1}};
}
