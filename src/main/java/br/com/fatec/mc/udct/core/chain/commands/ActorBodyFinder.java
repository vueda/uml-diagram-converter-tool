/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 22/09/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Circle;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.image.HistogramUtils;
import br.com.fatec.mc.udct.utils.image.ImageConverterUtils;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Checks the area under a circle looking for the body of the actor. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 22/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class ActorBodyFinder implements Command {
	private Logger logger = LoggerFactory.getLogger(ActorBodyFinder.class);

	/**
	 * Minimum blank space to cut the image
	 */
	private final int BLANK_SPACE = 10; 

	/** 
	 * Checks for the actors body
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> circles2Remove = new ArrayList<Geometry>();
		List<Geometry> circles = circlesAndEllipses.get(Circle.class.getSimpleName());
		Image image = (Image)context.get(UseCaseContext.IMAGE);
    	Map<Geometry, List<Image>> texts = (Map<Geometry, List<Image>>) context.get(UseCaseContext.TEXTS);

		for(Geometry form : circles){
			Circle circle = (Circle) form;
			int diameter = (int) (circle.getRadius() * 2);

			logger.info("Looking near the circle for the possible actor's body.");
			int x = (int) (circle.getCenter().getX() - circle.getRadius());
			int y = circle.getCenter().getY();
			int heightCut = image.getHeight() + diameter * 4 > image.getHeight() ? image.getHeight() - y - 1  : diameter * 4;
			BufferedImage subImage = image.getImage().getSubimage(x, y, diameter, heightCut);

			Image img = ImageConverterUtils.cloneImage(subImage);
			img = ImageConverterUtils.image2BinaryWhiteEdge(img, 150);

			int[] hHist = HistogramUtils.getHorizontalHistogram(img);
			int bodyYCut = 0;
			boolean hasBroke = false;
			for(int i = 0 ; i < hHist.length ; i ++){
				if(hHist[i] == 0){
					for(int j = i ; j < hHist.length ; j ++){
						int nextVal = hHist[j];
						if(nextVal != 0 && (j - i > BLANK_SPACE)){
							bodyYCut = i;
							hasBroke = true;
							break;
						}else if(nextVal != 0){
							i = j;							
						}
					}
					if(bodyYCut != 0){
						bodyYCut += circle.getCenter().getY();
						break;
					}
				}
			}
			
			if(!hasBroke){
				/*logger.info("No body found. Clear the circle.");
				int topLeftX = (int) (circle.getCenter().getX() - circle.getRadius());
				int topLeftY = (int) (circle.getCenter().getY() - circle.getRadius());
				clearDataFromdImage(image, topLeftX, topLeftY, diameter, diameter);*/
			}else if(bodyYCut - circle.getCenter().getY() < diameter * 2){
				logger.info("No body found. Clear the circle.");
				int topLeftX = (int) (circle.getCenter().getX() - circle.getRadius());
				int topLeftY = (int) (circle.getCenter().getY() - circle.getRadius());
				clearDataFromImage(image, topLeftX, topLeftY, diameter, diameter);
				//circles2Remove.add(circle);
			}else{//Clear the Actor from the image and extract the text
				logger.info("Clear the circle, the actor's body and extract the text.");
				int topLeftX = (int) (circle.getCenter().getX() - circle.getRadius());
				int topLeftY = (int) (circle.getCenter().getY() - circle.getRadius());
				int clearHeight = bodyYCut - topLeftY;		
				clearDataFromImage(image, topLeftX, topLeftY, diameter, clearHeight);

				int textX = circle.getCenter().getX() - (2 * diameter) < 0 ? 0 : circle.getCenter().getX() - (2 * diameter);
				int textWidth = (int) ((textX + diameter * 3.5) > image.getWidth() ? image.getWidth() - textX - 1 : diameter * 3.5);
				int textHeight =  (bodyYCut + diameter + diameter/2) > image.getHeight() ? bodyYCut - image.getHeight() - 1 : diameter + diameter/2;
				BufferedImage textImage = image.getImage().getSubimage(textX, bodyYCut, textWidth, Math.abs(textHeight) - 2);
				Image txtImg = ImageConverterUtils.cloneImage(textImage);
				List<Image> textImages = new ArrayList<Image>();
				textImages.add(txtImg);
				texts.put(circle, textImages);
				clearDataFromImage(image, textX, bodyYCut, textWidth, textHeight);

			}					
		}
		circles.removeAll(circles2Remove);
		return false;
	}

	/** 
	 * Clear the given rectangular area from the image
	 */
	private void clearDataFromImage(Image image, int x, int y, int width, int height){
		Graphics graphic = (Graphics) image.getImage().getGraphics();
		graphic.setColor(Color.WHITE);
		graphic.fillRect(x, y, width, height);
	}


}
