/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.usecase;

import java.util.ArrayList;
import java.util.List;

import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Domain class that represents a relationship in a UseCase diagram. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 28/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class Relationship extends UseCaseEntity{
	private String description;
	private RelationshipType type = RelationshipType.ASSOCIATION;
	private List<UseCaseEntity> elements;
	private UseCaseEntity arrowedElement;
	
	/**
	 * Generalization, Includes and Extends have an arrow pointing to the element closer to this point
	 */
	private Coordinate2D arrowedElementPosition;
	
	/** 
	 * The type of this relationship (ie. Association/Generalization/Include/Extends)
	 * @return {@link RelationshipType}
	 */
	public RelationshipType getType() {
		return type;
	}
	
	public void setType(RelationshipType type) {
		this.type = type;
	}

	/** 
	 * Elements associated to this relationship (ie. {@link Actor} and {@link UseCase})
	 * @return
	 */
	public List<UseCaseEntity> getElements() {
		if(elements == null){
			elements = new ArrayList<UseCaseEntity>();
		}
		return elements;
	}

	public void setElements(List<UseCaseEntity> elements) {
		this.elements = elements;
	}

	/** 
	 * The relationship description
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/** 
	 * The {@link Coordinate2D} position of the element which the arrows points at.
	 * @return
	 */
	public Coordinate2D getArrowedElementPosition() {
		return arrowedElementPosition;
	}

	public void setArrowedElementPosition(Coordinate2D arrowedElement) {
		this.arrowedElementPosition = arrowedElement;
	}

	/** 
	 * The element which the arrow is pointing
	 * @return
	 */
	public UseCaseEntity getArrowedElement() {
		return arrowedElement;
	}

	public void setArrowedElement(UseCaseEntity arrowedElement) {
		this.arrowedElement = arrowedElement;
	}
	
}
