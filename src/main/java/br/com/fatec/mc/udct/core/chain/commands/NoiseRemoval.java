/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 08/09/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.image.ImageConverterUtils;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Command that removes noise from the image. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 08/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class NoiseRemoval implements Command {
	private Logger logger = LoggerFactory.getLogger(NoiseRemoval.class);

	/** 
	 * Removes noise from the image using a binary strategy process.
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		logger.info("Removing noises on the image using the binarization process.");

		Image image = (Image) context.get(UseCaseContext.IMAGE);
		Image binImage = ImageConverterUtils.image2BinaryWhiteEdge(image, 180);
		context.put(UseCaseContext.IMAGE, binImage);

		return false;
	}

}
