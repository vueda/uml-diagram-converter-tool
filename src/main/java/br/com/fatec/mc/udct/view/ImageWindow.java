/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 30/05/2013<br>
 */

package br.com.fatec.mc.udct.view;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;

import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Window with an image loaded. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 30/05/2013 - @author Vinícius Oliveira Ueda<br>
 *<br>
 *<br>
 */

@SuppressWarnings("serial")
public class ImageWindow extends JInternalFrame{
	private JLabel imageLabel;
	private Image image;
	
	public ImageWindow(Image image){
		setLayout(new BorderLayout());
		setClosable(true);
		setTitle(image.getImageName());
        setBounds(0, 0, image.getWidth(), image.getHeight());
		
        this.image = image;
        
		ImageIcon imageIcon = new ImageIcon(image.getImage());
		this.imageLabel = new JLabel();
		this.imageLabel.setIcon(imageIcon);
		this.imageLabel.validate();
        add(imageLabel, BorderLayout.CENTER);
       
	}

	/** 
	 * Returns the image label
	 * @return {@link String}: Image label
	 */
	public JLabel getImageLabel() {
		return imageLabel;
	}

	/** 
	 * Returns the image contained in this window
	 * @return {@link Image}: Image 
	 */
	public Image getImage() {
		return image;
	}
	
	public void setImageLabel(JLabel imageLabel) {
		this.imageLabel = imageLabel;
	}
	
	public void setImage(Image image) {
		this.image = image;
	}
	
	
}
