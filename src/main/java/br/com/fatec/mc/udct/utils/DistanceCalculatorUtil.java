package br.com.fatec.mc.udct.utils;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;

public class DistanceCalculatorUtil{

	/**
	 * Returns distance to segment
	 * @param ss : segment start Coordinate2D
	 * @param se : segment end Coordinate2D
	 * @param p : Coordinate2D to found closest Coordinate2D on segment
	 * @return distance to segment
	 */
	public static double getDistanceToSegment(Coordinate2D ss, Coordinate2D se, Coordinate2D p)
	{
		return getDistanceToSegment(ss.getX(), ss.getY(), se.getX(), se.getY(), p.getX(), p.getY());
	}

	/**
	 * Returns distance to segment
	 * 
	 * @param sx1
	 *            segment x coord 1
	 * @param sy1
	 *            segment y coord 1
	 * @param sx2
	 *            segment x coord 2
	 * @param sy2
	 *            segment y coord 2
	 * @param px
	 *            Coordinate2D x coord
	 * @param py
	 *            Coordinate2D y coord
	 * @return distance to segment
	 */
	public static double getDistanceToSegment(int sx1, int sy1, int sx2, int sy2, int px, int py)
	{
		Coordinate2D closestCoordinate2D = getClosestCoordinate2DOnSegment(sx1, sy1, sx2, sy2, px, py);
		return getDistance(closestCoordinate2D.getX(), closestCoordinate2D.getY(), px, py);
	}
	/**
	 * Returns distance between two 2D Coordinate2Ds
	 * 
	 * @param Coordinate2D1
	 *            first Coordinate2D
	 * @param Coordinate2D2
	 *            second Coordinate2D
	 * @return distance between Coordinate2Ds
	 */
	public static double getDistance(Coordinate2D Coordinate2D1, Coordinate2D Coordinate2D2)
	{
		return getDistance(Coordinate2D1.getX(), Coordinate2D1.getY(), Coordinate2D2.getX(), Coordinate2D2.getY());
	}


	/**
	 * Returns distance between two sets of coords
	 * 
	 * @param x1
	 *            first x coord
	 * @param y1
	 *            first y coord
	 * @param x2
	 *            second x coord
	 * @param y2
	 *            second y coord
	 * @return distance between sets of coords
	 */
	public static double getDistance(float x1, float y1, float x2, float y2)
	{
		// using long to avoid possible overflows when multiplying
		double dx = x2 - x1;
		double dy = y2 - y1;

		// return Math.hypot(x2 - x1, y2 - y1); // Extremely slow
		// return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)); // 20 times faster than hypot
		return Math.sqrt(dx * dx + dy * dy); // 10 times faster then previous line
	}

	/**
	 * Returns closest Coordinate2D on segment to Coordinate2D
	 * 
	 * @param ss
	 *            segment start Coordinate2D
	 * @param se
	 *            segment end Coordinate2D
	 * @param p
	 *            Coordinate2D to found closest Coordinate2D on segment
	 * @return closest Coordinate2D on segment to p
	 */
	public static Coordinate2D getClosestCoordinate2DOnSegment(Coordinate2D ss, Coordinate2D se, Coordinate2D p)
	{
		return getClosestCoordinate2DOnSegment(ss.getX(), ss.getY(), se.getX(), se.getY(), p.getX(), p.getY());
	}

	/**
	 * Returns closest Coordinate2D on segment to Coordinate2D
	 * 
	 * @param sx1
	 *            segment x coord 1
	 * @param sy1
	 *            segment y coord 1
	 * @param sx2
	 *            segment x coord 2
	 * @param sy2
	 *            segment y coord 2
	 * @param px
	 *            Coordinate2D x coord
	 * @param py
	 *            Coordinate2D y coord
	 * @return closets Coordinate2D on segment to Coordinate2D
	 */
	public static Coordinate2D getClosestCoordinate2DOnSegment(int sx1, int sy1, int sx2, int sy2, int px, int py)
	{
		double xDelta = sx2 - sx1;
		double yDelta = sy2 - sy1;

		if ((xDelta == 0) && (yDelta == 0))
		{
			throw new IllegalArgumentException("Segment start equals segment end");
		}

		double u = ((px - sx1) * xDelta + (py - sy1) * yDelta) / (xDelta * xDelta + yDelta * yDelta);

		final Coordinate2D closestCoordinate2D;
		if (u < 0)
		{
			closestCoordinate2D = new Coordinate2D(sx1, sy1);
		}
		else if (u > 1)
		{
			closestCoordinate2D = new Coordinate2D(sx2, sy2);
		}
		else
		{
			closestCoordinate2D = new Coordinate2D((int) Math.round(sx1 + u * xDelta), (int) Math.round(sy1 + u * yDelta));
		}

		return closestCoordinate2D;
	}
}