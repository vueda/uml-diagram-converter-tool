/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/05/2013<br>
 */

package br.com.fatec.mc.udct.core.facade.impl;

import org.apache.commons.chain.Catalog;
import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.apache.commons.chain.config.ConfigParser;
import org.apache.commons.chain.impl.CatalogFactoryBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.core.exceptions.DiagramParsingException;
import br.com.fatec.mc.udct.core.exceptions.XMIParsingException;
import br.com.fatec.mc.udct.core.facade.IApplicationFacade;
import br.com.fatec.mc.udct.core.strategy.impl.ArgoUMLXMIParseStrategy;
import br.com.fatec.mc.udct.core.strategy.impl.EDCirclesStrategy;
import br.com.fatec.mc.udct.core.strategy.impl.EDLinesStrategy;
import br.com.fatec.mc.udct.core.strategy.impl.EdgeDrawingStrategy;
import br.com.fatec.mc.udct.core.strategy.impl.TesseractOCRStrategy;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.domain.usecase.UseCaseDiagram;


/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * The application facade. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 28/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class ApplicationFacade implements IApplicationFacade {
	/**
	 * Interface logger.
	 */
	Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * Facade singleton instance.
	 */
	private static ApplicationFacade instance = null;
	
	private Catalog catalog;
	private Context detectionContext;
	
	/** 
	 * Facade private constructor
	 */
	private ApplicationFacade(){
		this.catalog = createCatalog();
	}
	
	/** 
	 * Method that executes a detection of use case elements in an loaded image.
	 * @param context {@link Context}: The context passed to each command in the detection process.
	 * @see br.com.fatec.mc.udct.core.facade.IApplicationFacade#manualDetection(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	public synchronized UseCaseDiagram manualDetection(Image image) throws DiagramParsingException{
		contextSetUp();
		UseCaseDiagram diagram = null;
		detectionContext.put(UseCaseContext.IMAGE, image);
		try {
			Command chain = catalog.getCommand("use-case-detection-chain");
			chain.execute(detectionContext);
			diagram = (UseCaseDiagram) detectionContext.get(UseCaseContext.DIAGRAM);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new DiagramParsingException(e);
		}
		return diagram;		
	}
	
	/** 
	 * Generates a XMI file based on the {@link UseCaseDiagram} model.
	 * @param diagram - {@link UseCaseDiagram}:Use case diagram entity model
	 * @throws XMIParsingException 
	 * @see br.com.fatec.mc.udct.core.facade.IApplicationFacade#generateXMIFile(br.com.fatec.mc.udct.domain.usecase.UseCaseDiagram)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public synchronized void generateXMIFile(UseCaseDiagram diagram) throws XMIParsingException {
		detectionContext = new UseCaseContext();
		detectionContext.put(UseCaseContext.XMI_PARSING_STRATEGY, ArgoUMLXMIParseStrategy.class);
		detectionContext.put(UseCaseContext.DIAGRAM, diagram);
		try {
			Command chain = catalog.getCommand("xmi-generation-chain");
			chain.execute(detectionContext);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new XMIParsingException(e);
		}
	}	
	
	/** 
	 * Creates the {@link Catalog} containing the chain for use case detection
	 * @return {@link Catalog}: The chain catalog
	 * @see br.com.fatec.mc.udct.core.facade.IApplicationFacade#createCatalog()
	 */
	private Catalog createCatalog() {
		String file = "/br/com/fatec/mc/udct/resources/chain/chain-config.xml";
		ConfigParser parser = new ConfigParser();
		try {
			parser.parse(this.getClass().getResource(file));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return CatalogFactoryBase.getInstance().getCatalog("use-case-detection-catalog");
	}
	
	/** 
	 * Sets up the context for the process
	 */
	@SuppressWarnings("unchecked")
	private void contextSetUp(){
		detectionContext = new UseCaseContext();
		detectionContext.put(UseCaseContext.EDGE_DETECTION_STRATEGY, EdgeDrawingStrategy.class);
		detectionContext.put(UseCaseContext.ELLIPSE_DETECTION_STRATEGY, new EDCirclesStrategy());
		detectionContext.put(UseCaseContext.LINE_DETECTION_STRATEGY, new EDLinesStrategy());
		detectionContext.put(UseCaseContext.XMI_PARSING_STRATEGY, ArgoUMLXMIParseStrategy.class);
		detectionContext.put(UseCaseContext.OCR_RECOGNITION_STRATEGY, TesseractOCRStrategy.class);
	}
	
	/**
	 * Getter method of ApplicationFacade instance
	 * @return instance {@link ApplicationFacade}: Return the facade instance.
	 */
	public static ApplicationFacade getInstance() {
		if(instance == null){
			instance = new ApplicationFacade();
		}
		return instance;
	}

	
}
