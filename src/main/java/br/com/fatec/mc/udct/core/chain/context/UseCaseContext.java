/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/05/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.context;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.impl.ContextBase;

import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.strategy.IEdgeDetectionStrategy;
import br.com.fatec.mc.udct.core.strategy.IEllipseDetectionStrategy;
import br.com.fatec.mc.udct.core.strategy.ILineDetectionStrategy;
import br.com.fatec.mc.udct.core.strategy.IOCRStrategy;
import br.com.fatec.mc.udct.core.strategy.IXMIParsingStrategy;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.domain.usecase.UseCaseDiagram;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Class that represents the context in a UseCase diagram detection chain. <br>
 * The context is a stateful object with values needed to complete the transaction.<br>
 * It can stores data that will be used by every command that composes the chain.<br>
 * <br>
 * CLASS VERSIONS: <br>
 * 28/05/2013 - @author Vinícius Oliveira Ueda<br>
 *<br>
 *<br>
 */

@SuppressWarnings("serial")
public class UseCaseContext extends ContextBase{
	public static final String IMAGE = "image";
	public static final String DIAGRAM = "diagram";
	public static final String SEGMENTS = "segments";
	public static final String LINES = "lines";
	public static final String CIRCLES_ELLIPSES = "circlesAndEllipses";
	public static final String DETAIL_LEVEL = "detailLevel";
	public static final String TEXTS = "texts";
	public static final String STRINGS = "stringTexts";
	public static final String LINES_STRINGS = "linesTexts";

	public static final String EDGE_DETECTION_STRATEGY = "edgeDetectionStrategy";
	public static final String ELLIPSE_DETECTION_STRATEGY = "ellipseDetectionStrategy";
	public static final String LINE_DETECTION_STRATEGY = "lineDetectionStrategy";
	public static final String XMI_PARSING_STRATEGY = "xmiParsingStrategy";
	public static final String OCR_RECOGNITION_STRATEGY = "ocrRecognitionStrategy";
	
	private Image image;
	private UseCaseDiagram diagram;
	private List<List<Coordinate2D>> segments;
	private List<Line> lines;
	private Map<String,List<Geometry>> circlesAndEllipses;
	private Map<Geometry, List<Image>> texts;
	private Map<Geometry, String> stringTexts;
	private Map<Line, String> linesTexts;

	private int detailLevel;
	
	private Class<IEdgeDetectionStrategy> edgeDetectionStrategy;
	private Class<IXMIParsingStrategy> xmiParsingStrategy;
	private Class<IOCRStrategy> ocrRecognitionStrategy;
	private IEllipseDetectionStrategy ellipseDetectionStrategy;
	private ILineDetectionStrategy lineDetectionStrategy;

	public UseCaseContext(){
		this.diagram = new UseCaseDiagram();
	}
	
	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public UseCaseDiagram getDiagram() {
		return diagram;
	}

	public void setDiagram(UseCaseDiagram diagram) {
		this.diagram = diagram;
	}

	public Class<IEdgeDetectionStrategy> getEdgeDetectionStrategy() {
		return edgeDetectionStrategy;
	}

	public void setEdgeDetectionStrategy(
			Class<IEdgeDetectionStrategy> edgeDetectionStrategy) {
		this.edgeDetectionStrategy = edgeDetectionStrategy;
	}

	public IEllipseDetectionStrategy getEllipseDetectionStrategy() {
		return ellipseDetectionStrategy;
	}

	public void setEllipseDetectionStrategy(
			IEllipseDetectionStrategy ellipseDetectionStrategy) {
		this.ellipseDetectionStrategy = ellipseDetectionStrategy;
	}

	public ILineDetectionStrategy getLineDetectionStrategy() {
		return lineDetectionStrategy;
	}

	public void setLineDetectionStrategy(
			ILineDetectionStrategy lineDetectionStrategy) {
		this.lineDetectionStrategy = lineDetectionStrategy;
	}

	public List<List<Coordinate2D>> getSegments() {
		return segments;
	}

	public void setSegments(List<List<Coordinate2D>> segments) {
		this.segments = segments;
	}

	public List<Line> getLines() {
		return lines;
	}

	public void setLines(List<Line> lines) {
		this.lines = lines;
	}

	public Map<String, List<Geometry>> getCirclesAndEllipses() {
		return circlesAndEllipses;
	}

	public void setCirclesAndEllipses(Map<String, List<Geometry>> circlesAndEllipses) {
		this.circlesAndEllipses = circlesAndEllipses;
	}

	public int getDetailLevel() {
		return detailLevel;
	}

	public void setDetailLevel(int detailLevel) {
		this.detailLevel = detailLevel;
	}

	public Class<IXMIParsingStrategy> getXmiParsingStrategy() {
		return xmiParsingStrategy;
	}

	public void setXmiParsingStrategy(Class<IXMIParsingStrategy> xmiParsingStrategy) {
		this.xmiParsingStrategy = xmiParsingStrategy;
	}
	
	public Class<IOCRStrategy> getOcrRecognitionStrategy() {
		return ocrRecognitionStrategy;
	}

	public void setOcrRecognitionStrategy(Class<IOCRStrategy> ocrRecognitionStrategy) {
		this.ocrRecognitionStrategy = ocrRecognitionStrategy;
	}

	public Map<Geometry, List<Image>> getTexts() {
		if(texts == null){
			texts = new HashMap<Geometry, List<Image>>();
		}
		return texts;
	}

	public void setTexts(Map<Geometry, List<Image>> texts) {
		this.texts = texts;
	}

	public Map<Geometry, String> getStringTexts() {
		if(stringTexts == null){
			stringTexts = new HashMap<Geometry, String>();
		}
		return stringTexts;
	}

	public void setStringTexts(Map<Geometry, String> stringTexts) {
		this.stringTexts = stringTexts;
	}

	public Map<Line, String> getLinesTexts() {
		if(linesTexts == null){
			linesTexts = new HashMap<Line, String>();
		}
		return linesTexts;
	}

	public void setLinesTexts(Map<Line, String> linesTexts) {
		this.linesTexts = linesTexts;
	}
	
}
