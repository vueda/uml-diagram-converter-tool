/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 20/08/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Circle;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Validate the minimum and maximum size of the detected circle.<br>
 * <br>
 * CLASS VERSIONS: <br>
 * 20/08/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class CircleSizeValidator implements Command {
	private Logger logger = LoggerFactory.getLogger(CircleSizeValidator.class);

	/**
	 * Circle minimum radius
	 */
	private final float MIN_RADIUS = 20;
	
	/**
	 * Circle maximum radius
	 */
	private final float MAX_RADIUS = 80;
	
	/** 
	 * Validates the minimum and maximum size of the circle radius,
	 *  discarding the ones that doesn't fit the requirements 
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> circles2Remove = new ArrayList<Geometry>();
		List<Geometry> circles = circlesAndEllipses.get(Circle.class.getSimpleName());
		
		logger.info("Validating the radius of the detected circles. Maximum radius is " + MAX_RADIUS + " and minimum radius is " + MIN_RADIUS +".");
		logger.info("Number of circles before process: " + circles.size());
		for(Geometry form : circles){
			Circle circle = (Circle) form;
			if(circle.getRadius() < MIN_RADIUS ||
					circle.getRadius() > MAX_RADIUS){
				circles2Remove.add(circle);
			}
		}
		
		circles.removeAll(circles2Remove);
		logger.info("Number of circles after process: " + circles.size());

		return false;
	}

}
