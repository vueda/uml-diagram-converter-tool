/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 29/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.geometry;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Domain class that represents a circle. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 29/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class Circle extends GeometryEntity{
	private Double radius;
	private Coordinate2D center;
	
	/** 
	 * Class constructor
	 * @param center - Center of the circle represented by a {@link Coordinate2D}
	 * @param radius - Circle radius
	 */
	public Circle(Coordinate2D center, Double radius){
		this.center = center;
		this.radius = radius;
	}
	
	/** 
	 * The radius of the circle
	 * @return {@link Double}
	 */
	public Double getRadius() {
		return radius;
	}
	
	/** 
	 * The center coordinate of the circle 
	 * @return {@link Coordinate2D}
	 */
	public Coordinate2D getCenter() {
		return center;
	}
	
	public void setRadius(Double radius) {
		this.radius = radius;
	}
	public void setCenter(Coordinate2D center) {
		this.center = center;
	}
	
	
}
