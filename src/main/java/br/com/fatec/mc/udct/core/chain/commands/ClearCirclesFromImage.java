/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 27/07/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import web.unbc.ca.Circle;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Removes from the image the pixels of already processed circles.
 * This helps to keep the image as clear as possible for the next processes <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 27/07/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class ClearCirclesFromImage implements Command {

	private final int CLEAR_AREA = 5;
	
	/** 
	 * Clear the image removing the already detected circles from the source image
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> circles = circlesAndEllipses.get(Circle.class.getSimpleName());
		Image image = (Image)context.get(UseCaseContext.IMAGE);
		Graphics graphic = (Graphics) image.getImage().getGraphics();
		graphic.setColor(Color.WHITE);
		
		for(Geometry form : circles){
			Circle c = (Circle) form;

	    	graphic.fillOval((int)(c.getCenter().getX().intValue() - c.getRadius() - CLEAR_AREA),
    				(int)(c.getCenter().getY().intValue() - c.getRadius()- CLEAR_AREA),
    				(int)(c.getRadius() + CLEAR_AREA) * 2, 
    				(int)(c.getRadius() + CLEAR_AREA) * 2);
			
		}
		return false;
	}
	
}
