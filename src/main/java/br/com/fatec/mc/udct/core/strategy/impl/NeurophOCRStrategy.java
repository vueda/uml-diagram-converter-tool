/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 15/09/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy.impl;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import net.sourceforge.javaocr.ocrPlugins.CharacterExtractor;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.imgrec.image.ImageJ2SE;
import org.neuroph.ocr.OcrPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.strategy.IOCRStrategy;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.image.ImageConverterUtils;
import br.com.fatec.mc.udct.utils.preferences.Preferences;
import br.com.fatec.mc.udct.utils.preferences.PreferencesManager;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * OCR strategy using the Neuroph framework. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 15/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class NeurophOCRStrategy implements IOCRStrategy{
	private Logger logger = LoggerFactory.getLogger(NeurophOCRStrategy.class);
	@SuppressWarnings("rawtypes")
	private NeuralNetwork neuralNetwork;
	private final int CHAR_WIDTH = 35;
	private final int CHAR_HEIGHT = 35;
	private final int DETECT_CHAR_WIDTH = 40;
	private final int DETECT_CHAR_HEIGHT = 40;
	
	private final String TEMP_DIR = "./temp";

	/** 
	 * Recognize characters in the given image
	 * @param image - {@link Image}: Image with characters
	 * @return {@link String} - Recognized string
	 * @see br.com.fatec.mc.udct.core.strategy.IOCRStrategy#doRecognition(br.com.fatec.mc.udct.domain.image.Image)
	 */
	@Override
	public String doRecognition(Image image) {
		String neuralNetworkFile = PreferencesManager.getPreferences().get(Preferences.NEURAL_NETWORK, "");
		createNeuralNetwork(neuralNetworkFile);
		OcrPlugin ocrPlugin = (OcrPlugin) neuralNetwork.getPlugin(OcrPlugin.class);
		List<BufferedImage> charImages = cutCharacters(image.getImage());
		StringBuilder desc = new StringBuilder();
		for(BufferedImage character : charImages){//Interpret the char images
			BufferedImage resizedImage = new BufferedImage(DETECT_CHAR_WIDTH, DETECT_CHAR_HEIGHT, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = resizedImage.createGraphics();
			g.drawImage(character, 0, 0, DETECT_CHAR_WIDTH, DETECT_CHAR_HEIGHT, null);
			g.dispose();
			Image charImg = ImageConverterUtils.image2BinaryWhiteEdge(new Image("", resizedImage), 180);
			Character ch = ocrPlugin.recognizeCharacter(new ImageJ2SE(charImg.getImage()));
			desc.append(ch);
		}

		return desc.toString();
	}
	
	/** 
	 *Creates a {@link NeuralNetwork} with the given file
	 * @param neuralNetworkFile - {@link String} .nnet file path
	 */
	private void createNeuralNetwork(String neuralNetworkFile){
		if(neuralNetwork == null){
			if(neuralNetworkFile.isEmpty() || !neuralNetworkFile.endsWith(".nnet")){
				neuralNetwork = NeuralNetwork.createFromFile("src/main/resources/HNN.nnet");
			}else{
				try{
					neuralNetwork = NeuralNetwork.createFromFile(neuralNetworkFile);
				}catch(Exception e){
					neuralNetwork = NeuralNetwork.createFromFile("src/main/resources/HNN.nnet");
				}
			}
		}
	}

	/** 
	 * Cut the characters in the given image
	 * @param textImage - {@link BufferedImage} with the characters
	 * @return {@link List} of {@link BufferedImage} of each character
	 */
	private List<BufferedImage> cutCharacters(BufferedImage textImage){
		List<BufferedImage> charImages = new ArrayList<BufferedImage>();
		File outputfile = new File("tempImage.png");

		try {
			Image binImage = ImageConverterUtils.image2BinaryWhiteEdge(new Image("", textImage), 180);
			ImageIO.write(binImage.getImage(), "png", outputfile);
			CharacterExtractor slicer = new CharacterExtractor();
			File tempDir =  new File(TEMP_DIR);
			if(tempDir.exists()){
				delete(tempDir);
			}
			tempDir.mkdirs();
			slicer.slice(outputfile, tempDir,CHAR_WIDTH, CHAR_HEIGHT);

			for(File file : tempDir.listFiles()){
				BufferedImage charImage = ImageIO.read(file);
				charImages.add(charImage);
			}
			
			delete(tempDir);//Delete the temporary directory and all it's files
			outputfile.delete();
		} catch (IOException e) {
			logger.error("Failed to write file", e);
		}

		return charImages;
	}

	/** 
	 * Delete files/directories recursively
	 * @param file {@link File}: File or directory
	 * @throws IOException
	 */
	private void delete(File file) throws IOException{	 
		if(file.isDirectory()){//If directory is empty delete
			if(file.list().length==0){
				file.delete();
			}else{

				String files[] = file.list();//Get all files
				for (String temp : files) {
					File fileDelete = new File(file, temp);
					delete(fileDelete);//Recursive delete
				}

				if(file.list().length==0){
					file.delete();
				}
			}

		}else{
			file.delete();
		}
	}

}
