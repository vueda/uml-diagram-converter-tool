/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 08/09/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.StandardLine;
import br.com.fatec.mc.udct.utils.DistanceCalculatorUtil;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Validates the precision rates of the detected lines comparing the distance between the points and the
 * detected line. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 08/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class LineRateValidator implements Command {

	private final double PRECISION_LINE = 10;
	/** 
	 * Validates the precision rates of detected lines
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		List<StandardLine> lines = (List<StandardLine>) context.get(UseCaseContext.LINES);
		List<StandardLine> lines2Remove = new ArrayList<StandardLine>();

		for(StandardLine line:lines){
			double distance = 0;
			for(Coordinate2D c:line.getLinePoints()){
				distance += DistanceCalculatorUtil.getDistanceToSegment(line.getFirstPoint(), line.getLastPoint(), c);
			}
			System.out.println(distance/line.getLinePoints().size());
			if(distance/line.getLinePoints().size() > PRECISION_LINE){
				lines2Remove.add(line);
			}
		}
		lines.removeAll(lines2Remove);
		return false;
	}

}
