/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 19/06/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import web.unbc.ca.Circle;
import web.unbc.ca.Ellipse;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.image.ImageFormPrintUtils;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Draws the detected forms in the source image. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 19/06/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class DrawImages implements Command {

	/** 
	 * Draws the detected forms in the image
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Image image = (Image) context.get(UseCaseContext.IMAGE);
		Map<String,List<Geometry>> result = (Map<String, List<Geometry>>) context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> circles = result.get(Circle.class.getSimpleName());
		List<Geometry> ellipses = result.get(Ellipse.class.getSimpleName());
		List<Line> lines = (List<Line>) context.get(UseCaseContext.LINES);
		
		ImageFormPrintUtils.printCircles(circles, image);
		ImageFormPrintUtils.printEllipses(ellipses, image);		
		ImageFormPrintUtils.printLines(lines,image); 
		return true;
	}

}
