/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 29/09/2013<br>
 */

package br.com.fatec.mc.udct.utils;

import java.util.List;

import org.apache.commons.math3.stat.regression.RegressionResults;
import org.apache.commons.math3.stat.regression.SimpleRegression;

import web.unbc.ca.Coordinate;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.LineType;
import br.com.fatec.mc.udct.domain.geometry.StandardLine;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Utility class that provides methods to connect points
 * that are part of a same segment. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 29/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class CoordinateConnectorUtil {
	
	/**
	 * Represents the maximum distance between points of the same line
	 * A line with gaps is a dashed line
	 */
	private static final int MAX_GAP_SIZE = 35;
	private static final int MIN_DIST_GAP = 5;

	private static int gaps = 0;
	
	private static Coordinate elementCenter;
	
	/** 
	 * Create a new {@link StandardLine} based on the given segment
	 * @param partialSegment - {@link List} of {@link Coordinate2D} that represents the line
	 * @return {@link StandardLine}
	 */
	public static StandardLine createLine(List<Coordinate2D> partialSegment){
		StandardLine line = new StandardLine();		
		line.setLinePoints(partialSegment);
		
		SimpleRegression reg = new SimpleRegression();
		for(int i = 0 ; i < partialSegment.size() ; i++){
			Coordinate2D coord = partialSegment.get(i);
			reg.addData(coord.getX(), coord.getY());
		}		
		
		RegressionResults res = reg.regress();
		line.setAngularCoefficient(res.getParameterEstimate(1));
		line.setLinearCoefficient(res.getParameterEstimate(0));
		if(gaps > 3){
			line.setLineType(LineType.DASHED);
		}else{
			line.setLineType(LineType.FULL);
		}
		
		return line;
	}
	

	/** 
	 * Check gaps in a given segment extracted from an image
	 * @param partialSegment - {@link List} of {@link Coordinate2D} (segment)
	 * @param image - Source image
	 * @return {@link Coordinate2D}: Next coordinate of the segment. If there is no next point returns <code>null</code>
	 */
	private static Coordinate2D gapCheck(List<Coordinate2D> partialSegment, Image image){		
		//Needs to check if there is a gap here or if it's the end of the line
		Coordinate2D start = partialSegment.get(0);
		Coordinate2D end = partialSegment.get(partialSegment.size() - 1);

		//Distance between the points, x and y variation
		double distance = start.distance2Coordinate(end);
		int xVariation =  end.getX() - start.getX();
		int yVariation =  end.getY() - start.getY();
		double variation = Math.abs(xVariation / distance);
		variation = variation < 1 ? 1 : variation;

		if(xVariation > 0){//The line is going to the right side
			int xFinal = end.getX() + MAX_GAP_SIZE < image.getWidth()? end.getX() + MAX_GAP_SIZE : image.getWidth() - 1;
			for(int i = end.getX() ; i < xFinal; i++){
				if(yVariation > 0){//Going down
					int yHeight = (int) ((end.getY() + (variation * MAX_GAP_SIZE)) < image.getHeight()? 
							(end.getY() + (variation * MAX_GAP_SIZE)): image.getHeight() - 1);
					for(int j = end.getY() ; j < yHeight ; j ++){
						if(image.getR(i, j) != 255){
							image.setRGB(i, j, 255, 255, 255);
							return new Coordinate2D(i, j);
						}
					}
				}else if(yVariation < 0){//Going up
					int yHeight = (int) ((end.getY() - (variation * MAX_GAP_SIZE)) > 0? 
							(end.getY() - (variation * MAX_GAP_SIZE)): 0);
					for(int j = end.getY() ; j > yHeight ; j --){
						if(image.getR(i, j) != 255){
							image.setRGB(i, j, 255, 255, 255);
							return new Coordinate2D(i, j);
						}
					}
				}else{//Going forward
					int yVal = end.getY() - 5 > 0 ?end.getY() - 5 : 0;
					int yFinal = end.getY() + 5 < image.getHeight() ? end.getY() + 5 : image.getHeight() - 1;
					for(int j = yVal ; j < yFinal ; j ++){
						if(image.getR(i, j) != 255){
							image.setRGB(i, j, 255, 255, 255);
							return new Coordinate2D(i, j);
						}
					}
				}
			}
		}else if(xVariation < 0){//The line is going to the left side
			int xFinal = end.getX() - MAX_GAP_SIZE > 0? end.getX() - MAX_GAP_SIZE : 0;
			for(int i = end.getX() ; i > xFinal; i--){
				if(yVariation > 0){//Going down
					int yHeight = (int) ((end.getY() + (variation * MAX_GAP_SIZE)) < image.getHeight()? 
							(end.getY() + (variation * MAX_GAP_SIZE)): image.getHeight() - 1);
					for(int j = end.getY() ; j < yHeight ; j ++){
						if(image.getR(i, j) != 255){
							image.setRGB(i, j, 255, 255, 255);
							return new Coordinate2D(i, j);
						}
					}
				}else if(yVariation < 0){//Going up
					int yHeight = (int) ((end.getY() - (variation * MAX_GAP_SIZE)) > 0? 
							(end.getY() - (variation * MAX_GAP_SIZE)): 0);
					for(int j = end.getY() ; j > yHeight ; j --){
						if(image.getR(i, j) != 255){
							image.setRGB(i, j, 255, 255, 255);
							return new Coordinate2D(i, j);
						}
					}
				}else{//Going backward
					int yVal = end.getY() - 5 > 0 ?end.getY() - 5 : 0;
					int yFinal = end.getY() + 5 < image.getHeight() ? end.getY() + 5 : image.getHeight() - 1;
					for(int j = yVal ; j < yFinal ; j ++){
						if(image.getR(i, j) != 255){
							image.setRGB(i, j, 255, 255, 255);
							return new Coordinate2D(i, j);
						}
					}
				}
			}

		}else{//If there is no xVariation the line is vertical
			if(yVariation > 0){//Going down
				int yHeight = (int) ((end.getY() + (variation * MAX_GAP_SIZE)) < image.getHeight()? 
						(end.getY() + (variation * MAX_GAP_SIZE)): image.getHeight() - 1);
				for(int j = end.getY() ; j < yHeight ; j ++){
					if(image.getR(end.getX(), j) != 255){
						image.setRGB(end.getX(), j, 255, 255, 255);
						return new Coordinate2D(end.getX(), j);
					}
				}
			}else if(yVariation < 0){//Going up
				int yHeight = (int) ((end.getY() - (variation * MAX_GAP_SIZE)) > 0? 
						(end.getY() - (variation * MAX_GAP_SIZE)): 0);
				for(int j = end.getY() ; j > yHeight ; j --){
					if(image.getR(end.getX(), j) != 255){
						image.setRGB(end.getX(), j, 255, 255, 255);
						return new Coordinate2D(end.getX(), j);
					}
				}
			}else{//Zero variation
				int y = elementCenter.getY();
				if(y - start.getY() < 0){//Goes down
					int yHeight = (int) ((end.getY() + (variation * MAX_GAP_SIZE)) < image.getHeight()? 
							(end.getY() + (variation * MAX_GAP_SIZE)): image.getHeight() - 1);
					for(int j = end.getY() ; j < yHeight ; j ++){
						if(image.getR(end.getX(), j) != 255){
							image.setRGB(end.getX(), j, 255, 255, 255);
							return new Coordinate2D(end.getX(), j);
						}
					}
				}else{//goes up
					int yHeight = (int) ((end.getY() - (variation * MAX_GAP_SIZE)) > 0? 
							(end.getY() - (variation * MAX_GAP_SIZE)): 0);
					for(int j = end.getY() ; j > yHeight ; j --){
						if(image.getR(end.getX(), j) != 255){
							image.setRGB(end.getX(), j, 255, 255, 255);
							return new Coordinate2D(end.getX(), j);
						}
					}
				}

			}
		}
		
		return null;
	}
	
	/**
	 * Will look a the specified points (marked with X). The 0 point is the coordinate marked
	 * by the informed parameters.<br>
	 *	 . . X<br>
	 * 	 . 0 X<br>		 
	 * 	 X X X<br>
	 * @param x
	 * @param y
	 */
	public static void checkRightDownSide(int x , int y, Image image, List<Coordinate2D> partialSegment){
		if(x - 1 > 0 && y + 1 < image.getHeight()){
			if(image.getR(x - 1 , y + 1) != 255){//Left-down
				partialSegment.add(new Coordinate2D(x - 1 , y + 1));
				image.setRGB(x - 1, y + 1, 255, 255, 255);		
				checkRightDownSide(x - 1, y + 1, image, partialSegment);
			}
		}

		if(y + 1 < image.getHeight()){
			if(image.getR(x , y + 1) != 255){//down
				partialSegment.add(new Coordinate2D(x , y + 1));
				image.setRGB(x , y + 1, 255, 255, 255);			
				checkRightDownSide(x , y + 1, image, partialSegment);
			}
		}
		if(x + 1 < image.getWidth() && y + 1  < image.getHeight()){
			if(image.getR(x + 1 , y + 1) != 255){//right-down
				partialSegment.add(new Coordinate2D(x + 1 , y + 1));
				image.setRGB(x + 1, y + 1, 255, 255, 255);		
				checkRightDownSide(x + 1, y + 1, image, partialSegment);
			}	
		}

		if(x + 1 < image.getWidth()){//right
			if(image.getR(x + 1, y) != 255){
				partialSegment.add(new Coordinate2D(x + 1 , y));
				image.setRGB(x + 1, y, 255, 255, 255);		
				checkRightDownSide(x + 1, y, image, partialSegment);
			}	
		}

		if(x + 1 < image.getWidth() && y - 1 > 0){
			if(image.getR(x + 1, y - 1) != 255){//right-up
				partialSegment.add(new Coordinate2D(x + 1 , y - 1));
				image.setRGB(x + 1, y - 1, 255, 255, 255);		
				checkRightDownSide(x + 1, y - 1, image, partialSegment);
			}	
		}
		
		Coordinate2D nextCoord = gapCheck(partialSegment,image);
		if(nextCoord != null){//If exists a gap continue from the next point
			//Counts how many gaps to classify the line
			Coordinate2D lastCoord = partialSegment.get(partialSegment.size() - 1);
			if(Math.floor(nextCoord.distance2Coordinate(lastCoord)) > MIN_DIST_GAP){
				gaps++;
			}
			checkRightDownSide(nextCoord.getX(), nextCoord.getY(), image, partialSegment);
		}
			
	}

	/**
	 * Will look a the specified points (marked with X). The 0 point is the coordinate marked
	 * by the informed parameters.<br>
	 *	 X . .<br>
	 * 	 X 0 .<br>		 
	 * 	 X X X<br>
	 * @param x
	 * @param y
	 */
	public static void checkLeftDownSide(int x , int y, Image image, List<Coordinate2D> partialSegment){
		if(x - 1 > 0 && y - 1 > 0){
			if(image.getR(x - 1, y - 1) != 255){
				partialSegment.add(new Coordinate2D(x - 1, y - 1));
				image.setRGB(x - 1, y - 1, 255, 255, 255);
				checkLeftDownSide(x - 1, y - 1, image, partialSegment);
			}
		}
		if(x - 1 > 0){
			if(image.getR(x - 1 , y) != 255){
				partialSegment.add(new Coordinate2D(x - 1, y));
				image.setRGB(x - 1, y, 255, 255, 255);			
				checkLeftDownSide(x - 1, y , image, partialSegment);
			}
		}
		if(x - 1 > 0 && y + 1 < image.getHeight()){
			if(image.getR(x - 1 , y + 1) != 255){
				partialSegment.add(new Coordinate2D(x - 1 , y + 1));
				image.setRGB(x - 1, y + 1, 255, 255, 255);		
				checkLeftDownSide(x - 1, y + 1, image, partialSegment);
			}
		}

		if(y + 1 < image.getHeight()){
			if(image.getR(x , y + 1) != 255){
				partialSegment.add(new Coordinate2D(x , y + 1));
				image.setRGB(x , y + 1, 255, 255, 255);			
				checkLeftDownSide(x , y + 1, image, partialSegment);
			}
		}
		if(x + 1 < image.getWidth() && y + 1  < image.getHeight()){
			if(image.getR(x + 1 , y + 1) != 255){
				partialSegment.add(new Coordinate2D(x + 1 , y + 1));
				image.setRGB(x + 1, y + 1, 255, 255, 255);		
				checkLeftDownSide(x + 1, y + 1, image, partialSegment);
			}	
		}
		
		Coordinate2D nextCoord = gapCheck(partialSegment,image);
		if(nextCoord != null){//If exists a gap continue from the next point
			//Counts how many gaps to classify the line
			Coordinate2D lastCoord = partialSegment.get(partialSegment.size() - 1);
			if(nextCoord.distance2Coordinate(lastCoord) > MIN_DIST_GAP){
				gaps++;
			}
			checkLeftDownSide(nextCoord.getX(), nextCoord.getY(), image, partialSegment);
		}
			
	}

	/**
	 * Will look a the specified points (marked with X). The 0 point is the coordinate marked
	 * by the informed parameters.<br>
	 *	 X X X<br>
	 * 	 X 0 .<br>		 
	 * 	 X . .<br>
	 * @param x
	 * @param y
	 */
	public static void checkLeftUpperSide(int x , int y, Image image, List<Coordinate2D> partialSegment){
		if(x - 1 > 0 && y - 1 > 0){
			if(image.getR(x - 1, y - 1) != 255){
				partialSegment.add(new Coordinate2D(x - 1, y - 1));
				image.setRGB(x - 1, y - 1, 255, 255, 255);
				checkLeftUpperSide(x - 1, y - 1, image, partialSegment);
			}
		}
		if(y - 1 > 0){
			if(image.getR(x , y - 1) != 255){
				partialSegment.add(new Coordinate2D(x , y - 1));
				image.setRGB(x, y - 1, 255, 255, 255);
				checkLeftUpperSide(x , y - 1, image, partialSegment);
			}
		}
		if(x + 1 < image.getWidth() && y - 1 > 0){
			if(image.getR(x + 1 , y - 1) != 255){
				partialSegment.add(new Coordinate2D(x + 1 , y - 1));
				image.setRGB(x + 1, y - 1, 255, 255, 255);		
				checkLeftUpperSide(x + 1, y - 1, image, partialSegment);
			}
		}

		if(x - 1 > 0){
			if(image.getR(x - 1 , y) != 255){
				partialSegment.add(new Coordinate2D(x - 1, y));
				image.setRGB(x - 1, y, 255, 255, 255);			
				checkLeftUpperSide(x - 1, y , image, partialSegment);
			}
		}
		if(x - 1 > 0 && y + 1 < image.getHeight()){
			if(image.getR(x - 1 , y + 1) != 255){
				partialSegment.add(new Coordinate2D(x - 1 , y + 1));
				image.setRGB(x - 1, y + 1, 255, 255, 255);		
				checkLeftUpperSide(x - 1, y + 1, image, partialSegment);
			}	
		}
		
		Coordinate2D nextCoord = gapCheck(partialSegment,image);
		if(nextCoord != null){//If exists a gap continue from the next point
			//Counts how many gaps to classify the line
			Coordinate2D lastCoord = partialSegment.get(partialSegment.size() - 1);
			if(nextCoord.distance2Coordinate(lastCoord) > MIN_DIST_GAP){
				gaps++;
			}
			checkLeftUpperSide(nextCoord.getX(), nextCoord.getY(), image, partialSegment);
		}
			
	}

	/**
	 * Will look a the specified points (marked with X). The 0 point is the coordinate marked
	 * by the informed parameters
	 *	 X X X
	 * 	 . 0 X		 
	 * 	 . . X
	 * @param x
	 * @param y
	 */
	public static void checkRightUpperSide(int x , int y, Image image, List<Coordinate2D> partialSegment){

		if(x - 1 > 0 && y - 1 > 0){
			if(image.getR(x - 1, y - 1) != 255){
				partialSegment.add(new Coordinate2D(x - 1, y - 1));
				image.setRGB(x - 1, y - 1, 255, 255, 255);
				checkRightUpperSide(x - 1, y - 1, image, partialSegment);
			}
		}
		if(y - 1 > 0){
			if(image.getR(x , y - 1) != 255){
				partialSegment.add(new Coordinate2D(x , y - 1));
				image.setRGB(x, y - 1, 255, 255, 255);
				checkRightUpperSide(x , y - 1, image, partialSegment);
			}
		}
		if(x + 1 < image.getWidth() && y - 1 > 0){
			if(image.getR(x + 1 , y - 1) != 255){
				partialSegment.add(new Coordinate2D(x + 1 , y - 1));
				image.setRGB(x + 1, y - 1, 255, 255, 255);		
				checkRightUpperSide(x + 1, y - 1, image, partialSegment);
			}
		}
		if(x + 1 < image.getWidth()){
			if(image.getR(x + 1 , y) != 255){
				partialSegment.add(new Coordinate2D(x + 1, y));
				image.setRGB(x + 1, y, 255, 255, 255);			
				checkRightUpperSide(x + 1, y , image, partialSegment);
			}
		}
		if(x + 1 < image.getWidth() && y + 1 < image.getHeight()){
			if(image.getR(x + 1 , y + 1) != 255){
				partialSegment.add(new Coordinate2D(x + 1 , y + 1));
				image.setRGB(x + 1, y + 1, 255, 255, 255);		
				checkRightUpperSide(x + 1, y + 1, image, partialSegment);
			}	
		}
		
		Coordinate2D nextCoord = gapCheck(partialSegment,image);
		if(nextCoord != null){//If exists a gap continue from the next point
			//Counts how many gaps to classify the line
			Coordinate2D lastCoord = partialSegment.get(partialSegment.size() - 1);
			if(nextCoord.distance2Coordinate(lastCoord) > MIN_DIST_GAP){
				gaps++;
			}
			checkRightUpperSide(nextCoord.getX(), nextCoord.getY(), image, partialSegment);
		}
			
	}

	/**
	 * Will look a the specified points (marked with X). The 0 point is the coordinate marked
	 * by the informed parameters
	 *	 X X X
	 * 	 . 0 .		 
	 * 	 . . .
	 * @param x
	 * @param y
	 */
	public static void checkUpperSide(int x , int y, Image image, List<Coordinate2D> partialSegment){

		if(x - 1 > 0 && y - 1 > 0){
			if(image.getR(x - 1, y - 1) != 255){
				partialSegment.add(new Coordinate2D(x - 1, y - 1));
				image.setRGB(x - 1, y - 1, 255, 255, 255);
				checkUpperSide(x - 1, y - 1, image, partialSegment);
			}
		}

		if(y - 1 > 0){
			if(image.getR(x , y - 1) != 255){
				partialSegment.add(new Coordinate2D(x , y - 1));
				image.setRGB(x , y - 1, 255, 255, 255);
				checkUpperSide(x , y - 1, image, partialSegment);
			}
		}

		if(x + 1 < image.getWidth() && y - 1 > 0){
			if(image.getR(x + 1 , y - 1) != 255){
				partialSegment.add(new Coordinate2D(x + 1 , y - 1));
				image.setRGB(x + 1, y - 1, 255, 255, 255);
				checkUpperSide(x + 1, y - 1, image, partialSegment);
			}
		}
		
		Coordinate2D nextCoord = gapCheck(partialSegment,image);
		if(nextCoord != null){//If exists a gap continue from the next point
			//Counts how many gaps to classify the line
			Coordinate2D lastCoord = partialSegment.get(partialSegment.size() - 1);
			if(nextCoord.distance2Coordinate(lastCoord) > MIN_DIST_GAP){
				gaps++;
			}
			checkUpperSide(nextCoord.getX(), nextCoord.getY(), image, partialSegment);
		}
			
	}

	/**
	 * Will look a the specified points (marked with X). The 0 point is the coordinate marked
	 * by the informed parameters
	 *	 . . .
	 * 	 . 0 .		 
	 * 	 X X X
	 * @param x
	 * @param y
	 */
	public static void checkDownSide(int x , int y, Image image, List<Coordinate2D> partialSegment){

		if(x - 1 > 0 && y + 1 < image.getHeight()){
			if(image.getR(x - 1, y + 1) != 255){
				partialSegment.add(new Coordinate2D(x - 1, y + 1));
				image.setRGB(x - 1, y + 1, 255, 255, 255);
				checkDownSide(x - 1, y + 1, image, partialSegment);
			}
		}

		if(y + 1 < image.getHeight()){
			if(image.getR(x , y + 1) != 255){
				partialSegment.add(new Coordinate2D(x , y + 1));
				image.setRGB(x , y + 1, 255, 255, 255);
				checkDownSide(x , y + 1, image, partialSegment);
			}
		}

		if(x + 1 < image.getWidth() && y + 1 < image.getHeight()){
			if(image.getR(x + 1 , y + 1) != 255){
				partialSegment.add(new Coordinate2D(x + 1 , y + 1));
				image.setRGB(x + 1, y + 1, 255, 255, 255);
				checkDownSide(x + 1, y + 1, image, partialSegment);
			}
		}
		
		Coordinate2D nextCoord = gapCheck(partialSegment,image);
		if(nextCoord != null){//If exists a gap continue from the next point
			//Counts how many gaps to classify the line
			Coordinate2D lastCoord = partialSegment.get(partialSegment.size() - 1);
			if(nextCoord.distance2Coordinate(lastCoord) > MIN_DIST_GAP){
				gaps++;
			}
			checkDownSide(nextCoord.getX(), nextCoord.getY(), image, partialSegment);
		}
			
	}

	/**
	 * Will look a the specified points (marked with X). The 0 point is the coordinate marked
	 * by the informed parameters
	 *	 X . .
	 * 	 X 0 .		 
	 * 	 X . .
	 * @param x
	 * @param y
	 */
	public static void checkLeftSide(int x , int y, Image image, List<Coordinate2D> partialSegment){

		if(x - 1 > 0 && y - 1 > 0){
			if(image.getR(x - 1, y - 1) != 255){
				partialSegment.add(new Coordinate2D(x - 1, y - 1));
				image.setRGB(x - 1, y - 1, 255, 255, 255);
				checkLeftSide(x - 1, y - 1, image, partialSegment);
			}
		}

		if(x - 1 > 0){
			if(image.getR(x - 1 , y ) != 255){
				partialSegment.add(new Coordinate2D(x - 1, y ));
				image.setRGB(x - 1 , y , 255, 255, 255);
				checkLeftSide(x - 1, y , image, partialSegment);
			}
		}

		if(x - 1 > 0 && y + 1 < image.getHeight()){
			if(image.getR(x - 1 , y + 1) != 255){
				partialSegment.add(new Coordinate2D(x - 1 , y + 1));
				image.setRGB(x - 1, y + 1, 255, 255, 255);
				checkLeftSide(x - 1, y + 1, image, partialSegment);
			}
		}
		Coordinate2D nextCoord = gapCheck(partialSegment,image);
		if(nextCoord != null){//If exists a gap continue from the next point
			//Counts how many gaps to classify the line
			Coordinate2D lastCoord = partialSegment.get(partialSegment.size() - 1);
			if(nextCoord.distance2Coordinate(lastCoord) > MIN_DIST_GAP){
				gaps++;
			}
			checkLeftSide(nextCoord.getX(), nextCoord.getY(), image, partialSegment);
		}
			
	}

	/**
	 * Will look a the specified points (marked with X). The 0 point is the coordinate marked
	 * by the informed parameters
	 *	 . . X
	 * 	 . 0 X		 
	 * 	 . . X
	 * @param x
	 * @param y
	 */
	public static void checkRightSide(int x , int y, Image image, List<Coordinate2D> partialSegment){

		if(x + 1 < image.getWidth() && y - 1 > 0){
			if(image.getR(x + 1, y - 1) != 255){
				partialSegment.add(new Coordinate2D(x + 1, y - 1));
				image.setRGB(x + 1, y - 1, 255, 255, 255);
				checkRightSide(x + 1, y - 1, image, partialSegment);
			}
		}

		if(x + 1 < image.getWidth()){
			if(image.getR(x + 1 , y ) != 255){
				partialSegment.add(new Coordinate2D(x + 1, y ));
				image.setRGB(x + 1 , y , 255, 255, 255);
				checkRightSide(x + 1, y , image, partialSegment);
			}
		}

		if(x + 1 < image.getWidth() && y + 1 < image.getHeight()){
			if(image.getR(x + 1 , y + 1) != 255){
				partialSegment.add(new Coordinate2D(x + 1 , y + 1));
				image.setRGB(x + 1, y + 1, 255, 255, 255);
				checkRightSide(x + 1, y + 1, image, partialSegment);
			}
		}
		Coordinate2D nextCoord = gapCheck(partialSegment,image);
		if(nextCoord != null){//If exists a gap continue from the next point
			//Counts how many gaps to classify the line
			Coordinate2D lastCoord = partialSegment.get(partialSegment.size() - 1);
			if(nextCoord.distance2Coordinate(lastCoord) > MIN_DIST_GAP){
				gaps++;
			}
			checkRightSide(nextCoord.getX(), nextCoord.getY(), image, partialSegment);
		}
			
	}
	
	/**
	 * Setter method of gaps
	 * @param gaps
	 */
	public static void setGaps(int gaps) {
		CoordinateConnectorUtil.gaps = gaps;
	}

	/**
	 * Setter method of elementCenter
	 *
	 * @param elementCenter Class attribute
	 */
	public static void setElementCenter(Coordinate elementCenter) {
		CoordinateConnectorUtil.elementCenter = elementCenter;
	}
}
