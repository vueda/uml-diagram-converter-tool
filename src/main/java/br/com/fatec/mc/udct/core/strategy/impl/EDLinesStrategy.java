/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.stat.regression.RegressionResults;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.strategy.ILineDetectionStrategy;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.geometry.StandardLine;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.DistanceCalculatorUtil;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Strategy of Lines Detection.<br>
 * Implementation based on: <i>http://ceng.anadolu.edu.tr/cv/EDLines/</i><br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class EDLinesStrategy implements ILineDetectionStrategy {
	Logger logger = LoggerFactory.getLogger(EDLinesStrategy.class);

	/**
	 * Line minimum size
	 */
	int LINE_MIN_SIZE = 5;
	
	/**
	 * Line angular coefficient
	 */
	double angularCoefficient = 0;
	
	/**
	 * Line linear coefficient
	 */
	double linearCoefficient = 0;
	
	/** 
	 * Detect lines in the image using the EDLines strategy
	 * @param diagramImage {@link Image}:Image where the lines will be searched
	 * @return {@link List} of {@link Line} detected
	 * @see br.com.fatec.mc.udct.core.strategy.ILineDetectionStrategy#detectLines(br.com.fatec.mc.udct.domain.image.Image)
	 */
	public List<Line> detectLines(List<List<Coordinate2D>> listSegments, Image diagramImage) {
		List<Line> lines = new ArrayList<Line>();
		
		logger.info("Calculating line minimum size.");
		LINE_MIN_SIZE = lineMinSize(diagramImage);

		for(List<Coordinate2D> segment:listSegments){
			lineFitMethod(segment, lines,listSegments.indexOf(segment));
		}
		
		return lines;
	}
	
	/** 
	 * Calculates the line that best fit the given points
	 * @param segment - Segment
	 * @param lines - Actual detected lines
	 * @param segmentIndex - Index of segment
	 * @return
	 */
	private boolean lineFitMethod(List<Coordinate2D> segment, List<Line> lines, int segmentIndex){		
		double lineFitError = 2;
		while(segment.size() > LINE_MIN_SIZE){
			lineFitError = leastSquaresLineFitMethod(segment, LINE_MIN_SIZE);
			if(lineFitError <= 1.0){
				break;
			}
			segment.remove(0);
		}
		
		if(lineFitError > 1.0)
			return false;
		
		int lineLen = LINE_MIN_SIZE;
		
		while(lineLen < segment.size()){	
			double distance = DistanceCalculatorUtil.getDistanceToSegment(segment.get(0), segment.get(lineLen - 1), segment.get(lineLen));
			
			if(distance > 1.0){
				break;
			}
			lineLen++;
		}
		
		leastSquaresLineFitMethod(segment, lineLen);

		StandardLine line = new StandardLine();
		line.setAngularCoefficient(angularCoefficient);
		line.setLinearCoefficient(linearCoefficient);
		for(int i = 0 ; i < lineLen ; i++){
			line.getLinePoints().add(segment.get(i));
		}
		line.setId(Long.valueOf(segmentIndex));
		lines.add(line);
		
		
		if(lineLen < segment.size())		
			lineFitMethod(segment.subList(lineLen, segment.size()), lines, segmentIndex);
		
		return true;
	}
	
	/** 
	 * Calculates the line with the lesat squares method
	 * @param coordinates - Segment
	 * @param size - Segment size
	 * @return {@link Double}: Least square error
	 */
	private double leastSquaresLineFitMethod(List<Coordinate2D> coordinates, int size){
		
		SimpleRegression reg = new SimpleRegression();
		for(int i = 0 ; i < size ; i++){
			Coordinate2D coord = coordinates.get(i);
			reg.addData(coord.getX(), coord.getY());
		}		
		
		RegressionResults res = reg.regress();

		linearCoefficient = res.getParameterEstimate(0);
		angularCoefficient = res.getParameterEstimate(1);
		
		return Math.floor(res.getMeanSquareError());
	}
		
	/** 
	 * Calculates the minimum size of the segment
	 * @param image - {@link Image}
	 * @return {@link Integer} - minimum size of the line
	 */
	private int lineMinSize(Image image){
		int width = image.getWidth();
		int height = image.getHeight();
		return (int) ((-4 * Math.log10(width > height?width:height)) / Math.log(0.125));
	}

	@Override
	public List<Line> detectLines(Image diagramImage) {
		logger.info("NOT IMPLEMENTED BY THIS STRATEGY!!!");
		return null;
	}

}
