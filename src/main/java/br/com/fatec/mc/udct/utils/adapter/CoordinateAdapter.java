/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 02/06/2013<br>
 */

package br.com.fatec.mc.udct.utils.adapter;

import java.util.ArrayList;
import java.util.List;

import web.unbc.ca.Coordinate;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Coordinate adapter. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 02/06/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class CoordinateAdapter {

	/** 
	 * Adapts a {@link List} of {@link Coordinate2D} to a {@link List} of {@link Coordinate}
	 * @param coordinates - Source {@link Coordinate2D}
	 * @return resulting {@link Coordinate}
	 */
	public static List<Coordinate> adapt2Coordinate(List<Coordinate2D> coordinates){
		List<Coordinate> coords = new ArrayList<Coordinate>();
		for(Coordinate2D c:coordinates){
			coords.add(new Coordinate(c.getX(), c.getY()));
		}
		return coords;
	}
	
	/** 
	 * Adapts a single {@link Coordinate} to a {@link Coordinate2D}
	 * @param coordinate - {@link Coordinate} to be converted
	 * @return {@link Coordinate2D}
	 */
	public static Coordinate2D adapt2Coordinate2D(Coordinate coordinate){
		return new Coordinate2D(coordinate.getX(), coordinate.getY());
	}
	
	/** 
	 * Adapts a {@link Coordinate2D} to a {@link Coordinate}
	 * @param coordinate2D - {@link Coordinate2D}
	 * @return {@link Coordinate}
	 */
	public static Coordinate adapt2Coordinate(Coordinate2D coordinate2D){
		return new Coordinate(coordinate2D.getX(), coordinate2D.getY());
	}
}
