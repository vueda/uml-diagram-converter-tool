/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 15/09/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.core.strategy.IOCRStrategy;
import br.com.fatec.mc.udct.core.strategy.context.OCRContext;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Recognize the image characters. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 15/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class RecognizeCharacters implements Command {
	
	private Logger logger = LoggerFactory.getLogger(RecognizeCharacters.class);

	private OCRContext ocrContext;

	/** 
	 * Recognize characters in the images
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Class<IOCRStrategy> clazz = (Class<IOCRStrategy>) context.get(UseCaseContext.OCR_RECOGNITION_STRATEGY);
		ocrContext = new OCRContext(clazz.newInstance());
		Map<Geometry, List<Image>> texts = (Map<Geometry, List<Image>>) context.get(UseCaseContext.TEXTS);
		Map<Geometry, String> strings = (Map<Geometry, String>) context.get(UseCaseContext.STRINGS);
		
		//Map<Line, String> linesTexts = (Map<Line, String>) context.get(UseCaseContext.LINES_STRINGS);
		
		logger.info("Interpreting texts using the " + clazz.getSimpleName() + " strategy.");
		for(Geometry key : texts.keySet()){
			List<Image> formTexts = texts.get(key);
			StringBuilder description = new StringBuilder();
			for(Image img : formTexts){
				description.append(ocrContext.doRecognition(img)).append(System.getProperty("line.separator"));
			}
			strings.put(key, description.toString());
		}
		
		return false;
	}

}
