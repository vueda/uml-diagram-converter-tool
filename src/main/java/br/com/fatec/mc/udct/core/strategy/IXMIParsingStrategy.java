/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 21/08/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy;

import br.com.fatec.mc.udct.domain.usecase.UseCaseDiagram;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Interface of XMI parsing strategies. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 21/08/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public interface IXMIParsingStrategy {
	
	void doParse(UseCaseDiagram diagram) throws Exception;

}
