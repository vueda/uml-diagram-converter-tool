/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 01/09/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.StandardLine;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Do a final check on the detected lines removing similar ones. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 01/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class MergedLineCheck implements Command {

	private final double MAX_ANGLE = 10;
	private final double POINTS_DISTANCE = 5;

	/** 
	 * Validates the merged lines removing similar ones.
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		List<StandardLine> lines = (List<StandardLine>) context.get(UseCaseContext.LINES);
		List<StandardLine> lines2Remove = new ArrayList<StandardLine>();
		StandardLine ln = null;
		for(int i = 0 ; i < lines.size() - 1; i++){
			ln = (StandardLine) lines.get(i);
			for(int j = i + 1; j <lines.size() ; j++){	
				StandardLine ln2 = (StandardLine) lines.get(j);
				
				Coordinate2D s1 = ln.getFirstPoint();
				Coordinate2D e1 = ln.getLastPoint();

				Coordinate2D s2 = ln2.getFirstPoint();
				Coordinate2D e2 = ln2.getLastPoint();
				
				double alfa = calculateAngle(ln, ln2);		
				double angle = Math.abs(Math.toDegrees(Math.atan(alfa)));
				double startDistance = s1.distance2Coordinate(s2);
				double endDistance = e1.distance2Coordinate(e2);
				if(angle < MAX_ANGLE && 
						(startDistance < POINTS_DISTANCE || endDistance < POINTS_DISTANCE)){
					if(ln.getLinePoints().size() <= ln2.getLinePoints().size()){
						lines2Remove.add(ln);
					}else{
						lines2Remove.add(ln2);						
					}
				}				
			}
			lines.removeAll(lines2Remove);
		}
		return false;
	}
	
	private double calculateAngle(StandardLine firstLine, StandardLine secondLine){
		double alfa = 0;

		//Find the appropriate way to calculate the angle
		if(firstLine.getAngularCoefficient().equals(Double.NaN) 
				&& !secondLine.getAngularCoefficient().equals(Double.NaN)){
			alfa = 1/secondLine.getAngularCoefficient();
		}else if(!firstLine.getAngularCoefficient().equals(Double.NaN) 
				&& secondLine.getAngularCoefficient().equals(Double.NaN)){
			alfa = 1/firstLine.getAngularCoefficient();						
		}else if (!firstLine.getAngularCoefficient().equals(Double.NaN) 
				&& !secondLine.getAngularCoefficient().equals(Double.NaN)){
			alfa = Math.abs((firstLine.getAngularCoefficient() - secondLine.getAngularCoefficient()) 
					/ (1 + (firstLine.getAngularCoefficient() * secondLine.getAngularCoefficient())));
		}else{
			Coordinate2D line1Start = firstLine.getLinePoints().get(0);
			Coordinate2D line1End = firstLine.getLinePoints().get(firstLine.getLinePoints().size() - 1);
			Coordinate2D line2Start = secondLine.getLinePoints().get(0);
			Coordinate2D line2End = secondLine.getLinePoints().get(secondLine.getLinePoints().size() - 1);

			double angle1 = Math.atan2(line1Start.getY() - line1End.getY(),
					line1Start.getX() - line1End.getX());
			double angle2 = Math.atan2(line2Start.getY() - line2End.getY(),
					line2Start.getX() - line2End.getX());
			alfa = angle1-angle2;
		}

		return alfa;
	}

}
