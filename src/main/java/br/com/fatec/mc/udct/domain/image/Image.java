/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.image;

import java.awt.Color;
import java.awt.image.BufferedImage;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Domain class that represents an Image. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 28/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */
public class Image {
	private BufferedImage image;
	private String imageName;
	
	/** 
	 * Constructor that creates a new image with the width an height informed
	 * @param width {@link Integer}: The image width
	 * @param height {@link Integer}: The image height
	 */
	public Image(Integer width, Integer height){
		this.image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);		
	}
	
	/** 
	 * Constructor that creates an Image with the informed BufferedImage and Image name
	 * @param imageName {@link String}: The image name
	 * @param image {@link BufferedImage}: The BufferedImage
	 */
	public Image(String imageName, BufferedImage image){
		this.imageName = imageName;
		this.image = image;
	}
	
	/** 
	 * Returns the R pixel value. 
	 * @param x {@link Integer}: X coordinate of the pixel
	 * @param y {@link Integer}: Y coordinate of the pixel
	 * @return {@link Integer}: R value of the pixel
	 */
	public Integer getR(Integer x, Integer y) { 
		return(new Color(this.image.getRGB(x, y)).getRed()); 
	}
	
	/** 
	 * Returns the G pixel value. 
	 * @param x {@link Integer}: X coordinate of the pixel
	 * @param y {@link Integer}: Y coordinate of the pixel
	 * @return {@link Integer}: G value of the pixel
	 */
    public Integer getG(Integer x, Integer y) { 
    	return(new Color(this.image.getRGB(x, y)).getGreen()); 
    }
    
    /** 
	 * Returns the B pixel value. 
	 * @param x {@link Integer}: X coordinate of the pixel
	 * @param y {@link Integer}: Y coordinate of the pixel
	 * @return {@link Integer}: B value of the pixel
	 */
    public Integer getB(Integer x, Integer y) { 
    	return(new Color(this.image.getRGB(x, y)).getBlue()); 
    }
    
    /** 
     * Set the RGB value to the specified pixel
     * @param x {@link Integer}: X coordinate of the pixel
     * @param y {@link Integer}: Y coordinate of the pixel
     * @param R {@link Integer}: R value of the pixel
     * @param G {@link Integer}: G value of the pixel
     * @param B {@link Integer}: B value of the pixel
     */
    public void setRGB(Integer x, Integer y, Integer R, Integer G, Integer B) { 
    	this.image.setRGB(x, y, new Color(R, G, B).getRGB()); 
    }
    
    /** 
     * Returns the image width
     * @return {@link Integer}: The image width
     */
    public Integer getWidth(){
    	return this.image.getWidth();
    }
    
    /** 
     * Returns the image height
     * @return {@link Integer}: The image height
     */
    public Integer getHeight(){
    	return this.image.getHeight();
    }

	/** 
	 * Returns the image
	 * @return {@link BufferedImage}: The image
	 */
	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	/** 
	 * The image name
	 * @return {@link String} image name
	 */
	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
    
    

}
