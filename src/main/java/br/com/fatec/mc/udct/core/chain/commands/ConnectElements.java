/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 23/09/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Circle;
import web.unbc.ca.Ellipse;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.geometry.StandardLine;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.CoordinateConnectorUtil;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Connect elements (Circles and Ellipses) searching from segments through the image. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 23/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class ConnectElements implements Command {
	private Logger logger = LoggerFactory.getLogger(ConnectElements.class);

	private final int MIN_START_DIST = 20;

	/** 
	 * Connect elements (Circles and Ellipses) searching from segments through the image.
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> circles = circlesAndEllipses.get(Circle.class.getSimpleName());
		List<Geometry> ellipses = circlesAndEllipses.get(Ellipse.class.getSimpleName());
		Image image = (Image)context.get(UseCaseContext.IMAGE);
		
		List<Line> actorRelationships = new ArrayList<Line>();
		List<Line> useCaseRelationships = new ArrayList<Line>();
		
		logger.info("Searching for all actors relationships.");
		for(Geometry geom: circles){
			Circle circle = (Circle) geom;
			//Defines the search area
			int diameter = (int) (circle.getRadius() * 2);
			int xStart = (int) (circle.getCenter().getX() - (diameter * 1.5) < 0 ? 0 : circle.getCenter().getX() - (diameter * 1.5));
			int yStart = circle.getCenter().getY() - diameter < 0 ? 0 : circle.getCenter().getY() - diameter;
			int searchWidth = xStart + diameter * 3 > image.getWidth() ? image.getWidth() - circle.getCenter().getX() - 1 : diameter * 3 ;
			int searchHeight = yStart + diameter * 4 > image.getHeight() ? image.getHeight() - circle.getCenter().getY() - 1 : diameter * 4;			
			List<Coordinate2D> startPoints = findStartPoints(xStart, yStart, searchWidth, searchHeight, image);
			actorRelationships.addAll(connectPoints(image, geom, startPoints));
		}

		logger.info("Searching for all use cases relationships.");
		for(Geometry geom: ellipses){
			Ellipse ellipse = (Ellipse) geom;
			//Defines the search area
			float majorAxis = 0;
			float minorAxis = 0;
			if(Ellipse.getLength(ellipse.getAxisA()) > Ellipse.getLength(ellipse.getAxisB())){
				majorAxis = Float.valueOf(Ellipse.getLength(ellipse.getAxisA())).equals(Float.NaN)? 10:Ellipse.getLength(ellipse.getAxisA());
				minorAxis = Float.valueOf(Ellipse.getLength(ellipse.getAxisB())).equals(Float.NaN)? 10:Ellipse.getLength(ellipse.getAxisB());
			}else{
				minorAxis = Float.valueOf(Ellipse.getLength(ellipse.getAxisA())).equals(Float.NaN)? 10:Ellipse.getLength(ellipse.getAxisA());
				majorAxis = Float.valueOf(Ellipse.getLength(ellipse.getAxisB())).equals(Float.NaN)? 10:Ellipse.getLength(ellipse.getAxisB());
			}

			int xStart = (int) (ellipse.getCenter().getX() - majorAxis * 0.7 < 0 ? 0 : ellipse.getCenter().getX() - majorAxis * 0.7);
			int yStart = (int) (ellipse.getCenter().getY() - minorAxis * 0.7 < 0 ? 0 : ellipse.getCenter().getY() - minorAxis * 0.7);

			int searchWidth = (int) ((ellipse.getCenter().getX() + majorAxis * 1.4)  > image.getWidth() ? 
					image.getWidth()- ellipse.getCenter().getX() - 1 : (majorAxis * 1.4) - 4) ;

			int searchHeight = (int) ((ellipse.getCenter().getY() + minorAxis * 1.4)  > image.getHeight() ? 
					image.getHeight() - ellipse.getCenter().getY()  - 1 : (minorAxis * 1.4) - 4);
			List<Coordinate2D> startPoints = findStartPoints(xStart, yStart, searchWidth, searchHeight, image);
			useCaseRelationships.addAll(connectPoints(image, geom, startPoints));
		}

		List<Line> lines = new ArrayList<Line>();
		lines.addAll(actorRelationships);
		lines.addAll(useCaseRelationships);
		
		//Clear lines
		for(Line line : lines){
			for(Coordinate2D c : line.getLinePoints()){
				image.setRGB(c.getX(), c.getY(), 255, 255, 255);
			}
		}
		
		logger.info("Lines found:" + lines.size() +" .");
		context.put(UseCaseContext.LINES, lines);

		return false;
	}

	/** 
	 * Searches for points to start the element connection
	 * @return {@link List} of starting points
	 */
	private List<Coordinate2D> findStartPoints(int xStart, int yStart, int width, int height, Image image){
		List<Coordinate2D> startPoints = new ArrayList<Coordinate2D>();

		List<Coordinate2D> topStartPoints = new ArrayList<Coordinate2D>();
		List<Coordinate2D> downStartPoints = new ArrayList<Coordinate2D>();
		for(int x = xStart ; x < xStart + width ; x++ ){
			int yStartVal = yStart - 15 > 0 ? yStart - 15 : 0;
			int yEndVal = yStart + 15 < image.getHeight() ? yStart + 15 : image.getHeight() - 1 ;

			for(int y = yEndVal ; y > yStartVal ; y--){
				//Looks for start points over the top border
				if(image.getR(x, y) != 255){
					topStartPoints.add(new Coordinate2D(x, yStart));
				}
			}

			for(int y = yStartVal ; y < yEndVal - 1 ; y++){
				//Looks for points over the down border
				if(image.getR(x, (y + height)) != 255){
					downStartPoints.add(new Coordinate2D(x, yStart + height));
				}

			}
		}

		//Validates the top points that are part of the same line
		for(int i = 0 ; i < topStartPoints.size() ; i++){
			for(int j = i + 1 ; j < topStartPoints.size() ; j++){
				Coordinate2D c1 = topStartPoints.get(i);
				Coordinate2D nextCoord = topStartPoints.get(j);
				int coordsDistance = Math.abs(c1.getX() - nextCoord.getX());
				if(coordsDistance < MIN_START_DIST){
					topStartPoints.remove(nextCoord);
					j--;
				}
			}
		}	

		//Validates the down points that are part of the same line
		for(int i = 0 ; i < downStartPoints.size() ; i++){
			for(int j = i + 1 ; j < downStartPoints.size() ; j++){
				Coordinate2D c1 = downStartPoints.get(i);
				Coordinate2D nextCoord = downStartPoints.get(j);
				int coordsDistance = Math.abs(c1.getX() - nextCoord.getX());
				if(coordsDistance < MIN_START_DIST){
					downStartPoints.remove(nextCoord);
					j--;
				}
			}
		}

		List<Coordinate2D> leftSideStartPoints = new ArrayList<Coordinate2D>();
		List<Coordinate2D> rightSideStartPoints = new ArrayList<Coordinate2D>();

		for(int y = yStart ; y < height + yStart ; y++ ){

			int xStartVal = xStart - 15 > 0 ? xStart - 15 : 0;
			int xEndVal = xStart + 15 < image.getWidth() ? xStart + 15 : image.getWidth() - 1 ;

			for(int x = xEndVal ; x > xStartVal ; x--){
				//Looks for start points over the left border
				if(image.getR(x, y) != 255){
					leftSideStartPoints.add(new Coordinate2D(x, y));
				}
			}

			for(int x = xStartVal ; x < xEndVal - 1 ; x++){
				//Looks for points over the right border
				if(image.getR((x + width), y) != 255){
					rightSideStartPoints.add(new Coordinate2D(x + width, y));
				}

			}
		}

		//Validates left points that are part of the same line
		for(int i = 0 ; i < leftSideStartPoints.size() ; i++){
			for(int j = i + 1 ; j < leftSideStartPoints.size() ; j++){
				Coordinate2D c1 = leftSideStartPoints.get(i);
				Coordinate2D nextCoord = leftSideStartPoints.get(j);
				int coordsDistance = Math.abs(c1.getY() - nextCoord.getY());
				if(coordsDistance < MIN_START_DIST){
					leftSideStartPoints.remove(nextCoord);
					j--;
				}
			}
		}

		//Validates left points that are part of the same line
		for(int i = 0 ; i < rightSideStartPoints.size() ; i++){
			for(int j = i + 1 ; j < rightSideStartPoints.size() ; j++){
				Coordinate2D c1 = rightSideStartPoints.get(i);
				Coordinate2D nextCoord = rightSideStartPoints.get(j);
				int coordsDistance = Math.abs(c1.getY() - nextCoord.getY());
				if(coordsDistance < MIN_START_DIST){
					rightSideStartPoints.remove(nextCoord);
					j--;
				}
			}
		}
		startPoints.addAll(topStartPoints);
		startPoints.addAll(downStartPoints);
		startPoints.addAll(leftSideStartPoints);
		startPoints.addAll(rightSideStartPoints);

		return startPoints;
	}

	/** 
	 * Connect point. 
	 * @param image - Source image
	 * @param geometry - Element from which the line start
	 * @param startPoints - List of starting points
	 * @return List of lines associated to this element
	 */
	private List<Line> connectPoints(Image image, Geometry geometry,List<Coordinate2D> startPoints){
		List<Line> lines = new ArrayList<Line>();
		for(Coordinate2D stPoint : startPoints){
			int x = stPoint.getX();
			int y = stPoint.getY();
			List<Coordinate2D> partialSegment = new ArrayList<Coordinate2D>();
			partialSegment.add(new Coordinate2D(x, y));
			CoordinateConnectorUtil.setElementCenter(geometry.getCenter());
			if(x > geometry.getCenter().getX() && y <geometry.getCenter().getY()){//Right-upper side
				CoordinateConnectorUtil.checkRightUpperSide(x, y, image, partialSegment);
			}else if(x < geometry.getCenter().getX() && y <geometry.getCenter().getY()){//Left-upper side
				CoordinateConnectorUtil.checkLeftUpperSide(x, y, image, partialSegment);
			}else if(x < geometry.getCenter().getX() && y > geometry.getCenter().getY()){//Left-down side
				CoordinateConnectorUtil.checkLeftDownSide(x, y, image, partialSegment);
			}else if(x > geometry.getCenter().getX() && y > geometry.getCenter().getY()){//Right-down side
				CoordinateConnectorUtil.checkRightDownSide(x, y, image, partialSegment);
			}else if(x == geometry.getCenter().getX() && y > geometry.getCenter().getY()){//Down
				CoordinateConnectorUtil.checkDownSide(x, y, image, partialSegment);
			}else if(x == geometry.getCenter().getX() && y < geometry.getCenter().getY()){//Up
				CoordinateConnectorUtil.checkUpperSide(x, y, image, partialSegment);
			}else if(x < geometry.getCenter().getX() && y == geometry.getCenter().getY()){//Left
				CoordinateConnectorUtil.checkLeftSide(x, y, image, partialSegment);
			}else if(x > geometry.getCenter().getX() && y == geometry.getCenter().getY()){//Right
				CoordinateConnectorUtil.checkRightSide(x, y, image, partialSegment);
			}
			CoordinateConnectorUtil.setElementCenter(null);

			if(partialSegment.size() > 30){	
				StandardLine line = CoordinateConnectorUtil.createLine(partialSegment);
				CoordinateConnectorUtil.setGaps(0);
				lines.add(line);
			}
		}
		return lines;
	}

}
