/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 17/11/2013<br>
 */

package br.com.fatec.mc.udct.utils.dictionary;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Utility class that provides methods for search words in a list
 * verifying if it exists or if there is a similar one. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 17/11/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class DictionaryUtils {
	private Logger logger = LoggerFactory.getLogger(DictionaryUtils.class);

	private final String INDEXES_DIR = "dictionary/";
	private final String FILE_EXT = ".txt";
	private final int WORD_SIZE = 2;

	private Map<String, Map<String,String>> dictionaryCache = new HashMap<>();

	/** 
	 * Search for valid words based in the received parameter.
	 * IE: The parameter is ACTORCASE. The word will be checked against a dictionary
	 * to return ACTOR and CASE
	 * @param letters
	 * @return 
	 */
	public List<String> findWords(String letters, List<String> words){
		boolean check = true;
		boolean found = false;
		if(letters.length() > WORD_SIZE){

			File index = null;
			for(int i = 0 ; i < letters.length() - 1; i ++){	
				index = getIndex(letters.substring(i, i + 1));
				if(index != null){
					break;
				}

			}
			if(index == null){
				words.add(letters);
				return words;
			}
			String word = "";
			if(getDictionary(index).containsKey(letters)){
				check = false;
				found = true;
				words.add(letters);	
				return words;
			}else{
				for(int i = 1 ; i < letters.length() ; i ++){//Searches for a exact match
					word = letters.substring(0, i);
					if(getDictionary(index).containsKey(word)){ 
						check = false;
						found = true;
						words.add(word);
						break;
					}			
				}
			}

			if(check){//If no exact match found, looks for similar entries
				for(String key : getDictionary(index).keySet()){
					for(int i = letters.length() ; i > letters.length() /2 ; i --){
						word = letters.substring(0, i);
						if(key.contains(word)){
							words.add(key);
							found = true;
							break;
						}
					}
					if(found){
						break;
					}
				}
			}


			if(found == true && letters.length() - word.length() > WORD_SIZE){//Recursive search
				findWords(letters.substring(word.length()), words);
			}else if(found == false){//If no match is found returns the given string
				words.add(letters);
			}
		}else{
			words.add(letters);			
		}

		return words;
	}

	private File getIndex(String letters){
		//Gets the dictionary file
		String dir = INDEXES_DIR.concat(letters.substring(0, 1)).concat(FILE_EXT);
		File resource = new File(dir);
		logger.info("Trying to locate resource at ".concat(dir));
		if(!resource.exists()){//If file doesn't exists try the next character
			return null;
		}
		logger.info("Found resource: ".concat(resource.getAbsolutePath()));

		return resource; 

	}

	/** 
	 * Retrieves the dictionary
	 * @param file - File that represents the index by Character
	 * @return
	 */
	private Map<String,String> getDictionary(File file){
		if(!dictionaryCache.containsKey(file.getName())){
			Map<String,String> dictionary = new HashMap<String, String>();
			try {
				List<String> lines = Files.readLines(file,  Charsets.UTF_8);
				for(String line : lines){
					dictionary.put(line, "");
				}
				dictionaryCache.put(file.getName(), dictionary);
			} catch (IOException e) {
				logger.error("Error reading dictionary file",e);
			}
			return dictionary;
		}else{
			return dictionaryCache.get(file.getName());
		}
	}

}
