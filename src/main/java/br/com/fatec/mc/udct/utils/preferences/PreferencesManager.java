/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 29/05/2013<br>
 */

package br.com.fatec.mc.udct.utils.preferences;

import java.util.prefs.Preferences;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Class that manages the application preferences. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 29/05/2013 - @author Vinícius Oliveira Ueda<br>
 *<br>
 *<br>
 */

public class PreferencesManager {
	private static final String PREFERENCES_NODE = "br.com.fatec.mc.udct.utils.preferences";
	
	/** 
	 * Get the application preferences
	 * @return {@link Preferences}: Application preferences
	 */
	public static Preferences getPreferences() {
		return Preferences.userRoot().node(PREFERENCES_NODE);
	}
}
