/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 26/07/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Circle;
import web.unbc.ca.Coordinate;
import web.unbc.ca.Geometry;
import web.unbc.ca.Polygon;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.image.ImageConverterUtils;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Command that validates circle detection precision rates. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 26/07/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class CircleRateValidator implements Command {
	private Logger logger = LoggerFactory.getLogger(CircleRateValidator.class);

	/**
	 * Rate to be considered a valid circle
	 */
	private final double REAL_CIRCLE_RATE = 40;

	/** 
	 * Validates the circle detection precision rates, confronting calculated data and image data
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> circles = circlesAndEllipses.get(Circle.class.getSimpleName());
		List<Geometry> circles2Remove = new ArrayList<Geometry>();
		Image image = (Image) context.get(UseCaseContext.IMAGE);
		image = ImageConverterUtils.image2BinaryWhiteEdge(image, 200);
		
		logger.info("Validating rate of the detected circles. The minimum rate is: " + REAL_CIRCLE_RATE + "%");
		logger.info("Number of circles before process: " + circles.size());
		for(Geometry form : circles){
			double precisionRate = 0;
			Circle circle = (Circle) form;
			Polygon poly = circle.getPolygon(Circle.MAX_POINTS);
			for(Coordinate c:poly.getPoints()){
				if(checkPointsInImageNeighborhood(c,image)){
					precisionRate++;
				}
			}
			if(((precisionRate/Circle.MAX_POINTS) * 100) < REAL_CIRCLE_RATE){
				circles2Remove.add(circle);
			}
		}
		circles.removeAll(circles2Remove);
		logger.info("Number of circles after process: " + circles.size());
		return false;
	}
	
	/** 
	 * Look in the image for the pixel provided as the parameter.
	 * It will consider a tolerance distance, so it will look in the pixel neighborhood
	 * and if it finds a border pixel it will be considered as part of the circle.
	 * @return <code>true</code> if there is a border pixel or <code>false</code> if there
	 * is no border pixel.
	 */
	private boolean checkPointsInImageNeighborhood(Coordinate c, Image image){
		int x = c.getX();
		int y = c.getY();
		int widthBoundarie = 15;
		int heigthBoundarie = 15;
		//Check image boundaries
		if((x - widthBoundarie >= 0) && (y - heigthBoundarie >= 0)
				&& (x + widthBoundarie < image.getWidth()) && (y + heigthBoundarie < image.getHeight())){
			for(int i = x - widthBoundarie; i <= x + widthBoundarie ;i++){
				for(int j = y - heigthBoundarie; j <= y + heigthBoundarie ;j++){
					if(image.getR(i, j) != 255){
						image.setRGB(i, j, 255, 255, 255);
						return true;
					}
				}	
			}
		}
		return false;
	} 

}
