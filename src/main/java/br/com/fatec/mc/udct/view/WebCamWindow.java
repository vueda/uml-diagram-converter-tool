/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 30/05/2013<br>
 */

package br.com.fatec.mc.udct.view;

import java.awt.BorderLayout;

import javax.swing.JInternalFrame;

import com.github.sarxos.webcam.WebcamPanel;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Window with the webcam loaded. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 30/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

@SuppressWarnings("serial")
public class WebCamWindow extends JInternalFrame{

	public WebCamWindow(WebcamPanel camPanel){
		setLayout(new BorderLayout());
		setClosable(true);
		setTitle(camPanel.getName());
		camPanel.setVisible(true);
        add(camPanel, BorderLayout.CENTER);
	}
}
