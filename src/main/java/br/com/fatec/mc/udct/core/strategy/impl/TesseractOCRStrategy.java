/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 15/11/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy.impl;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.imageio.ImageIO;

import net.sourceforge.javaocr.ocrPlugins.CharacterExtractor;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.strategy.IOCRStrategy;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.dictionary.DictionaryUtils;
import br.com.fatec.mc.udct.utils.image.ImageConverterUtils;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * OCR strategy using the Tesseract framework. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 15/11/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class TesseractOCRStrategy implements IOCRStrategy {
	private Logger logger = LoggerFactory.getLogger(TesseractOCRStrategy.class);

	private final int CHAR_WIDTH = 35;
	private final int CHAR_HEIGHT = 35;
	
	private final String TEMP_DIR = "./temp";
	private final String PT_BR_LANGUAGE = "por";

	/** 
	 * Recognize characters in the given image
	 * @param image - {@link Image}: Image with characters
	 * @return {@link String} - Recognized string
	 * @see br.com.fatec.mc.udct.core.strategy.IOCRStrategy#doRecognition(br.com.fatec.mc.udct.domain.image.Image)
	 */
	@Override
	public String doRecognition(Image image) {
		List<String> words = new ArrayList<>();
		Tesseract instance = Tesseract.getInstance();
		instance.setLanguage(PT_BR_LANGUAGE);
		
		try {
			BufferedImage tmp2 = resize(mergeCharacters(image.getImage()));
			String text =  instance.doOCR(tmp2);
			text = text.replace(" ", "");
			text = text.replace("\n", "");
			text = text.toUpperCase();
			text = Normalizer.normalize(text, Normalizer.Form.NFD);
			text = text.replaceAll("[^\\p{ASCII}]", "");
			new DictionaryUtils().findWords(text, words);
			StringBuilder completeWord = new StringBuilder();
			for(String word : words){
				completeWord.append(word).append(" ");
			}
			completeWord.setLength(completeWord.length() - 1);
			return completeWord.toString();
			//return text;
		} catch (TesseractException e) {
			logger.error("Error recognizing text with Tesseract");
			logger.error(e.getMessage());
			return null;
		}
	}
	
	/** 
	 * Resize image
	 * @param image
	 * @return
	 */
	private BufferedImage resize(BufferedImage image){
		BufferedImage resizedImage = new BufferedImage(image.getWidth() * 3, 105, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(image, 0, 0, image.getWidth() * 3, 100, null);
		g.dispose();
		return ImageConverterUtils.image2BinaryWhiteEdge(new Image("", resizedImage), 225).getImage();
	}
	
	/** 
	 * Merge characters
	 * @param image
	 * @return
	 */
	private BufferedImage mergeCharacters(BufferedImage image){
		List<BufferedImage> chars = cutCharacters(image);
		int imgWidth = chars.size() * CHAR_WIDTH;
		BufferedImage result = new BufferedImage(imgWidth == 0 ? 10:imgWidth, CHAR_HEIGHT, BufferedImage.TYPE_INT_RGB);
		Graphics graph = result.getGraphics();
		graph.setColor(Color.white);
		graph.fillRect(0, 0, result.getWidth(), result.getHeight());
		
		for(int i = 0 ; i < chars.size() ; i++){
			BufferedImage img = chars.get(i);
			graph.drawImage(img, i * CHAR_WIDTH, 0, null);
		}


		return result;
	}
	
	/** 
	 * Cut the characters in the given image
	 * @param textImage - {@link BufferedImage} with the characters
	 * @return {@link List} of {@link BufferedImage} of each character
	 */
	private List<BufferedImage> cutCharacters(BufferedImage textImage){
		List<BufferedImage> charImages = new ArrayList<BufferedImage>();
		if(textImage.getWidth() < CHAR_WIDTH || textImage.getHeight() < CHAR_HEIGHT){
			return charImages;
		}
		File outputfile = new File("tempImage.png");

		try {
			ImageIO.write(textImage, "png", outputfile);
			CharacterExtractor slicer = new CharacterExtractor();
			File tempDir =  new File(TEMP_DIR);
			if(tempDir.exists()){
				delete(tempDir);
			}
			tempDir.mkdirs();
			slicer.slice(outputfile, tempDir,CHAR_WIDTH, CHAR_HEIGHT);
			
			List<File> files = Arrays.asList(tempDir.listFiles());
			Collections.sort(files, new Comparator<File>() {

				@Override
				public int compare(File o1, File o2) {
					Integer n1 = Integer.valueOf(o1.getName().split("_")[1].replace(".png", ""));
					Integer n2 = Integer.valueOf(o2.getName().split("_")[1].replace(".png", ""));
					if(n1 > n2){
						return 1;
					}else if (n2 > n1){
						return -1;
					}

					return 0;
				}
			});
			for(File file : files){
				BufferedImage charImage = ImageIO.read(file);
				charImages.add(charImage);
			}
			
			delete(tempDir);//Delete the temporary directory and all it's files
			outputfile.delete();
		} catch (IOException e) {
			logger.error("Failed to write file", e);
		}

		return charImages;
	}

	/** 
	 * Delete files/directories recursively
	 * @param file {@link File}: File or directory
	 * @throws IOException
	 */
	private void delete(File file) throws IOException{	 
		if(file.isDirectory()){//If directory is empty delete
			if(file.list().length==0){
				file.delete();
			}else{

				String files[] = file.list();//Get all files
				for (String temp : files) {
					File fileDelete = new File(file, temp);
					delete(fileDelete);//Recursive delete
				}

				if(file.list().length==0){
					file.delete();
				}
			}

		}else{
			file.delete();
		}
	}

}
