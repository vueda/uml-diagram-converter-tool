/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.image.GreyscaleFilterUtils;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Command that converts an Image to grey scale. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class Image2Greyscale implements Command {

	private Logger logger = LoggerFactory.getLogger(Image2Greyscale.class);

	/** 
	 * Converts the context image to grey scale
	 * @param context {@link Context}: Chain context 
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@Override
	public boolean execute(Context context) throws Exception {
		logger.info("Convert image to Greyscale.");
		Image image = (Image) context.get(UseCaseContext.IMAGE);
		if(image == null){
			logger.error("There is no image to process.");
			return true;
		}
		GreyscaleFilterUtils.greyScaleAverageTechnique(image);
		return false;
	}

}
