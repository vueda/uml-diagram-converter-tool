/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 06/10/2013<br>
 */

package br.com.fatec.mc.udct.view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import br.com.fatec.mc.udct.utils.preferences.Preferences;
import br.com.fatec.mc.udct.utils.preferences.PreferencesManager;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Dialog for neural network configuration. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 06/10/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

@SuppressWarnings("serial")
public class ConfigurationDialog extends JDialog {
	private File fileDirectory = new File(PreferencesManager.getPreferences().get(Preferences.NEURAL_NETWORK, ""));
	private JFileChooser fileChooser = new JFileChooser(fileDirectory);
	private File neuralNetworkFile = null;
	
	private ResourceBundle messages;
	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	
	/**
	 * Create the dialog.
	 */
	public ConfigurationDialog() {

		messages = ApplicationMain.getInstance().getMessages();
		setBounds(100, 100, 561, 363);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 535, 269);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		
		JLabel lblNeuralNetwork = new JLabel(messages.getString("menu.neural.network.path"));
		
		lblNeuralNetwork.setBounds(10, 21, 249, 14);
		contentPanel.add(lblNeuralNetwork);
		
		textField = new JTextField();
		textField.setBounds(10, 36, 414, 20);
		contentPanel.add(textField);
		textField.setColumns(10);
		
		if(fileDirectory != null){
			textField.setText(fileDirectory.getAbsolutePath());
		}
		
		JButton loadButton = new JButton(messages.getString("menu.neural.network.load"));
		loadButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fileChooser.setFileFilter(new FileNameExtensionFilter("Neural Network files", "nnet"));
				fileChooser.setAcceptAllFileFilterUsed(false); 
				fileChooser.setMultiSelectionEnabled(false);
				if(fileChooser.showOpenDialog(fileChooser) == JFileChooser.APPROVE_OPTION){
					neuralNetworkFile = fileChooser.getSelectedFile();
					textField.setText(neuralNetworkFile.getAbsolutePath());
				}				
			}
		});
		
		loadButton.setBounds(436, 35, 89, 23);
		contentPanel.add(loadButton);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(0, 280, 535, 33);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane);
			
			JButton okButton = new JButton(messages.getString("menu.neural.network.confirm"));
			okButton.addActionListener(new ActionListener() {		
				@Override
				public void actionPerformed(ActionEvent e) {
					PreferencesManager.getPreferences().put(Preferences.NEURAL_NETWORK, textField.getText());
					setVisible(false);					
				}
			});
			buttonPane.add(okButton);
			
			JButton cancelButton = new JButton(messages.getString("menu.neural.network.cancel"));
			cancelButton.addActionListener(new ActionListener() {				
				@Override
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
					System.out.println();
				}
			});
			buttonPane.add(cancelButton);
		}
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setVisible(true);
	}

}
