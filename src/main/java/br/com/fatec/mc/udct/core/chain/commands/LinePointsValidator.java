/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 25/08/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.ArrowType;
import br.com.fatec.mc.udct.domain.geometry.ArrowedLine;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.StandardLine;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Command that validate if the lines are composed for at least a 
 * minimum number of points. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 25/08/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class LinePointsValidator implements Command {

	private final int POINTS_NUMBER = 100;
	private final Double POINTS_DISTANCE = Double.valueOf(40);
	private final int MAX_ANGLE = 45;
	private final int MIN_ANGLE = 15;
	private final int ANGLE_SAME_LINE = 10;


	/** 
	 * Remove lines that have less points than the defined parameter
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		List<StandardLine> lines = (List<StandardLine>) context.get(UseCaseContext.LINES);
		List<StandardLine> lines2Remove = new ArrayList<StandardLine>();
		List<ArrowedLine> lines2Add = new ArrayList<ArrowedLine>();

		for(StandardLine l:lines){
			if(l.getLinePoints().size() < POINTS_NUMBER){
				//Needs to check if the small line is closer to a big one
				//so it's probably related to them. Else needs to be discarded
				existsRelatedLine(lines,lines2Remove, lines2Add, l);
			}
		}
		lines.removeAll(lines2Remove);
		lines.addAll(lines2Add);
		return false;
	}

	public void existsRelatedLine(List<StandardLine> lines, List<StandardLine> lines2Remove, List<ArrowedLine> lines2Add, StandardLine currentLine){
		for(StandardLine l:lines){
			if(!l.getAngularCoefficient().equals(currentLine.getAngularCoefficient()) && 
					!l.getLinearCoefficient().equals(currentLine.getLinearCoefficient()) && 
					!(l.getLinePoints().size() == currentLine.getLinePoints().size())){//Ignores the current line
				
				List<Double> distances = new ArrayList<Double>();
				Map<Double,Coordinate2D> mapDistances = new HashMap<Double, Coordinate2D>();
				double dist= 0;
				dist = currentLine.getFirstPoint().distance2Coordinate(l.getFirstPoint());
				mapDistances.put(dist, l.getFirstPoint());		
				distances.add(dist);

				dist = currentLine.getFirstPoint().distance2Coordinate(l.getLastPoint());
				mapDistances.put(dist, l.getLastPoint());		
				distances.add(dist);
				
				dist = currentLine.getLastPoint().distance2Coordinate(l.getFirstPoint());
				mapDistances.put(dist, l.getFirstPoint());		
				distances.add(dist);

				dist = currentLine.getLastPoint().distance2Coordinate(l.getLastPoint());
				mapDistances.put(dist, l.getLastPoint());		
				distances.add(dist);

				Collections.sort(distances);
				if(distances.get(0) < POINTS_DISTANCE){//The lines are closer. So let's check the angle between them.
					double alfa = calculateAngle(l, currentLine);
					double angle = Math.abs(Math.toDegrees(Math.atan(alfa)));
					System.out.println("Angle:" + angle);
					System.out.println("Current p1: " + currentLine.getFirstPoint().getX() + " " + currentLine.getFirstPoint().getY());
					System.out.println("Current p2: " + currentLine.getLastPoint().getX() + " " + currentLine.getLastPoint().getY());
					System.out.println("It p1: " + l.getFirstPoint().getX() + " " + l.getFirstPoint().getY());
					System.out.println("It p2: " + l.getLastPoint().getX() + " " + l.getLastPoint().getY());
					System.out.println("Maybe it's an arrow");
					if(isBetweenAngles(angle)){//Found an arrow line 
						//Here I will have to find if it's a closed or an open arrow
						ArrowedLine aLine = new ArrowedLine(l);
						aLine.setArrowType(ArrowType.OPEN);
						aLine.setArrowPosition(mapDistances.get(distances.get(0)));
						lines2Remove.add(l);
						lines2Add.add(aLine);
					}else if(angle <= ANGLE_SAME_LINE){//It's the same line 
/*						l.getLinePoints().addAll(currentLine.getLinePoints());
*/					}
				}
			}
		}
		lines2Remove.add(currentLine);
	}

	public boolean isBetweenAngles(double angle){
		if(angle > MIN_ANGLE && angle < MAX_ANGLE){
			return true;
		}
		return false;
	}

	private double calculateAngle(StandardLine firstLine, StandardLine secondLine){
		double alfa = 0;

		//Find the appropriate way to calculate the angle
		if(firstLine.getAngularCoefficient().equals(Double.NaN) 
				&& !secondLine.getAngularCoefficient().equals(Double.NaN)){
			alfa = 1/secondLine.getAngularCoefficient();
		}else if(!firstLine.getAngularCoefficient().equals(Double.NaN) 
				&& secondLine.getAngularCoefficient().equals(Double.NaN)){
			alfa = 1/firstLine.getAngularCoefficient();						
		}else if (!firstLine.getAngularCoefficient().equals(Double.NaN) 
				&& !secondLine.getAngularCoefficient().equals(Double.NaN)){
			alfa = Math.abs((firstLine.getAngularCoefficient() - secondLine.getAngularCoefficient()) 
					/ (1 + (firstLine.getAngularCoefficient() * secondLine.getAngularCoefficient())));
		}else{
			Coordinate2D line1Start = firstLine.getLinePoints().get(0);
			Coordinate2D line1End = firstLine.getLinePoints().get(firstLine.getLinePoints().size() - 1);
			Coordinate2D line2Start = secondLine.getLinePoints().get(0);
			Coordinate2D line2End = secondLine.getLinePoints().get(secondLine.getLinePoints().size() - 1);

			double angle1 = Math.atan2(line1Start.getY() - line1End.getY(),
					line1Start.getX() - line1End.getX());
			double angle2 = Math.atan2(line2Start.getY() - line2End.getY(),
					line2Start.getX() - line2End.getX());
			alfa = angle1-angle2;
		}

		return alfa;
	}

}
