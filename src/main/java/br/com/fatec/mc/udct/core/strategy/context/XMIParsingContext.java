/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 21/08/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy.context;

import br.com.fatec.mc.udct.core.strategy.IXMIParsingStrategy;
import br.com.fatec.mc.udct.domain.usecase.UseCaseDiagram;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Context of XMI parsing strategies execution. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 21/08/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class XMIParsingContext {
	
	private IXMIParsingStrategy strategy;
	
	/** 
	 * Class constructor
	 * @param strategy {@link IXMIParsingStrategy}: The concrete strategy to xmi parsing
	 */
	public XMIParsingContext(IXMIParsingStrategy strategy){
		this.strategy = strategy;
	}
	
	/** 
	 * Parse the {@link UseCaseDiagram} do a xmi file
	 * @param diagram - {@link UseCaseDiagram}
	 */
	public void doParse(UseCaseDiagram diagram) throws Exception{
		strategy.doParse(diagram);
	}
	
}
