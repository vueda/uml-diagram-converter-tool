/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.usecase;

import java.util.ArrayList;
import java.util.List;

import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Domain class that represents an UseCase diagram. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 28/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class UseCaseDiagram{
	private List<UseCaseEntity> useCaseElements;
	private Image image;

	/** 
	 * Returns the {@link List} with {@link UseCaseEntity} that represents the diagram model
	 * @return {@link List} of {@link UseCaseEntity}: the diagram model
	 */
	public List<UseCaseEntity> getUseCaseElements() {
		if(useCaseElements == null){
			useCaseElements = new ArrayList<UseCaseEntity>();
		}
		return useCaseElements;
	}

	/** 
	 * Setter method of {@link List} {@link UseCaseEntity}
	 * @param useCaseElements - {@link List} {@link UseCaseEntity}
	 */
	public void setUseCaseElements(List<UseCaseEntity> useCaseElements) {
		this.useCaseElements = useCaseElements;
	}

	/** 
	 * Image from where the model was extracted
	 * @return {@link Image}: The source image
	 */
	public Image getImage() {
		return image;
	}

	/** 
	 * Image's setter method
	 * @param image - {@link Image}:Source image
	 */
	public void setImage(Image image) {
		this.image = image;
	}
	
	
	
	
	
}
