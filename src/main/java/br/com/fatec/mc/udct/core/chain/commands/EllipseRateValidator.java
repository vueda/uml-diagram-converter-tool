/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/07/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Coordinate;
import web.unbc.ca.Ellipse;
import web.unbc.ca.EllipseFit;
import web.unbc.ca.Geometry;
import web.unbc.ca.Polygon;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.adapter.CoordinateAdapter;
import br.com.fatec.mc.udct.utils.image.ImageConverterUtils;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Command that validates the ellipses detection precision rate. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 28/07/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class EllipseRateValidator implements Command {
	
	private Logger logger = LoggerFactory.getLogger(EllipseRateValidator.class);

	/**
	 * Minimum rate for real ellipses
	 */
	private final double REAL_ELLIPSE_RATE = 55;
	
	/**
	 * Distance between ellipses center to merge them into one
	 */
	private final double MERGE_CENTER_DISTANCE = 50;
	
	/** 
	 * Validates the ellipses detection precision rates, confronting calculated data and image data
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> ellipses = circlesAndEllipses.get(Ellipse.class.getSimpleName());
		List<Geometry> ellipses2Add = new ArrayList<Geometry>();
		List<Geometry> ellipses2Remove = new ArrayList<Geometry>();
		
		Image image = (Image) context.get(UseCaseContext.IMAGE);
		image = ImageConverterUtils.image2BinaryWhiteEdge(image, 200);
		
		logger.info("Validating the ellipse detection rate. The minimum rate for real ellipses is " + REAL_ELLIPSE_RATE+ "%.");
		logger.info("Number of ellipses before process: " + ellipses.size());
		for(Geometry form : ellipses){
			double precisionRate = 0;
			Ellipse ellipse = (Ellipse) form;
			Polygon poly = ellipse.getPolygon(Ellipse.MAX_POINTS);
			for(Coordinate c:poly.getPoints()){
				if(checkPointsInImageNeighborhood(c,image)){
					precisionRate++;
				}
			}
			if(((precisionRate/Ellipse.MAX_POINTS) * 100) < REAL_ELLIPSE_RATE){
				ellipses2Remove.add(ellipse);
			}
		}

		ellipses.removeAll(ellipses2Remove);
		
		//Performs a secondary check merging valid ellipses with near invalid ellipses.
		//This is necessary to approach the detected ellipse to it's real size
		for(Geometry form : ellipses){
			Ellipse ellipse = (Ellipse) form;
			if(!ellipses2Remove.contains(ellipse));{
				List<Geometry> ellipses2Merge = new ArrayList<Geometry>();
				for(Geometry rForm : ellipses2Remove){
					Ellipse ellipseMerge = (Ellipse) rForm;
					Double distance = CoordinateAdapter.adapt2Coordinate2D(ellipse.getCenter())
					.distance2Coordinate(CoordinateAdapter.adapt2Coordinate2D(ellipseMerge.getCenter()));
					if(distance < MERGE_CENTER_DISTANCE){
						ellipses2Merge.add(ellipseMerge);
					}
				}
				List<Coordinate> ellipsePoints = new ArrayList<Coordinate>();
				for(Coordinate c: ellipse.getPolygon(Ellipse.MAX_POINTS).getPoints()){
					ellipsePoints.add(c);
				}
				for(Geometry mEl : ellipses2Merge){
					Ellipse el = (Ellipse) mEl;
					for(Coordinate c: el.getPolygon(Ellipse.MAX_POINTS).getPoints()){
						ellipsePoints.add(c);
					}	
				}
				EllipseFit ef = new EllipseFit(new HashSet<>(ellipsePoints));
				Ellipse finalEllipse = ef.getEllipse(EllipseFit.FPF);
				ellipses2Add.add(finalEllipse);
				ellipses2Remove.add(ellipse);
			}
		}
		
		ellipses.removeAll(ellipses2Remove);
		ellipses.addAll(ellipses2Add);
		
		logger.info("Number of ellipses after process: " + ellipses.size());

		return false;
	}
	
	/** 
	 * Look in the image for the pixel provided as the parameter.
	 * It will consider a tolerance distance, so it will look in the pixel neighborhood
	 * and if it finds a border pixel it will be considered as part of the circle.
	 * @return <code>true</code> if there is a border pixel or <code>false</code> if there
	 * is no border pixel.
	 */
	private boolean checkPointsInImageNeighborhood(Coordinate c, Image image){
		int x = c.getX();
		int y = c.getY();
		int widthBoundarie = 10;
		int heigthBoundarie = 10;
		//Check image boundaries
		if((x - widthBoundarie >= 0) && (y - heigthBoundarie >= 0)
				&& (x + widthBoundarie < image.getWidth()) && (y + heigthBoundarie < image.getHeight())){
			for(int i = x - widthBoundarie; i <= x + widthBoundarie ;i++){
				for(int j = y - heigthBoundarie; j <= y + heigthBoundarie ;j++){
					if(image.getR(i, j) == 0){
						image.setRGB(i, j, 255, 255, 255);
						return true;
					}
				}	
			}
		}
		return false;
	}

}
