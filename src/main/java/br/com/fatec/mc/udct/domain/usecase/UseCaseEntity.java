/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.usecase;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Abstract class that represents every UseCase entity related. <br>
 * Must be extended by all UseCase entities.<br> 
 * <br>
 * CLASS VERSIONS: <br>
 * 28/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public abstract class UseCaseEntity {
	private Integer id;

	/** 
	 * Unique identification of the UseCase diagram element.
	 * @return {@link Integer}
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
}

