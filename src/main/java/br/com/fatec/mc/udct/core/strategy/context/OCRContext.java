/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 15/09/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy.context;

import br.com.fatec.mc.udct.core.strategy.IOCRStrategy;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Context of OCR strategies execution. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 15/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class OCRContext {
	
	private IOCRStrategy strategy;
	
	/** 
	 * Class constructor
	 * @param strategy {@link IOCRStrategy}: The concrete OCR strategy
	 */
	public OCRContext(IOCRStrategy strategy){
		this.strategy = strategy;
	}
	
	/** 
	 * Perform the recognition of characters in the given image
	 * @param image - {@link Image} with characters
	 * @return {@link String} text referent to the image
	 */
	public String doRecognition(Image image){
		return strategy.doRecognition(image);
	}

}
