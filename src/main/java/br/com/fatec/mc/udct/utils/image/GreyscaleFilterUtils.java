/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 30/05/2013<br>
 */

package br.com.fatec.mc.udct.utils.image;

import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Util class to convert a Image to Grey scale. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 30/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class GreyscaleFilterUtils {
	
	/** 
	 * Converts the image to grey scale using the bright technique 
	 * @param image {@link Image}: The image to be converted to grey scale
	 */
	public static void greyScaleBrightTechnique(Image image){
		int [][] greyMatrix = new int [image.getImage().getWidth()][image.getImage().getHeight()];
		int maxValue = Integer.MIN_VALUE;
		int minValue = Integer.MAX_VALUE;

		for(int i = 0 ; i < image.getImage().getWidth() ; i++){
			for(int j = 0 ; j < image.getImage().getHeight()  ; j++){
				if(image.getR(i, j) > maxValue)
					maxValue = image.getR(i, j);
				if(image.getR(i, j) < minValue)
					minValue = image.getR(i, j);
				if(image.getG(i, j) > maxValue)
					maxValue = image.getG(i, j);
				if(image.getG(i, j) < minValue)
					minValue = image.getG(i, j);
				if(image.getB(i, j) > maxValue)
					maxValue = image.getB(i, j);
				if(image.getB(i, j) < minValue)
					minValue = image.getB(i, j);

				greyMatrix [i][j] = ((minValue + maxValue)/2);
				image.setRGB(i, j, greyMatrix [i][j], greyMatrix [i][j], greyMatrix [i][j]);
				
				maxValue = Integer.MIN_VALUE;
				minValue = Integer.MAX_VALUE;
			}
		}
	}

	/** 
	 * Converts the image to grey scale using the average technique
	 * @param image {@link Image}: The image to be converted to grey scale
	 */
	public static void greyScaleAverageTechnique(Image image){
		int [][] greyMatrix = new int [image.getImage().getWidth()][image.getImage().getHeight()];

		for(int i = 0 ; i < image.getImage().getWidth() ; i++){
			for(int j = 0 ; j < image.getImage().getHeight()  ; j++){
				greyMatrix [i][j] = ((image.getR(i, j) + image.getG(i, j) + image.getB(i, j))/3);
				image.setRGB(i, j, greyMatrix [i][j],greyMatrix [i][j] , greyMatrix [i][j]);
			}
		}         
	}

	/** 
	 * Converts the image to grey scale using the luminosity technique 
	 * @param image {@link Image}: The image to be converted to grey scale
	 */
	public static void greyScaleLuminosityTechnique(Image image){
		int [][] greyMatrix = new int [image.getImage().getWidth()][image.getImage().getHeight()];

		for(int i = 0 ; i < image.getImage().getWidth() ; i++){
			for(int j = 0 ; j < image.getImage().getHeight()  ; j++){
				greyMatrix [i][j] = (int)((image.getR(i, j)* 0.2125) + (image.getG(i, j)*0.7145) + (image.getB(i, j)*0.0721)); 
				image.setRGB(i, j, greyMatrix [i][j], greyMatrix [i][j], greyMatrix [i][j]);
			}
		}
	}

	/** 
	 * Converts the image to grey scale using the element technique 
	 * @param image {@link Image}: The image to be converted to grey scale
	 * @param element {@link Integer}: Which element to use (R, G or B)
	 */
	public static void greyScaleByElementTechnique(Image image, Integer element){
		for(int i = 0 ; i < image.getImage().getWidth() ; i++){
			for(int j = 0 ; j < image.getImage().getHeight()  ; j++){                
				if(element == 0){
					int pixelValue = image.getR(i, j);
					image.setRGB(i, j, pixelValue, pixelValue, pixelValue);
				}
				if(element == 1){
					int pixelValue = image.getG(i, j); 
					image.setRGB(i, j, pixelValue, pixelValue, pixelValue);
				}
				if(element == 2){
					int pixelValue = image.getB(i, j); 
					image.setRGB(i, j, pixelValue, pixelValue, pixelValue);
				}
			}
		}
	}
}
