/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/07/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Ellipse;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Validates the minimum size of the ellipses axis. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 28/07/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class EllipseAxisSizeValidator implements Command {
	private Logger logger = LoggerFactory.getLogger(EllipseAxisSizeValidator.class);
	
	private final int MIN_MINOR_ELLIPSE_AXIS = 60;
	private final int MIN_MAJOR_ELLIPSE_AXIS = 110;
	private final int MAX_MINOR_ELLIPSE_AXIS = 350;
	private final int MAX_MAJOR_ELLIPSE_AXIS = 550;
	
	/** 
	 * Validates the minimum size of the detected ellipses, discarding the ones that doesn't fit
	 * the requirements 
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> ellipses = circlesAndEllipses.get(Ellipse.class.getSimpleName());
		List<Geometry> ellipses2Remove = new ArrayList<Geometry>();
		
		logger.info("Validating axis size of the ellipses.");
		logger.info("Number of ellipses before process: " + ellipses.size());
		for(Geometry form : ellipses){
			Ellipse ellipse = (Ellipse)form;
			float majorAxis = 0;
			float minorAxis = 0;
			if(Ellipse.getLength(ellipse.getAxisA()) > Ellipse.getLength(ellipse.getAxisB())){
				majorAxis = Float.valueOf(Ellipse.getLength(ellipse.getAxisA())).equals(Float.NaN)? 10:Ellipse.getLength(ellipse.getAxisA());
				minorAxis = Float.valueOf(Ellipse.getLength(ellipse.getAxisB())).equals(Float.NaN)? 10:Ellipse.getLength(ellipse.getAxisB());
			}else{
				minorAxis = Float.valueOf(Ellipse.getLength(ellipse.getAxisA())).equals(Float.NaN)? 10:Ellipse.getLength(ellipse.getAxisA());
				majorAxis = Float.valueOf(Ellipse.getLength(ellipse.getAxisB())).equals(Float.NaN)? 10:Ellipse.getLength(ellipse.getAxisB());
			}
			
			majorAxis = majorAxis * 2;
			minorAxis = minorAxis * 2;
			
			if(majorAxis < MIN_MAJOR_ELLIPSE_AXIS || minorAxis < MIN_MINOR_ELLIPSE_AXIS ||
					majorAxis > MAX_MAJOR_ELLIPSE_AXIS || minorAxis > MAX_MINOR_ELLIPSE_AXIS ){
				ellipses2Remove.add(ellipse);
			}
		}
		ellipses.removeAll(ellipses2Remove);
		logger.info("Number of ellipses after process: " + ellipses.size());
		return false;
	}

}
