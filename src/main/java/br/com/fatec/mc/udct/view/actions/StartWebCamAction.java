/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 30/05/2013<br>
 */

package br.com.fatec.mc.udct.view.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.KeyStroke;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import br.com.fatec.mc.udct.view.ApplicationMain;
import br.com.fatec.mc.udct.view.WebCamWindow;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Action that starts the webcam. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 30/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

@SuppressWarnings("serial")
public class StartWebCamAction extends AbstractAction {

	/** 
	 * Starts the webcam
	 * @param event
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent event) {
		final Webcam webcam = Webcam.getDefault();
		webcam.setViewSize(WebcamResolution.VGA.getSize());
		final WebCamWindow camWindow = new WebCamWindow(new WebcamPanel(webcam));
		camWindow.setSize(WebcamResolution.VGA.getSize());
		camWindow.setVisible(true);
		camWindow.getInputMap().put(KeyStroke.getKeyStroke("F2"), "captureScreenAction");
		//camWindow.getActionMap().put("captureScreenAction", new CaptureScreenAction());

		camWindow.addInternalFrameListener(new InternalFrameListener() {		
			
			public void internalFrameClosed(InternalFrameEvent arg0) {
				webcam.close();
				camWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);		
			}				
						
			public void internalFrameIconified(InternalFrameEvent arg0) {}
			
			public void internalFrameDeiconified(InternalFrameEvent arg0) {}
			
			public void internalFrameDeactivated(InternalFrameEvent arg0) {}
			
			public void internalFrameClosing(InternalFrameEvent arg0) {}
			
			public void internalFrameActivated(InternalFrameEvent arg0) {}

			public void internalFrameOpened(InternalFrameEvent arg0) {}
		});
		
		ApplicationMain.getInstance().getDesktopPane().add(camWindow);
		ApplicationMain.getInstance().getDesktopPane().repaint();
	}

}
