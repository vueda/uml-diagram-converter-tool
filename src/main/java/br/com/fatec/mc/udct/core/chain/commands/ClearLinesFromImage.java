/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 14/09/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.List;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.StandardLine;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Clear the detected lines from the image. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 14/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class ClearLinesFromImage implements Command {

	/** 
	 * Clear the detected lines from image.
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		List<StandardLine> lines = (List<StandardLine>) context.get(UseCaseContext.LINES);
		Image image = (Image)context.get(UseCaseContext.IMAGE);
		Graphics2D graphic = (Graphics2D) image.getImage().getGraphics();
    	graphic.setColor(Color.WHITE);	
    	
		for(StandardLine l : lines){
			Coordinate2D s = l.getFirstPoint();
			Coordinate2D e = l.getLastPoint();
			for(int i = 0 ; i <= 5 ; i ++){
				graphic.drawLine(s.getX() + i, s.getY(), e.getX() + i, e.getY());
				graphic.drawLine(s.getX() - i, s.getY(), e.getX() - i, e.getY());
				graphic.drawLine(s.getX() , s.getY() + i, e.getX() , e.getY() + i);
				graphic.drawLine(s.getX() , s.getY() - i, e.getX() , e.getY() - i);
			}
		}
		
		return false;
	}

}
