/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 14/09/2013<br>
 */

package br.com.fatec.mc.udct.utils.image;

import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Utility class that calculates the histogram of a given image. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 14/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class HistogramUtils {

	/** 
	 * Calculates the vertical histogram of a given image
	 * @param textArea - {@link Image}
	 * @return int[] - Vertical Histogram
	 */
	public static int[] getVerticalHistogram(Image textArea){
		int[] vertHistogram = new int[textArea.getWidth()];
		for(int x = 0; x < textArea.getWidth(); x++){
			int value = 0;
			for(int y = 0; y < textArea.getHeight(); y++){
				if(textArea.getR(x, y) < 50){
					value++;
				}
			}
			vertHistogram[x] = value;			
		}
		return vertHistogram;
	}

	/** 
	 * Returns the horizontal histogram of a given image
	 * @param textArea - {@link Image}
	 * @return Horizontal Histogram
	 */
	public static int[] getHorizontalHistogram(Image textArea){
		int[] horiHistogram = new int[textArea.getHeight()];
		for(int y = 0; y < textArea.getHeight(); y++){
			int value = 0;
			for(int x = 0; x < textArea.getWidth(); x++){
				if(textArea.getR(x, y) < 50){
					value++;
				}
			}
			horiHistogram[y] = value;			
		}
		return horiHistogram;
	}
}
