/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy;

import java.util.List;
import java.util.Map;

import web.unbc.ca.Geometry;

import br.com.fatec.mc.udct.domain.geometry.Ellipse;
import br.com.fatec.mc.udct.domain.geometry.GeometryEntity;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Interface of ellipse detection strategies. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public interface IEllipseDetectionStrategy {
	List<Ellipse> detectEllipses(Image diagramImage);
	Map<String,List<Geometry>> detectedEllipsesAndCircles(List<List<? extends GeometryEntity>> imageData);
}
