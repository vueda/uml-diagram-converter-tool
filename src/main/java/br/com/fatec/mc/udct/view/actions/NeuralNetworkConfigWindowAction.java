/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 06/10/2013<br>
 */

package br.com.fatec.mc.udct.view.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import br.com.fatec.mc.udct.view.ConfigurationDialog;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Open neural network configuration window. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 06/10/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

@SuppressWarnings("serial")
public class NeuralNetworkConfigWindowAction extends AbstractAction {
	
	/** 
	 * Open the neural network configuration dialog
	 * @param event {@link ActionEvent}
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		ConfigurationDialog dialog = new ConfigurationDialog();
		dialog.setVisible(true);
	}
	
}
