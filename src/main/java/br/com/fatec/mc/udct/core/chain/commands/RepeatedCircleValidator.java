/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 27/07/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Circle;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Command that validates possible repeated detected circles to remove them. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 27/07/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class RepeatedCircleValidator implements Command {
	private Logger logger = LoggerFactory.getLogger(RepeatedCircleValidator.class);

	/**
	 * Maximum distance between circles center to be considered a repeated circle
	 */
	private final double CENTER_DISTANCE = 5;
	
	/**
	 * Maximum radius difference between circles to be considered a repeated circle
	 */
	private final double RADIUS_DIFF = 10;

	/** 
	 * Validates if there are repeated detected circles and remove them from the list
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> circles = circlesAndEllipses.get(Circle.class.getSimpleName());
		logger.info("Validating repeated circles comparing center and radius size.");
		logger.info("Number of circles before process: " + circles.size());
		for(int i = 0; i < circles.size();i++){
			Circle circle = (Circle) circles.get(i);
			for(int j = i + 1 ; j < circles.size();j++){
				Circle nextCircle = (Circle) circles.get(j);
				if(((circle.getCenter().distance2Coordinate(nextCircle.getCenter()) < CENTER_DISTANCE)
						&& Math.abs(circle.getRadius() - nextCircle.getRadius()) < RADIUS_DIFF) || 
						isInsideCurrentCircle(circle, nextCircle)){
					circles.remove(j);
					j--;
				}
			}
		}
		
		logger.info("Number of circles before process: " + circles.size());

		return false;
	}
	
	/** 
	 * Verifies if the nextCircle is inside the circle
	 * @param circle - Current {@link Circle}
	 * @param nextCircle - Next {@link Circle}
	 * @return <code>true</code> if is inside and <code>false</code> if not.
	 */
	private boolean isInsideCurrentCircle(Circle circle, Circle nextCircle){
		float radius = circle.getRadius();

		float rightX = circle.getCenter().getX()  + (radius);
		float leftX = circle.getCenter().getX()  - (radius);
		
		float upperY = circle.getCenter().getY()  - (radius);
		float downY = circle.getCenter().getY()  + (radius);

		if((nextCircle.getCenter().getX() < rightX && nextCircle.getCenter().getX() > leftX) &&
				(nextCircle.getCenter().getY() < downY && nextCircle.getCenter().getY() > upperY)){
			return true;
		}
		return false;
	}

}
