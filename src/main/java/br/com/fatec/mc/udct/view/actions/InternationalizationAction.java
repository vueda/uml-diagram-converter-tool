/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.view.actions;

import java.awt.event.ActionEvent;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;

import br.com.fatec.mc.udct.view.ApplicationMain;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Action to change the language of the the application. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

@SuppressWarnings("serial")
public class InternationalizationAction extends AbstractAction{

	/** 
	 * Changes the application language
	 * @param event {@link ActionEvent}
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		ImageIcon selectedItem = (ImageIcon) ApplicationMain.getInstance().getComboBox().getSelectedItem(); 
		String[] locale = selectedItem.getDescription().split("_");
		//Receive the Locale
		ApplicationMain.getInstance().setLocale(new Locale(locale[0], locale[1]));
		//Loads the resource bundle				
		ApplicationMain.getInstance().setMessages(ResourceBundle.getBundle("br.com.fatec.mc.udct.resources.ApplicationBundle", ApplicationMain.getInstance().getLocale()));
		ApplicationMain.getInstance().textLocaleRefresh();
		
	}

}
