/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 27/07/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import web.unbc.ca.Circle;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.adapter.CoordinateAdapter;
import br.com.fatec.mc.udct.utils.image.HistogramUtils;
import br.com.fatec.mc.udct.utils.image.ImageConverterUtils;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Removes from the image the pixels of already processed circles and the lines under the circle.
 * This helps to remove the actor body from the image<br>
 * <br>
 * CLASS VERSIONS: <br>
 * 27/07/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class ClearLinesUnderCirclesFromImage implements Command {

	private final int CLEAR_AREA_MULTIPLIER = 3;
	private final int CLEAR_AREA = 5;

	
	/** 
	 * Clear the actor body from the image
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> circles = circlesAndEllipses.get(Circle.class.getSimpleName());
		Image image = (Image)context.get(UseCaseContext.IMAGE);
    	Map<Geometry, List<Image>> texts = (Map<Geometry, List<Image>>) context.get(UseCaseContext.TEXTS);

		for(Geometry form : circles){
			Circle c = (Circle) form;
			
			List<Image> circleTexts = extractText(c, image);
			texts.put(c, circleTexts);
			
			Graphics graphic = (Graphics) image.getImage().getGraphics();
			graphic.setColor(Color.WHITE);
	    	graphic.fillRect((int)(c.getCenter().getX() - c.getRadius() - CLEAR_AREA), 
	    			(int)(c.getCenter().getY() - c.getRadius() - CLEAR_AREA), 
	    			(int)c.getRadius() * 2 + 2 * CLEAR_AREA,
	    			(int)(c.getRadius() * 2) * CLEAR_AREA_MULTIPLIER);
			
		}
		return false;
	}
	
	private List<Image> extractText(Circle circle, Image image){
		Coordinate2D center = CoordinateAdapter.adapt2Coordinate2D(circle.getCenter());
		double diameter = circle.getRadius() * 2;
		
		int xVal = (int) (center.getX() - diameter - circle.getRadius());
		int yVal = (int) (center.getY() + (2 * diameter) + circle.getRadius());
		Coordinate2D topLeftTextArea = new Coordinate2D(xVal , yVal);
		if(topLeftTextArea.getX() < 0){
			topLeftTextArea.setX(0);
		}
		
		int crpWidth = (int) ((topLeftTextArea.getX() + diameter * 3) > image.getWidth() ?(diameter * 3) - ((diameter * 3) - image.getWidth()) : (diameter * 3)); 
		int crpHeight = (int) ((topLeftTextArea.getY() + diameter)  > image.getHeight() ? diameter - ((topLeftTextArea.getY() + diameter) - image.getHeight()) : diameter); 
		BufferedImage textArea = image.getImage().getSubimage(topLeftTextArea.getX(), topLeftTextArea.getY(),
				crpWidth, crpHeight);
				
		Image textImg = new Image("", textArea);
		
		List<Image> textImages = new ArrayList<Image>();
		cropTextFromImage(textImg, textImages);
		
		Graphics2D graphic = (Graphics2D) image.getImage().getGraphics();
    	graphic.setColor(Color.WHITE);
    	
    	graphic.fillRect(topLeftTextArea.getX(), topLeftTextArea.getY(),
				(int)diameter * 3, (int) diameter);
		
		return textImages;	
	}
	
	private void cropTextFromImage(Image textArea, List<Image> textImages){
		int[] horzHist = HistogramUtils.getHorizontalHistogram(textArea);	
		Integer textStart = null;
		Integer textEnd = null;

		for(int i = 0; i < horzHist.length; i++){
			int el = horzHist[i];
			if(el != 0){//If the value is different than zero it's the start of a text area
				textStart = i;
				for(int j = i+1 ; j < horzHist.length;j++){
					int nextEl = horzHist[j];
					if(nextEl == 0){
						textEnd = j - 1;
						if(textEnd - textStart < 3){//If it's a small area, discard
							textStart = textEnd = null;
							break;
						}
						BufferedImage text = textArea.getImage().getSubimage(0, textStart, textArea.getWidth(), textEnd - textStart);
						Image textImg = ImageConverterUtils.cloneImage(text);
						textImages.add(textImg);
						textStart = textEnd = null;
						i = j;
						break;
					}
				}
				if(textStart != null && textEnd == null){
					textEnd = horzHist.length-1;
					if(textEnd - textStart > 0){
						BufferedImage text = textArea.getImage().getSubimage(0, textStart, textArea.getWidth(), textEnd - textStart);
						Image textImg = ImageConverterUtils.cloneImage(text);
						textImages.add(textImg);	
					}	
				}
			}
		}
	
	}

}
