/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 23/09/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Circle;
import web.unbc.ca.Ellipse;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Remove circles contained inside ellipses. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 23/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class RemoveCirclesWithinEllipses implements Command {

	private Logger logger = LoggerFactory.getLogger(RemoveCirclesWithinEllipses.class);

	/** 
	 * Remove circles contained inside ellipses.
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> circles = circlesAndEllipses.get(Circle.class.getSimpleName());
		List<Geometry> ellipses = circlesAndEllipses.get(Ellipse.class.getSimpleName());
		
		logger.info("Verifying if there are circles inside ellipses.");
		logger.info("Number of circles before process: " + circles.size());
		for(int i = 0 ; i < circles.size() ; i++){
			Circle circle = (Circle) circles.get(i);
			for(Geometry el:ellipses){
				Ellipse ellipse = (Ellipse) el;
				if(isInsideCurrentEllipse(ellipse, circle)){
					circles.remove(i);
					i--;
				}
			}
		}
		
		logger.info("Number of circles after process: " + circles.size());

		return false;
	}

	/** 
	 * Verifies if the circle is inside the ellipse
	 * @param ellipse 
	 * @param circle
	 * @return <code>true</code> if the circle is inside of the ellipse or <code>false</code> if not
	 */
	private boolean isInsideCurrentEllipse(Ellipse ellipse, Circle circle){

		float axisA = Ellipse.getLength(ellipse.getAxisA());
		float axisB = Ellipse.getLength(ellipse.getAxisB());

		float majorAxis = axisA >= axisB? axisA : axisB;
		float minorAxis =  axisA >= axisB? axisB : axisA; 

		float rightX = ellipse.getCenter().getX()  + (majorAxis/2);
		float leftX = ellipse.getCenter().getX()  - (majorAxis/2);

		float upperY = ellipse.getCenter().getY()  - (minorAxis/2);
		float downY = ellipse.getCenter().getY()  + (minorAxis/2);

		if((circle.getCenter().getX() < rightX && circle.getCenter().getX() > leftX) &&
				(circle.getCenter().getY() < downY && circle.getCenter().getY() > upperY)){
			return true;
		}
		return false;
	}

}
