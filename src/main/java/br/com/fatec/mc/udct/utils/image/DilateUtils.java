/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 20/11/2013<br>
 */

package br.com.fatec.mc.udct.utils.image;

import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Applies dilate algorithm to images. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 20/11/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class DilateUtils {

	public static final int THREE_TILES = 3;
	public static final int FIVE_TILES = 5;

	public static Image dilateImage(Image image, int numberTiles){
		Image dilate = new Image(image.getWidth(), image.getHeight());

		for(int x = 0 ; x < image.getWidth() ; x++){
			for(int y = 0 ; y < image.getHeight() ; y ++){
				if(THREE_TILES == numberTiles){
					if(check3Tiles(x, y, image)){
						dilate.setRGB(x, y, 0, 0, 0);
					}else{
						dilate.setRGB(x, y, 255, 255, 255);					
					}
				} if(FIVE_TILES == numberTiles){
					if(check5Tiles(x, y, image)){
						dilate.setRGB(x, y, 0, 0, 0);
					}else{
						dilate.setRGB(x, y, 255, 255, 255);					
					}
				}	
			}
		}
		return dilate;
	}
	private static boolean check5Tiles(int x, int y, Image image){
		if(x - 1 >= 0 && y - 1 >= 0){//Top left
			if(image.getR(x - 1, y - 1) != 255){
				return true;
			}
		} if(y - 1 >= 0){//Top
			if(image.getR(x, y - 1) != 255){
				return true;
			}
		} if(x + 1 < image.getWidth() && y - 1 >= 0){//Top right
			if(image.getR(x + 1, y - 1) != 255){
				return true;
			}
		} if(x - 1 >= 0){//Left
			if(image.getR(x - 1, y) != 255){
				return true;
			}
		} if(x + 1 < image.getWidth()){//Right
			if(image.getR(x + 1, y) != 255){
				return true;
			}
		} if(x - 1 >= 0 && y + 1 < image.getHeight()){//Down left
			if(image.getR(x - 1, y + 1) != 255){
				return true;
			}
		} if(y + 1 < image.getHeight()){//Down
			if(image.getR(x, y + 1) != 255){
				return true;
			}
		} if(x + 1 < image.getWidth() && y + 1 < image.getHeight()){//Down right
			if(image.getR(x + 1, y + 1) != 255){
				return true;
			}
		} if(x - 2 >= 0 && y - 2 >= 0){
			if(image.getR(x - 2, y - 2) != 255){
				return true;
			}
		} if(x - 1 >= 0 && y - 2 >= 0){
			if(image.getR(x - 1, y - 2) != 255){
				return true;
			}
		} if(y - 2 >= 0){
			if(image.getR(x, y - 2) != 255){
				return true;
			}
		} if(x + 1 < image.getWidth() && y - 2 >= 0){
			if(image.getR(x + 1, y - 2) != 255){
				return true;
			}
		} if(x + 2 < image.getWidth() && y - 2 >= 0){
			if(image.getR(x + 2, y - 2) != 255){
				return true;
			}
		}

		if(x - 2 >= 0 && y + 2 < image.getHeight()){
			if(image.getR(x - 2, y + 2) != 255){
				return true;
			}
		} if(x - 1 >= 0 && y + 2 < image.getHeight()){
			if(image.getR(x - 1, y + 2) != 255){
				return true;
			}
		} if(y + 2 < image.getHeight()){
			if(image.getR(x, y + 2) != 255){
				return true;
			}
		} if(x + 1 < image.getWidth() && y + 2 < image.getHeight()){
			if(image.getR(x + 1, y + 2) != 255){
				return true;
			}
		} if(x + 2 < image.getWidth() && y + 2 < image.getHeight()){
			if(image.getR(x + 2, y + 2) != 255){
				return true;
			}
		}

		if(x - 2 >= 0 && y - 1 >= 0){
			if(image.getR(x - 2, y - 1) != 255){
				return true;
			}
		} if(x - 2 >= 0){
			if(image.getR(x - 2, y) != 255){
				return true;
			}
		} if(x - 2 >= 0 && y + 1 < image.getHeight()){
			if(image.getR(x - 2, y + 1) != 255){
				return true;
			}
		}

		if(x + 2 < image.getHeight() && y - 1 >= 0){
			if(image.getR(x + 2, y - 1) != 255){
				return true;
			}
		} if(x + 2 < image.getHeight()){
			if(image.getR(x + 2, y) != 255){
				return true;
			}
		} if(x + 2 < image.getHeight() && y + 1 < image.getHeight()){
			if(image.getR(x + 2, y + 1) != 255){
				return true;
			}
		}
		return false;
	}


	private static boolean check3Tiles(int x, int y, Image image){
		if(x - 1 >= 0 && y - 1 >= 0){//Top left
			if(image.getR(x - 1, y - 1) != 255){
				return true;
			}
		} if(y - 1 >= 0){//Top
			if(image.getR(x, y - 1) != 255){
				return true;
			}
		} if(x + 1 < image.getWidth() && y - 1 >= 0){//Top right
			if(image.getR(x + 1, y - 1) != 255){
				return true;
			}
		} if(x - 1 >= 0){//Left
			if(image.getR(x - 1, y) != 255){
				return true;
			}
		} if(x + 1 < image.getWidth()){//Right
			if(image.getR(x + 1, y) != 255){
				return true;
			}
		} if(x - 1 >= 0 && y + 1 < image.getHeight()){//Down left
			if(image.getR(x - 1, y + 1) != 255){
				return true;
			}
		} if(y + 1 < image.getHeight()){//Down
			if(image.getR(x, y + 1) != 255){
				return true;
			}
		} if(x + 1 < image.getWidth() && y + 1 < image.getHeight()){//Down right
			if(image.getR(x + 1, y + 1) != 255){
				return true;
			}
		}

		return false;
	}
}
