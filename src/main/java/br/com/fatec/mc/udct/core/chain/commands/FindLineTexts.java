/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 15/09/2013<br>
 */
package br.com.fatec.mc.udct.core.chain.commands;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import net.sourceforge.javaocr.ocrPlugins.LineExtractor;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.image.HistogramUtils;
import br.com.fatec.mc.udct.utils.image.ImageConverterUtils;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Cut lines in the image with possible texts associated to lines. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 15/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class FindLineTexts implements Command{
	//private Logger logger = LoggerFactory.getLogger(FindLineTexts.class);

	private final int MIN_TEXT_SIZE = 80;
	private final int MAX_GAPS = 10;
	private final String TEMP_DIR = "./temp";

	/** 
	 * Finds texts associated to lines
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@Override
	public boolean execute(Context context) throws Exception {
		Image image = (Image)context.get(UseCaseContext.IMAGE);
		List<BufferedImage> cuttedLines = cutLines(image.getImage());
		//List<Image> imageLines = adjustLines(cuttedLines);
		adjustLines(cuttedLines);
		return false;
	}

	/** 
	 * Crop blank data from cut lines
	 * @param cuttedLines
	 */
	private List<Image> adjustLines(List<BufferedImage> cuttedLines){
		List<Image> txtImages = new ArrayList<Image>();
		for(BufferedImage img : cuttedLines){
			int[] vHist = HistogramUtils.getVerticalHistogram(new Image("",img));
			Integer start = null;
			Integer end = null;
			int gaps = 0;
			for(int i = 0 ; i < vHist.length ; i++){
				int value = vHist[i];

				if(value != 0 && start == null){
					start = i;
				}else if(value == 0 && start != null){
					if(gaps > MAX_GAPS){
						end = i;
						gaps = 0;
					}else{
						gaps++;
					}
				}else{
					gaps = 0;
				}

				if(start != null && end != null){
					if(end - start > MIN_TEXT_SIZE){
						Image image = ImageConverterUtils.cloneImage(img.getSubimage(start, 0, end - start, img.getHeight()));
						txtImages.add(image);
					}
					start = null;
					end = null;
				}	
			}
		}

		return txtImages;
	}

	/** 
	 * Cut lines (parts with possible information)
	 * @param textImage - {@link BufferedImage}
	 * @return {@link List} of {@link BufferedImage}: Lines
	 * @throws IOException 
	 */
	private List<BufferedImage> cutLines(BufferedImage textImage) throws IOException{
		List<BufferedImage> cutLines = new ArrayList<BufferedImage>();
		File outputfile = new File("tempImage.png");
		ImageIO.write(textImage, "png", outputfile);

		File tempDir = new File(TEMP_DIR);
		if(tempDir.exists()){
			delete(tempDir);
		}
		tempDir.mkdirs();

		LineExtractor slicer = new LineExtractor();
		slicer.slice(outputfile, tempDir);

		for(File file : tempDir.listFiles()){
			BufferedImage lineImg = ImageIO.read(file);
			cutLines.add(lineImg);
		}

		delete(tempDir);//Delete the temporary directory and all it's files
		outputfile.delete();


		return cutLines;
	}

	/** 
	 * Delete files/directories recursively
	 * @param file {@link File}: File or directory
	 * @throws IOException
	 */
	private void delete(File file) throws IOException{	 
		if(file.isDirectory()){//If directory is empty delete
			if(file.list().length==0){
				file.delete();
			}else{

				String files[] = file.list();//Get all files
				for (String temp : files) {
					File fileDelete = new File(file, temp);
					delete(fileDelete);//Recursive delete
				}

				if(file.list().length==0){
					file.delete();
				}
			}

		}else{
			file.delete();
		}
	}

}
