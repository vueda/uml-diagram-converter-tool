/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 04/08/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Command that resizes the image if the image are lower or bigger than the 
 * parameters that define the image size. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 04/08/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class ImageResize implements Command {
	private Logger logger = LoggerFactory.getLogger(ImageResize.class);

	private final int MAX_WIDTH = 800;
	private final int MAX_HEIGHT = 600;

	/** 
	 * Resize the image if the image is too small or too big. This will be defined comparing
	 * to predefined parameters.
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {		
		Image image = (Image) context.get(UseCaseContext.IMAGE);
		if(image.getHeight() > MAX_HEIGHT){
			logger.info("Resizing the image.");
			double diff = image.getHeight() - MAX_HEIGHT;
			double h = image.getHeight();
			double percent = (diff / h);
			int width = (int) (image.getWidth() - (image.getWidth() * percent));
			
			BufferedImage resizedImage = new BufferedImage(width, MAX_HEIGHT, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = resizedImage.createGraphics();
			g.drawImage(image.getImage(), 0, 0, width, MAX_HEIGHT, null);
			g.dispose();
			image = new Image(image.getImageName(), resizedImage);
		}else if(image.getWidth() > MAX_WIDTH){
			logger.info("Resizing the image.");
			int diff = image.getWidth() - MAX_WIDTH;
			double percent = diff / image.getWidth();
			int height = (int) (image.getHeight() - (image.getHeight() * percent));
			
			BufferedImage resizedImage = new BufferedImage(MAX_WIDTH, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = resizedImage.createGraphics();
			g.drawImage(image.getImage(), 0, 0, MAX_WIDTH, height, null);
			g.dispose();
			image = new Image(image.getImageName(), resizedImage);
		}

		context.put(UseCaseContext.IMAGE, image);
		return false;
	}

}
