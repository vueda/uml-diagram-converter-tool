/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 31/05/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.strategy.ILineDetectionStrategy;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.geometry.PolarLine;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.image.ImageConverterUtils;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Strategy of line detection. Implements the Hough Transform.<br>
 * Implementation based on: <i>http://vase.essex.ac.uk/software/HoughTransform/index.html</i><br>
 * <br>
 * CLASS VERSIONS: <br>
 * 31/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

@Deprecated
public class HoughTransformLineDetectionStrategy implements ILineDetectionStrategy{

	//Class logger.
	Logger logger = LoggerFactory.getLogger(getClass());

	//Constants used.
	static final int MAX_THETA = 180; //Theta(angle) values of the parameters space.
	static final double THETA_STEP = Math.PI / MAX_THETA; 

	//Algorithm parameters.
	int THRESHOLD = 50; 				//Min value of votes/threshold to be considered a valid line
	int NEIGHBORHOOD_SIZE = 5; 			//Neighborhood size in which will be searched for max values in the accumulator.
	int DASHED_LINE_THRESHOLD = 8; 		//Space between the keys in a dashed line
	int LINE_SPACES_THRESHOLD = 2; 		//Min quantity of keys
	int LINE_POINTS_THRESHOLD = 50; 	//Min quantity of points that composes a line
		
	
	/** 
	 * Detect lines using the Hough Transform strategy
	 * @param diagramImage {@link Image}:Image where the lines will be searched
	 * @return {@link List} of detected {@link Line}
	 * @see br.com.fatec.mc.udct.core.strategy.ILineDetectionStrategy#detectLines(br.com.fatec.mc.udct.domain.image.Image)
	 */
	public List<Line> detectLines(Image diagramImage) {
		Long startTime = System.currentTimeMillis(); //Stores the start of the process in milliseconds.
		logger.info("Starting line recognition.");
		
		//Retrieves the center coordinate of the image.
		float centerX = diagramImage.getImage().getWidth() / 2;
		float centerY = diagramImage.getImage().getHeight() / 2;
		
		//Cache of sin and cos values for theta values. To best performance. 
		double[] sinCache = new double[MAX_THETA];
		double[] cosCache = sinCache.clone();
		for(int i = 0 ; i < MAX_THETA ; i++){
			double realTheta = i * THETA_STEP;
			sinCache[i] = Math.sin(realTheta);
			cosCache[i] = Math.cos(realTheta);
		}
		
		//Extract the interest coordinates from the image	
		diagramImage = ImageConverterUtils.image2BinaryWhiteEdge(diagramImage, 150);
		//diagramImage = EdgeDrawing.edgeDrawing(diagramImage);
		List<Coordinate2D> edgePixels = ImageConverterUtils.image2CoordinatesBlackEdge(diagramImage);
		
		//Detected lines list
		List<PolarLine> detectedLines = new ArrayList<PolarLine>();
		
		//Accumulator
		int houghHeight = (int) (2 * (Math.sqrt(2) * Math.max(diagramImage.getImage().getHeight(), diagramImage.getImage().getWidth())/2));
		double [][] accumulatorArray = new double [MAX_THETA][houghHeight];
		
		for(int i = 0 ; i < edgePixels.size() ; i ++){//Every image pixels
			for(int j = 0 ; j < MAX_THETA ; j++){ //All theta values
				//Distance between line and origin
				int distanceOrigin = (int)(((edgePixels.get(i).getX() - centerX) * cosCache[j]) + 
						((edgePixels.get(i).getY() - centerY) * sinCache[j]));
				
				distanceOrigin += houghHeight / 2;
				
				//If less than 0 or greater than accumulator size ignores
				if(distanceOrigin < 0 || distanceOrigin >= houghHeight)
					continue;
				
				//Increments the accumulator
				accumulatorArray[j][distanceOrigin]++;
			}
		}
				
		//Analysis the accumulator searching for the peaks
		for(int m = 0 ; m < MAX_THETA ; ++m){
			loop:
			for (int r = NEIGHBORHOOD_SIZE ; r < houghHeight - NEIGHBORHOOD_SIZE ; r++){
				if(accumulatorArray[m][r] > THRESHOLD){
					int peak = (int) accumulatorArray[m][r];

					for(int dx = -NEIGHBORHOOD_SIZE ; dx <= NEIGHBORHOOD_SIZE; dx++){
						for(int dy = -NEIGHBORHOOD_SIZE ; dy <= NEIGHBORHOOD_SIZE; dy++){
							int dt = m + dx;
							int dr = (int) (r + dy);
							if(dt < 0) 
								dt = dt + MAX_THETA;
							else if(dt >= MAX_THETA) 
								dt = dt - MAX_THETA;
							if(accumulatorArray[dt][dr] > peak){
								continue loop;
							}
						}
					}
					//The real theta value
					double theta = m * THETA_STEP;
					//Add line
					PolarLine line = new PolarLine();
					line.setDistanceOrigin((double)r);
					line.setOrientation(theta);
								
					//Removes from image the pixel of this line
					line = (PolarLine) classifyLine(line, diagramImage, edgePixels);
					
					detectedLines.add(line);
				}
			}
		}
		
//		int dashed = 0;
//		int normal = 0;
				
		int acc = 0;
		int div = detectedLines.size();
		for(int j = 0 ; j < detectedLines.size(); j++){
			if(detectedLines.get(j).getLinePoints().size() != 0)
				acc += detectedLines.get(j).getLinePoints().size();
			else
				div--;
		}
		div = div ==0?1:div;
		acc = acc / div;
		acc = (int) (acc * 0.5);

		//Collections.sort(detectedLines);
		for(int i = 0 ; i < detectedLines.size(); i++){
			
			PolarLine l = detectedLines.get(i);
		
			if(detectedLines.get(i).getLinePoints().size() < acc){
				detectedLines.remove(l);
				i--;
			}
			else if(i < detectedLines.size() - 1 && 
					detectedLines.get(i + 1).getDistanceOrigin() -  l.getDistanceOrigin()   < 15 ){
				double majorOri = l.getOrientation();
				double minorOri = l.getOrientation();
				if(majorOri < detectedLines.get(i + 1).getOrientation())
					majorOri = detectedLines.get(i + 1).getOrientation();
				else
					minorOri = detectedLines.get(i + 1).getOrientation();
				if(majorOri - minorOri < 0.08){
					int minor = l.getLinePoints().size();
					if(detectedLines.get(i + 1).getLinePoints().size() > minor)
						detectedLines.remove(l);
					else
						detectedLines.remove(i + 1);
					i--;
				}
			}	
		}
		
		logger.info("Processing Time: {} minutes", (new SimpleDateFormat("mm:ss").format(new Date(System.currentTimeMillis() - startTime))));
		logger.info(" Detected Lines: {}", detectedLines.size());		
		return new ArrayList<Line>(detectedLines);
	}
	
	private Line classifyLine(Line line, Image diagramImage, List<Coordinate2D> edgePixels){
		//Image center		
		PolarLine pLine = (PolarLine) line;
		float centerX = diagramImage.getImage().getWidth() / 2;
		float centerY = diagramImage.getImage().getHeight() / 2;
		int houghHeight = (int) (2 * (Math.sqrt(2) * Math.max(diagramImage.getImage().getHeight(), diagramImage.getImage().getWidth())/2));

		//Dashed and normal lines

		double tSin = Math.sin(pLine.getOrientation());
		double tCos = Math.cos(pLine.getOrientation());

		List<Coordinate2D> linePoints = new ArrayList<Coordinate2D>(); //Points that compose the line
		if(pLine.getOrientation() < Math.PI * 0.25 || pLine.getOrientation() > Math.PI * 0.75){
			for(int y = 0 ; y < diagramImage.getImage().getHeight() ; y++){
				int x = (int) ((((pLine.getDistanceOrigin() - houghHeight /2) - ((y - centerY)*tSin))/ tCos) + centerX);
				if(x < diagramImage.getImage().getWidth() && x >= 0){
					if(diagramImage.getR(x, y) == 0 && diagramImage.getG(x, y) == 0 && diagramImage.getB(x, y) == 0 
							&& !linePoints.contains(new Coordinate2D(x, y)))
						linePoints.add(new Coordinate2D(x, y));
					
						linePoints = checkPointsInNeighbourhood(linePoints, diagramImage, x, y);

				}
			}
		}else{
			for(int x = 0 ; x < diagramImage.getImage().getWidth() ; x++){
				int y = (int) ((((pLine.getDistanceOrigin() - houghHeight / 2) - ((x - centerX)*tCos))/ tSin) + centerY);
				if(y < diagramImage.getImage().getHeight() && y >= 0){
					if(diagramImage.getR(x, y) == 0 && diagramImage.getG(x, y) == 0 && diagramImage.getB(x, y) == 0
							&& !linePoints.contains(new Coordinate2D(x, y)))
						linePoints.add(new Coordinate2D(x, y));

					linePoints = checkPointsInNeighbourhood(linePoints, diagramImage, x, y);
				}
			}
		}
		//Collections.sort(linePoints);
		@SuppressWarnings("unused")
		int lineSpaces = 0;
		int size = (int) Math.floor(linePoints.size() * 0.9);
		for(int i = 0 ; i < size ; i++){ 			
			int distance = linePoints.get(i).distance2Coordinate(linePoints.get(i + 1)).intValue(); 
			if(distance > DASHED_LINE_THRESHOLD && distance < DASHED_LINE_THRESHOLD * 3.5)
				lineSpaces ++; 			
		}
				
//		if(lineSpaces > LINE_SPACES_THRESHOLD && lineSpaces < 20)
//			line.setRelationship("DASHED");
//
//		else
//			line.setRelationship("NORMAL");

		for (Coordinate2D point: linePoints){
			diagramImage.setRGB(point.getX(), point.getY(), 255, 255, 255);
		}
		line.setLinePoints(linePoints);

		return line;
	}
	
	/**
	 * Verifies the neighborhood for points that compose the line
	 * @param linePoints Points that compose the line
	 * @param diagramImage Image
	 * @param x (X position)
	 * @param y (Y position)
	 * @return List with points of the line
	 */
	private List<Coordinate2D> checkPointsInNeighbourhood(List<Coordinate2D> linePoints, Image diagramImage, int x, int y){
		int width = diagramImage.getImage().getWidth();
		int height = diagramImage.getImage().getHeight();

		if((x + 1 < width && x + 1 >= 0) && (y < height && y >= 0) &&
				diagramImage.getB(x + 1, y) == 0 && !linePoints.contains(new Coordinate2D(x + 1 , y)))
			linePoints.add(new Coordinate2D(x + 1, y));	
		if((x - 1 < width && x - 1 >= 0) && (y < height && y >= 0) &&
				diagramImage.getB(x - 1, y) == 0 && !linePoints.contains(new Coordinate2D(x - 1 , y)))
			linePoints.add(new Coordinate2D(x - 1, y));	
		if((x < width && x >= 0) && (y + 1 < height && y + 1 >= 0) &&
				diagramImage.getB(x, y + 1) == 0 && !linePoints.contains(new Coordinate2D(x , y + 1)))
			linePoints.add(new Coordinate2D(x, y + 1));	
		if((x < width && x >= 0) && (y - 1 < height && y - 1 >= 0) &&
				diagramImage.getB(x, y - 1) == 0 && !linePoints.contains(new Coordinate2D(x , y - 1)))
			linePoints.add(new Coordinate2D(x, y - 1));
		if((x + 1 < width && x + 1 >= 0) && (y + 1 < height && y + 1 >= 0) &&
				diagramImage.getB(x + 1, y + 1) == 0 && !linePoints.contains(new Coordinate2D(x + 1 , y + 1)))
			linePoints.add(new Coordinate2D(x + 1, y + 1));	
		if((x - 1 < width && x - 1 >= 0) && (y - 1 < height && y - 1 >= 0) && 
				diagramImage.getB(x - 1, y - 1) == 0 && !linePoints.contains(new Coordinate2D(x - 1 , y - 1)))	
			linePoints.add(new Coordinate2D(x - 1, y - 1));	
		if((x + 1 < width && x + 1 >= 0) && (y - 1 < height && y - 1 >= 0) &&
				diagramImage.getB(x + 1, y - 1) == 0 && !linePoints.contains(new Coordinate2D(x + 1 , y - 1)))
			linePoints.add(new Coordinate2D(x + 1, y - 1));	
		if((x - 1 < width && x - 1 >= 0) && (y + 1 < height && y + 1 >= 0) &&
				diagramImage.getB(x - 1, y + 1) == 0 && !linePoints.contains(new Coordinate2D(x - 1 , y + 1)))
			linePoints.add(new Coordinate2D(x - 1, y + 1));
		
		if((x + 2 < width && x + 2 >= 0) && (y < height && y >= 0) &&
				diagramImage.getB(x + 2, y) == 0 && !linePoints.contains(new Coordinate2D(x + 2 , y)))
			linePoints.add(new Coordinate2D(x + 2, y));	
		if((x - 2 < width && x - 2 >= 0) && (y < height && y >= 0) &&
				diagramImage.getB(x - 2, y) == 0 && !linePoints.contains(new Coordinate2D(x - 2 , y)))
			linePoints.add(new Coordinate2D(x - 2, y));	
		if((x < width && x >= 0) && (y + 2 < height && y + 2 >= 0) &&
				diagramImage.getB(x, y + 2) == 0 && !linePoints.contains(new Coordinate2D(x , y + 2)))
			linePoints.add(new Coordinate2D(x, y + 2));	
		if((x < width && x >= 0) && (y - 2 < height && y - 2 >= 0) &&
				diagramImage.getB(x, y - 2) == 0 && !linePoints.contains(new Coordinate2D(x , y - 2)))
			linePoints.add(new Coordinate2D(x, y - 2));	
		if((x + 2 < width && x + 2 >= 0) && (y + 2 < height && y + 2 >= 0) &&
				diagramImage.getB(x + 2, y + 2) == 0 && !linePoints.contains(new Coordinate2D(x + 2 , y + 2)))
			linePoints.add(new Coordinate2D(x + 2, y + 2));	
		if((x - 2 < width && x - 2 >= 0) && (y - 2 < height && y - 2 >= 0) && 
				diagramImage.getB(x - 2, y - 2) == 0 && !linePoints.contains(new Coordinate2D(x - 2 , y - 2)))	
			linePoints.add(new Coordinate2D(x - 2, y - 2));	
		if((x + 2 < width && x + 2 >= 0) && (y - 2 < height && y - 2 >= 0) &&
				diagramImage.getB(x + 2, y - 2) == 0 && !linePoints.contains(new Coordinate2D(x + 2 , y - 2)))
			linePoints.add(new Coordinate2D(x + 2, y - 2));	
		if((x - 2 < width && x - 2 >= 0) && (y + 2 < height && y + 2 >= 0) &&
				diagramImage.getB(x - 2, y + 2) == 0 && !linePoints.contains(new Coordinate2D(x - 2 , y + 2)))
			linePoints.add(new Coordinate2D(x - 2, y + 2));
		
		return linePoints;
	}

	@Override
	public List<Line> detectLines(List<List<Coordinate2D>> listSegments,
			Image diagramImage) {
		logger.info("NOT IMPLEMENTED BY THIS STRATEGY!!!");
		return null;
	}

}
