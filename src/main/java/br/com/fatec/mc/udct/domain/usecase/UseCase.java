/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 25/08/2013<br>
 */

package br.com.fatec.mc.udct.domain.usecase;

import java.util.ArrayList;
import java.util.List;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Class that represents a usecase in a usecase diagram. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 25/08/2013 - @author Vinícius Oliveira Ueda<br>
 *<br>
 *<br>
 */

public class UseCase extends UseCaseEntity{
	private String description;
	private List<Relationship> generalizations = new ArrayList<Relationship>();
	private List<Relationship> includeCases = new ArrayList<Relationship>();
	private List<Relationship> extendsCases = new ArrayList<Relationship>();
	private List<Relationship> extendedByCases = new ArrayList<Relationship>();
	
	/** 
	 * The description related to this usecase
	 * @return {@link String}: Usecase description
	 */
	public String getDescription() {
		return description;
	}

	/** 
	 * Sets the usecase description
	 * @param description - {@link String}:Usecase description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/** 
	 * Generalization associations
	 * @return {@link List} of {@link Relationship}: List of generalizations
	 */
	public List<Relationship> getGeneralizations() {
		return generalizations;
	}

	/** 
	 * Sets generalizations of this UseCase
	 * @param generalizations - {@link List} of {@link Relationship}
	 */
	public void setGeneralizations(List<Relationship> generalizations) {
		this.generalizations = generalizations;
	}
	
	/** 
	 * Indicates if this UseCase specifies other UseCases
	 * @return <code>true</code> if specifies or <code>false</code> if not
	 */
	public boolean hasParent(){
		return generalizations.isEmpty()?false:true;
	}

	/** 
	 * Include associations related to this {@link UseCase}
	 * @return {@link List} of {@link Relationship} of INCLUDE type associated
	 */
	public List<Relationship> getIncludeCases() {
		return includeCases;
	}

	public void setIncludeCases(List<Relationship> includeCases) {
		this.includeCases = includeCases;
	}

	/** 
	 * Extends associations related to this {@link UseCase}. This usecase extends another case
	 * @return {@link List} of {@link Relationship} of EXTENDS type associated
	 */
	public List<Relationship> getExtendsCases() {
		return extendsCases;
	}

	public void setExtendsCases(List<Relationship> extendsCases) {
		this.extendsCases = extendsCases;
	}

	/** 
	 * Extends associations related to this {@link UseCase}. This use case is extended by another case
	 * @return {@link List} of {@link Relationship} of EXTENDS type associated
	 */
	public List<Relationship> getExtendedByCases() {
		return extendedByCases;
	}

	public void setExtendedByCases(List<Relationship> extendedByCases) {
		this.extendedByCases = extendedByCases;
	}	
	
}
