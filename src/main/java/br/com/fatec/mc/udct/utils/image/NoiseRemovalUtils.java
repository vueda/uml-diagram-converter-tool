/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 08/09/2013<br>
 */

package br.com.fatec.mc.udct.utils.image;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Utility class that contains methods with techniques to remove noise
 * from the given image. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 08/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class NoiseRemovalUtils {
	
	/** 
	 * Applies the median filter to remove image noise
	 * @param processedImage - {@link Image}
	 */
	public static Image medianMaskFilter(Image image, int maskWidth, int maskHeight){
		Image processedImage = new Image(image.getImage().getWidth(), image.getImage().getHeight());
		int maskLines = maskWidth;
		int maskColumns = maskHeight;
		//Indicates how many columns exists between the central pixel and the more
		//external pixel from the mask
        int deltaX = (maskColumns-1)/2;
        //Indicates how many rows exists between the central pixel and the more
        //external pixel from the mask
        int deltaY = (maskLines-1)/2;
        //Iterates over all image pixels
		for(int i = 0 ; i < image.getImage().getWidth() ; i++){
			for(int j = 0 ; j < image.getImage().getHeight()  ; j++){
                //List that stores the RGB values
                List<Integer> rValues = new ArrayList<Integer>();
                List<Integer> gValues = new ArrayList<Integer>();
                List<Integer> bValues = new ArrayList<Integer>();
                //Goes over all mask pixels
                //k = i - deltaX --> Verifies if the mask is over the image columns
                //l = j - deltaY --> Verifies if the mask is over the image rows
                for (int k = i-deltaX, mx=0; k <= i+deltaX && mx < maskLines; k++, mx++) {
                    for (int l = j-deltaY, my=0; l <= j+deltaY && my < maskColumns; l++, my++) {
                        if(k>=0 && k<image.getImage().getWidth()  && l>=0 && l<image.getImage().getHeight()) {
                            rValues.add(image.getR(k, l));
                            gValues.add(image.getG(k, l));
                            bValues.add(image.getB(k, l));
                        }
                    }
                }
                
                //Convert to array
                Integer[] rArrange = rValues.toArray(new Integer[rValues.size()]);
                Integer[] gArrange = gValues.toArray(new Integer[gValues.size()]);
                Integer[] bArrange = bValues.toArray(new Integer[bValues.size()]);
                //Sort
                Arrays.sort(rArrange);
                Arrays.sort(gArrange);
                Arrays.sort(bArrange);
                
                int medianR = rArrange.length % 2 != 0 ? //Verifies if has odd number of elements
                        rArrange[(rArrange.length-1)/2] ://Yes: Gets central element 
                        (rArrange[rArrange.length/2]+rArrange[(rArrange.length/2)-1])/2;//No: Get the median between the two central elements
                int medianG = gArrange.length % 2 != 0 ? 
                        gArrange[(gArrange.length-1)/2] : 
                        (gArrange[gArrange.length/2]+gArrange[(gArrange.length/2)-1])/2;
                int medianB = bArrange.length % 2 != 0 ?
                        bArrange[(bArrange.length-1)/2] :
                        (bArrange[bArrange.length/2]+bArrange[(bArrange.length/2)-1])/2;
                processedImage.setRGB(i, j, medianR, medianG, medianB);
             
            }
        }
		return processedImage;
	}


}
