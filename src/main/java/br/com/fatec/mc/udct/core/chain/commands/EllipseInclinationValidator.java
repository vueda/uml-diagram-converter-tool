/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 21/09/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import web.unbc.ca.Ellipse;
import web.unbc.ca.Geometry;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Validates the maximum inclination of the ellipse. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 21/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class EllipseInclinationValidator implements Command {
	private Logger logger = LoggerFactory.getLogger(EllipseInclinationValidator.class);

	/**
	 * Ellipse max inclination
	 */
	private final double MAX_ANGLE = 15;
	
	/** 
	 * Removes ellipse that have an inclination higher than the parameter
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> ellipses = circlesAndEllipses.get(Ellipse.class.getSimpleName());
		List<Geometry> ellipses2Remove = new ArrayList<Geometry>();
		
		logger.info("Validating the inclination of the ellipses. The max inclination is " + MAX_ANGLE + " degrees.");
		logger.info("Number of ellipses before process: " + ellipses.size());
		for(Geometry form : ellipses){
			Ellipse e = (web.unbc.ca.Ellipse) form;
			Line2D.Float axis = null;
			if(Ellipse.getLength(e.getAxisA()) > Ellipse.getLength(e.getAxisB())){
				axis = e.getAxisA();
			}else{
				axis = e.getAxisB();
			}
			double alfa = Math.atan2(axis.getY1() - axis.getY2(), axis.getX1() - axis.getX2());
			double angle = Math.abs(Math.toDegrees(Math.atan(alfa)));
			if(angle > MAX_ANGLE){
				ellipses2Remove.add(e);
			}
		}
		
		ellipses.removeAll(ellipses2Remove);
		logger.info("Number of ellipses after process: " + ellipses.size());
		return false;
	}

}
