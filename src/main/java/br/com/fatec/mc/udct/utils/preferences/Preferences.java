/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 30/05/2013<br>
 */

package br.com.fatec.mc.udct.utils.preferences;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Class with the application preferences keys. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 30/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class Preferences {
	public static final String DIRECTORY = "DIRECTORY";
	public static final String NEURAL_NETWORK = "NEURAL_NETWORK";
	public static final String XMI_OUTPUT = "XMI_OUTPUT";
	public static final String ARGOUML_XMI = "ARGOUML_XMI";

}
