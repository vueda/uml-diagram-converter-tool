/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 02/06/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.List;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.core.strategy.IEllipseDetectionStrategy;
import br.com.fatec.mc.udct.core.strategy.context.EllipseDetectionContext;
import br.com.fatec.mc.udct.domain.geometry.GeometryEntity;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Detect ellipses based on the detected edges in the context. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 02/06/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class EllipseDetection implements Command {
	private Logger logger = LoggerFactory.getLogger(EllipseDetection.class);

	private EllipseDetectionContext edContext;
	
	/** 
	 * Detect ellipses using the edges in the context
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {		
		IEllipseDetectionStrategy strategy = (IEllipseDetectionStrategy) context.get(UseCaseContext.ELLIPSE_DETECTION_STRATEGY);
		edContext = new EllipseDetectionContext(strategy);
		
		logger.info("Detects circles and ellipses based on the strategy: " + strategy.getClass().getSimpleName());

		List<List<? extends GeometryEntity>> segments = (List<List<? extends GeometryEntity>>) context.get(UseCaseContext.SEGMENTS);
		edContext.setImage(null);
		edContext.setImageData(segments);
		edContext.executeDetection();
		context.put(UseCaseContext.CIRCLES_ELLIPSES, edContext.getResult());
		return false;
	}

}
