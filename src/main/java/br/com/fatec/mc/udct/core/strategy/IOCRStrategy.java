/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 15/09/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy;

import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Interface for optical character recognition strategies. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 15/09/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br> 
 */

public interface IOCRStrategy {
	String doRecognition(Image image);
}
