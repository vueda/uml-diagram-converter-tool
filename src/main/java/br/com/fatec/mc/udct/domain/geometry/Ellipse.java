/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 29/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.geometry;


/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Domain class that represents an ellipse. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 29/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class Ellipse extends GeometryEntity{
	private Double orientation;
	private Double minorAxis;
	private Double majorAxis;
	private Coordinate2D center;
	
	/** 
	 * Default constructor
	 */
	public Ellipse(){}
	
	/** 
	 * Constructor that receives the two coordinates of the ellipse major axis<br>
	 * It calculates the orientation, the center and the major axis size. 
	 * @param c1 {@link Coordinate2D}: Major axis extremity
	 * @param c2 {@link Coordinate2D}: Major axis extremity
	 */
	public Ellipse(Coordinate2D c1, Coordinate2D c2){
		this.center = new Coordinate2D();
		this.center.setX((c1.getX() + c2.getX())/2);
		this.center.setY((c1.getY() + c2.getY())/2);
		
		this.majorAxis = Math.sqrt(
				Math.pow((c1.getX() - c2.getX()),2) + 
				Math.pow((c1.getY() - c2.getY()),2)
				);
		
		this.orientation = Math.atan((c2.getY() - c1.getY()) /
				(c2.getX() - c1.getX()));
	}

	/** 
	 * Ellipse orientation
	 * @return {@link Double}: The orientation of the ellipse
	 */
	public Double getOrientation() {
		return orientation;
	}

	/** 
	 * Ellipse minor axis
	 * @return {@link Double}:The minor axis size of the ellipse
	 */
	public Double getMinorAxis() {
		return minorAxis;
	}

	/** 
	 * Ellipse major axis
	 * @return {@link Double}:The major axis size of the ellipse
	 */
	public Double getMajorAxis() {
		return majorAxis;
	}

	/** 
	 * Ellipse center
	 * @return {@link Coordinate2D}:The center coordinate of the ellipse
	 */
	public Coordinate2D getCenter() {
		return center;
	}
	
	public void setOrientation(Double orientation) {
		this.orientation = orientation;
	}
	
	public void setMinorAxis(Double minorAxis) {
		this.minorAxis = minorAxis;
	}
	
	public void setMajorAxis(Double majorAxis) {
		this.majorAxis = majorAxis;
	}
	
	public void setCenter(Coordinate2D center) {
		this.center = center;
	}
	
	
}
