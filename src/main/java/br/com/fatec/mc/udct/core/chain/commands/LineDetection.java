/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 10/08/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.core.strategy.ILineDetectionStrategy;
import br.com.fatec.mc.udct.core.strategy.context.LineDetectionContext;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.image.Image;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Convert the segments into lines with the selected strategy. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 10/08/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class LineDetection implements Command{
	private Logger logger = LoggerFactory.getLogger(LineDetection.class);

	private LineDetectionContext ldContext;
	
	/** 
	 * Converts the segments into lines using the selected strategy
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {		
		ILineDetectionStrategy strategy = (ILineDetectionStrategy) context.get(UseCaseContext.LINE_DETECTION_STRATEGY);
		ldContext = new LineDetectionContext(strategy);
		Image image = (Image) context.get(UseCaseContext.IMAGE);
		List<List<Coordinate2D>> segments = (List<List<Coordinate2D>>) context.get(UseCaseContext.SEGMENTS);
		List<List<Coordinate2D>> target = new ArrayList<List<Coordinate2D>>();
		cloneSegments(segments, target);
		ldContext.setImage(image);
		ldContext.setImageSegments(target);
		
		logger.info("Convert the detected segments into lines with the " + strategy.getClass().getSimpleName() + " strategy.");
		List<Line> lines = ldContext.executeDetection();
		context.put(UseCaseContext.LINES, lines);
		return false;
	}
	
	/** 
	 * Clones the given segment into the target parameter
	 * @param source - Segments to be cloned
	 * @param target - Target to clone the segments
	 */
	private void cloneSegments(List<List<Coordinate2D>> source, List<List<Coordinate2D>> target){
		for(List<Coordinate2D> sourceSeg: source){
			List<Coordinate2D> targetSeg = new ArrayList<Coordinate2D>();
			for(Coordinate2D c: sourceSeg){
				targetSeg.add(new Coordinate2D(c.getX(), c.getY()));
			}
			target.add(targetSeg);
		}
	}

}
