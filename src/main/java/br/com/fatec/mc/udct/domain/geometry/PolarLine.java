/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 30/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.geometry;


/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Domain class that represents a Line in the Polar form. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 30/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class PolarLine extends Line{
	private Double distanceOrigin;
	private Double orientation;
	
	/** 
	 * Distance from the line to the origin
	 * @return {@link Double}: Distance between the line and the origin
	 */
	public Double getDistanceOrigin() {
		return distanceOrigin;
	}
	
	/** 
	 * Orientation of the line
	 * @return {@link Double}: Line orientation
	 */
	public Double getOrientation() {
		return orientation;
	}
	
	public void setDistanceOrigin(Double distanceOrigin) {
		this.distanceOrigin = distanceOrigin;
	}
	
	public void setOrientation(Double orientation) {
		this.orientation = orientation;
	}	
}
