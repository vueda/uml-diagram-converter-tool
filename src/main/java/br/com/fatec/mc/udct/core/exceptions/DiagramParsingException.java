/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 15/11/2013<br>
 */

package br.com.fatec.mc.udct.core.exceptions;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Exception thrown when an error occurs while creating the diagram info. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 15/11/2013 - @author Vinícius Oliveira Ueda<br>
 *<br>
 *<br>
 */

@SuppressWarnings("serial")
public class DiagramParsingException extends Exception{
	
	/** 
	 * Class constructor
	 */
	public DiagramParsingException(Exception ex) {
		initCause(ex);
	}
}
