/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 25/08/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.core.strategy.IXMIParsingStrategy;
import br.com.fatec.mc.udct.core.strategy.context.XMIParsingContext;
import br.com.fatec.mc.udct.domain.usecase.UseCaseDiagram;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Command that uses parses the {@link UseCaseDiagram} to a XMI file
 * using the provided strategy. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 25/08/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class DiagramXMIParser implements Command {
	private Logger logger = LoggerFactory.getLogger(DiagramXMIParser.class);

	private XMIParsingContext parseContext;
	
	/** 
	 * Parses the {@link UseCaseDiagram} to a XMI file
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		UseCaseDiagram diagram = (UseCaseDiagram) context.get(UseCaseContext.DIAGRAM);
		Class<IXMIParsingStrategy> clazz = (Class<IXMIParsingStrategy>) context.get(UseCaseContext.XMI_PARSING_STRATEGY);
		parseContext = new XMIParsingContext(clazz.newInstance());
		
		logger.info("Parsing diagram model to xmi file using the " + clazz.getSimpleName() + " strategy.");
		parseContext.doParse(diagram);
		return false;
	}

}
