/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 04/08/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Ellipse;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.adapter.CoordinateAdapter;
import br.com.fatec.mc.udct.utils.image.HistogramUtils;
import br.com.fatec.mc.udct.utils.image.ImageConverterUtils;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Removes from the image the pixels of already processed ellipses.
 * This helps to keep the image as clear as possible for the next processes <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 04/08/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class ClearEllipsesFromImage implements Command {

	private Logger logger = LoggerFactory.getLogger(ClearEllipsesFromImage.class);

	private final int CLEAR_AREA = 25;
	private final int CLEAR_POINT = 15;

	/** 
	 * Clear the image removing the already detected ellipses from the source image
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> ellipses = circlesAndEllipses.get(Ellipse.class.getSimpleName());
		Image image = (Image)context.get(UseCaseContext.IMAGE);
		Graphics2D graphic = (Graphics2D) image.getImage().getGraphics();
		graphic.setColor(Color.WHITE);
		Map<Geometry, List<Image>> texts = (Map<Geometry, List<Image>>) context.get(UseCaseContext.TEXTS);

		logger.info("Extracting texts inside the ellipses and clearing the area.");
		for(Geometry g:ellipses){ 		
			Ellipse e = (Ellipse) g;	
			List<Image> ellipseTexts = extractText(e,image);
			texts.put(e, ellipseTexts);
			double alfa = Math.atan2(e.getAxisB().getY1() - e.getAxisB().getY2(),e.getAxisB().getX1() -e.getAxisB().getX2());

			graphic.rotate(alfa, e.getCenter().getX(), e.getCenter().getY());

			graphic.fillArc(
					(int)((e.getCenter().getX().intValue() - Ellipse.getLength(e.getAxisB()) / 2) - CLEAR_POINT), 
					(int)((e.getCenter().getY().intValue() - Ellipse.getLength(e.getAxisA())/2) - CLEAR_POINT), 
					(int)(Ellipse.getLength(e.getAxisB()) + CLEAR_AREA), 
					(int)(Ellipse.getLength(e.getAxisA()) + CLEAR_AREA),
					0, 360);

			graphic.rotate(-alfa, e.getCenter().getX(), e.getCenter().getY());
		}
		return false;
	}

	/** 
	 * Extract a text associated with this element
	 * @param ellipse - {@link Ellipse}
	 */
	private List<Image> extractText(Ellipse ellipse, Image image){
		Coordinate2D center = CoordinateAdapter.adapt2Coordinate2D(ellipse.getCenter());
		float majorAxis = 0;
		float minorAxis = 0;
		if(Ellipse.getLength(ellipse.getAxisA()) > Ellipse.getLength(ellipse.getAxisB())){
			majorAxis = Float.valueOf(Ellipse.getLength(ellipse.getAxisA())).equals(Float.NaN)? 10:Ellipse.getLength(ellipse.getAxisA());
			minorAxis = Float.valueOf(Ellipse.getLength(ellipse.getAxisB())).equals(Float.NaN)? 10:Ellipse.getLength(ellipse.getAxisB());
		}else{
			minorAxis = Float.valueOf(Ellipse.getLength(ellipse.getAxisA())).equals(Float.NaN)? 10:Ellipse.getLength(ellipse.getAxisA());
			majorAxis = Float.valueOf(Ellipse.getLength(ellipse.getAxisB())).equals(Float.NaN)? 10:Ellipse.getLength(ellipse.getAxisB());
		}

		int retangleMajorSide = (int) (Math.sqrt(2) * (majorAxis/2));
		int retangleMinorSide = (int) (Math.sqrt(2) * (minorAxis/2));
		int retX = (int) (center.getX() - (retangleMajorSide/2));
		int retY = (int) (center.getY() - (retangleMinorSide/2));	

		//This is the area inside the ellipse
		BufferedImage textArea = image.getImage().getSubimage(retX, retY ,retangleMajorSide, retangleMinorSide);

		Image insideEllipseText = ImageConverterUtils.cloneImage(textArea);

		List<Image> textImages = new ArrayList<Image>();
		textImages.add(insideEllipseText);

		if(!cropTextFromImage(insideEllipseText, textImages)){
			//If no text is found inside the ellipse the area under the ellipse needs to be evaluated as well

			//This is the area under the ellipse
			int topLeftX = (int) ((center.getX() - (majorAxis/2)) > 0 ? (center.getX() - (majorAxis/2)): 0);
			int topLeftY = (int) (center.getY() + (minorAxis/2));
			int cropWidth = (int) (topLeftX + majorAxis > image.getWidth()?image.getWidth() - topLeftX: majorAxis);
			int cropHeight = (int) (topLeftY + (minorAxis) > image.getHeight()? image.getHeight() - topLeftY: (minorAxis/2));
			BufferedImage textUnder = image.getImage().getSubimage(topLeftX, topLeftY,cropWidth, cropHeight);

			Image underEllipseText = new Image("", textUnder);
			cropTextFromImage(underEllipseText, textImages);		
		}

		return textImages;
	}
	private boolean cropTextFromImage(Image textArea, List<Image> textImages){
		boolean result = false;
		int[] horzHist = HistogramUtils.getHorizontalHistogram(textArea);

		Integer textStart = null;
		Integer textEnd = null;

		for(int i = 0; i < horzHist.length; i++){
			int el = horzHist[i];
			if(el != 0){//If the value is different than zero it's the start of a text area
				textStart = i;
				for(int j = i+1 ; j < horzHist.length;j++){
					int nextEl = horzHist[j];
					if(nextEl == 0){
						textEnd = j - 1;
						if(textEnd - textStart < 10){//If it's a small area, discard
							textStart = textEnd = null;
							break;
						}
						BufferedImage text = textArea.getImage().getSubimage(0, textStart, textArea.getWidth(), textEnd-textStart);
						Image textImg = ImageConverterUtils.cloneImage(text);
						textImages.add(textImg);
						result = true;
						textStart = textEnd = null;
						i = j;
						break;
					}
				}
				if(textStart != null && textEnd == null){
					textEnd = horzHist.length-1;
					if(textEnd - textStart < 3){//If it's a small area, discard
						textStart = textEnd = null;
						break;
					}
					BufferedImage text = textArea.getImage().getSubimage(0, textStart, textArea.getWidth(), textEnd - textStart);
					Image textImg = ImageConverterUtils.cloneImage(text);
					result = true;
					textImages.add(textImg);	
				}
			}
		}

		return result;
	}

}
