/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.usecase;

import java.util.ArrayList;
import java.util.List;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Domain class that represents an Actor in a UseCase diagram. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 28/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */
public class Actor extends UseCaseEntity{
	private String name;
	private List<Relationship> generalizations = new ArrayList<Relationship>();

	/** 
	 * Actor's name
	 * @return {@link String}:The name of this actor
	 */
	public String getName() {
		return name;
	}

	/** 
	 * Sets the actor's name
	 * @param name - {@link String}:Actor's name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/** 
	 * Generalization associations
	 * @return {@link List} of {@link Relationship}: List of generalizations
	 */
	public List<Relationship> getGeneralizations() {
		return generalizations;
	}

	/** 
	 * Sets generalizations of this actor
	 * @param generalizations - {@link List} of {@link Relationship}
	 */
	public void setGeneralizations(List<Relationship> generalizations) {
		this.generalizations = generalizations;
	}
	
	/** 
	 * Indicates if this Actor specifies other Actors
	 * @return <code>true</code> if specifies or <code>false</code> if not
	 */
	public boolean hasParent(){
		return generalizations.isEmpty()?false:true;
	}
	
}
