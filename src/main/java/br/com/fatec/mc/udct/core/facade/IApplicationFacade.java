/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/05/2013<br>
 */

package br.com.fatec.mc.udct.core.facade;

import br.com.fatec.mc.udct.core.exceptions.DiagramParsingException;
import br.com.fatec.mc.udct.core.exceptions.XMIParsingException;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.domain.usecase.UseCaseDiagram;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Interface of the application facade. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 28/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public interface IApplicationFacade {
	UseCaseDiagram manualDetection(Image image) throws DiagramParsingException;
	void generateXMIFile(UseCaseDiagram diagram) throws XMIParsingException;
}
