/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 29/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.geometry;


/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Domain class that represents a coordinate in a 2D plan. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 29/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class Coordinate2D extends GeometryEntity{
	private Integer x;
	private Integer y;
	
	/** 
	 * Default Class constructor
	 */
	public Coordinate2D(){}
	
	/** 
	 * Class constructor
	 * @param x - X value
	 * @param y - Y value
	 */
	public Coordinate2D(Integer x, Integer y){
		this.x = x;
		this.y = y;
	}
	
	/** 
	 * Represents the value of the coordinate related to the X axis
	 * @return {@link Integer}
	 */
	public Integer getX() {
		return x;
	}
	
	/** 
	 * Represents the value of the coordinate related to the Y axis
	 * @return {@link Integer}
	 */
	public Integer getY() {
		return y;
	}
	
	/** 
	 * Calculates the distance between this point and the informed one.
	 * @param c {@link Coordinate2D}: Another coordinate
	 * @return {@link Double}: The distance between the points
	 */
	public Double distance2Coordinate(Coordinate2D c){
		return Math.sqrt( Math.pow(c.getX() - this.x, 2) + Math.pow(c.getY() - this.y, 2));		
	}
	
	/** 
	 * Equals method
	 * @param obj {@link Object}: The coordinate to be compared
	 * @return <code>true</code> if the coordinates are equals or <code>false</code> if the
	 * coordinates aren't the same.
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		Coordinate2D c = (Coordinate2D) obj;
		
		if((c.getX().equals(this.x)) && (c.getY().equals(this.y))){
			return true;
		}else{
			return false;
		}
	}
	
	public void setX(Integer x) {
		this.x = x;
	}
	
	public void setY(Integer y) {
		this.y = y;
	}
	
}
