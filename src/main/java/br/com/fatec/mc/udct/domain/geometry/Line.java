/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 29/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.geometry;

import java.util.ArrayList;
import java.util.List;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Class that represents a Line. It needs to be extended by 
 * Line representation classes. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 29/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public abstract class Line extends GeometryEntity{
	private LineType lineType;
	private List<Coordinate2D> linePoints;
	
	/** 
	 * Points that compose the line
	 * @return {@link List}: List of {@link Coordinate2D} that composes the line.
	 */
	public List<Coordinate2D> getLinePoints() {
		if(linePoints == null)
			linePoints = new ArrayList<Coordinate2D>();
		return linePoints;
	}
	
	public void setLinePoints(List<Coordinate2D> linePoints) {
		this.linePoints = linePoints;
	}
	
	/** 
	 * Get the first point of the line
	 * @return {@link Coordinate2D}: First point of the line
	 */
	public Coordinate2D getFirstPoint(){
		Coordinate2D coord = null;
		for(Coordinate2D c : getLinePoints()){
			if(coord == null){
				coord = c;
			}else if(c.getX() < coord.getX()){
				coord = c;
			}
		}
		return coord;
	}
	
	/** 
	 * Get the last point of the line
	 * @return {@link Coordinate2D}: Last point of the line
	 */
	public Coordinate2D getLastPoint(){
		Coordinate2D coord = null;
		for(Coordinate2D c : getLinePoints()){
			if(coord == null){
				coord = c;
			}else if(c.getX() > coord.getX()){
				coord = c;
			}
		}
		return coord;
	}

	/** 
	 * The line type
	 * @return {@link LineType}
	 */
	public LineType getLineType() {
		return lineType;
	}

	public void setLineType(LineType lineType) {
		this.lineType = lineType;
	}

	
}
