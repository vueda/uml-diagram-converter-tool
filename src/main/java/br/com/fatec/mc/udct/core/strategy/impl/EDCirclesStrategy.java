/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 02/06/2013<br>
 */

package br.com.fatec.mc.udct.core.strategy.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Circle;
import web.unbc.ca.Ellipse;
import web.unbc.ca.EllipseFit;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.strategy.IEllipseDetectionStrategy;
import br.com.fatec.mc.udct.domain.geometry.Coordinate2D;
import br.com.fatec.mc.udct.domain.geometry.GeometryEntity;
import br.com.fatec.mc.udct.domain.geometry.Line;
import br.com.fatec.mc.udct.domain.image.Image;
import br.com.fatec.mc.udct.utils.adapter.CoordinateAdapter;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Strategy of Ellipses and Circles Detection.<br>
 * Implementation based on: <i>http://ceng.anadolu.edu.tr/cv/EDCircles/</i><br>
 * <br>
 * CLASS VERSIONS: <br>
 * 02/06/2013 - @author Vinícius Oliveira Ueda<br>
 *<br>
 *<br>
 */

public class EDCirclesStrategy implements IEllipseDetectionStrategy{
	Logger logger = LoggerFactory.getLogger(EDCirclesStrategy.class);

	@Override
	public List<br.com.fatec.mc.udct.domain.geometry.Ellipse> detectEllipses(Image diagramImage) {
		logger.warn("NOT IMPLEMENTED BY THIS STRATEGY");
		return null;
	}

	/** 
	 * Detect ellipses and circles using the EDCircles strategy
	 * @param imageData Data extracted from the image. Can be a list of {@link Coordinate2D} or a list
	 * of {@link Line}.
	 * @return Map with {@link Circle} and {@link Ellipse} extracted from the image.
	 * @see br.com.fatec.mc.udct.core.strategy.IEllipseDetectionStrategy#detectedEllipsesAndCircles(java.util.List)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, List<Geometry>> detectedEllipsesAndCircles(List<List<? extends GeometryEntity>> imageData) {
		Map<String,List<Geometry>> detectedEntities = new HashMap<String, List<Geometry>>();
		List<Geometry> circles = new ArrayList<>();
		List<Geometry> ellipses = new ArrayList<>();
		detectedEntities.put(Circle.class.getSimpleName(), circles);
		detectedEntities.put(Ellipse.class.getSimpleName(), ellipses);
		
		//Will try to detect circles and ellipses in the segments
		if(!imageData.isEmpty()){
			if(imageData.get(0).get(0) instanceof Coordinate2D){			
				for(List<? extends GeometryEntity> segment : imageData){
					List<Coordinate2D> segm = (List<Coordinate2D>) segment;
					EllipseFit ef = new EllipseFit(new HashSet<>(CoordinateAdapter.adapt2Coordinate(segm)));
					Ellipse ellipse = ef.getEllipse(EllipseFit.FPF);
					if(ellipse != null){
						if(ellipse.isCircle()){
							circles.add(ellipse.getCircle());
						}else{
							ellipses.add(ellipse);
						}
					}
				}
			}else if(imageData.get(0) instanceof Line){
			}
		}
		return detectedEntities;
	}
	
}
