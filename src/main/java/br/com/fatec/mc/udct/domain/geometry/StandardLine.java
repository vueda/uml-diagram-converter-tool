/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 30/05/2013<br>
 */

package br.com.fatec.mc.udct.domain.geometry;


/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Domain class that represents a Line in the standard form. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 30/05/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class StandardLine extends Line{
	private Double angularCoefficient;
	private Double linearCoefficient;
	
	/** 
	 * The angular coefficient of the line
	 * @return {@link Double}: Angular coefficient
	 */
	public Double getAngularCoefficient() {
		return angularCoefficient;
	}
	
	/** 
	 * The linear coefficient of the line
	 * @return {@link Double}: Linear coefficient
	 */
	public Double getLinearCoefficient() {
		return linearCoefficient;
	}
	
	public void setAngularCoefficient(Double angularCoefficient) {
		this.angularCoefficient = angularCoefficient;
	}
	
	public void setLinearCoefficient(Double linearCoefficient) {
		this.linearCoefficient = linearCoefficient;
	}
	
}
