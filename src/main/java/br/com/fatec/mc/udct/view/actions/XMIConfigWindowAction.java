/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 06/10/2013<br>
 */

package br.com.fatec.mc.udct.view.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import br.com.fatec.mc.udct.view.XMIConfigurationDialog;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Open xmi configuration window. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 06/10/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

@SuppressWarnings("serial")
public class XMIConfigWindowAction extends AbstractAction {
	
	/** 
	 * Open the xmi configuration dialog
	 * @param event {@link ActionEvent}
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		XMIConfigurationDialog dialog = new XMIConfigurationDialog();
		dialog.setVisible(true);
	}
	
}
