/*
 * Faculdade de Tecnologia de Mogi das Cruzes<br>
 * TODO Information about software licensing<br> 
 * Creation Date: 28/07/2013<br>
 */

package br.com.fatec.mc.udct.core.chain.commands;

import java.util.List;
import java.util.Map;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import web.unbc.ca.Ellipse;
import web.unbc.ca.Geometry;
import br.com.fatec.mc.udct.core.chain.context.UseCaseContext;

/** 
 * DESCRIPTION <br>
 * ---------------------- <br>
 * Command that validates possible repeated detected ellipses to remove them. <br>
 * <br>
 * CLASS VERSIONS: <br>
 * 28/07/2013 - @author Vinícius Oliveira Ueda <br>
 *<br>
 *<br>
 */

public class RepeatedEllipseValidator implements Command {
	
	private Logger logger = LoggerFactory.getLogger(RepeatedEllipseValidator.class);

	/**
	 * Center distance of repeated ellipses
	 */
	private final double CENTER_DISTANCE = 20;

	/** 
	 * Validates if there are repeated detected ellipses and remove them from the list
	 * @param context {@link Context}: Chain context
	 * @return {@link Boolean}: <code>true</code> if the chain ended or <code>false</code> if the chain continues
	 * @throws Exception
	 * @see org.apache.commons.chain.Command#execute(org.apache.commons.chain.Context)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(Context context) throws Exception {
		Map<String,List<Geometry>> circlesAndEllipses =(Map<String, List<Geometry>>) 
				context.get(UseCaseContext.CIRCLES_ELLIPSES);
		List<Geometry> ellipses = circlesAndEllipses.get(Ellipse.class.getSimpleName());
		
		logger.info("Validating duplicated ellipse.");
		logger.info("Number of ellipses before process: " + ellipses.size());
		for(int i = 0; i < ellipses.size();i++){
			Ellipse ellipse = (Ellipse) ellipses.get(i);
			for(int j = 0 ; j < ellipses.size();j++){
				Ellipse nextEllipse = (Ellipse) ellipses.get(j);
				if(!ellipse.equals(nextEllipse)){//The last element must compare the others as well
					if(ellipse.getCenter().distance2Coordinate(nextEllipse.getCenter()) < CENTER_DISTANCE ||
							isInsideCurrentEllipse(ellipse, nextEllipse)){
						ellipses.remove(j);
						j--;
					}
				}
			}
		}
		logger.info("Number of ellipses after process: " + ellipses.size());

		return false;
	}
	
	/** 
	 * Verifies if the nextEllipse is inside the current ellipse
	 * @param ellipse - Current ellipse
	 * @param nextEllipse - Next ellipse
	 * @return <code>false</code> if it's not contained or <code>true</code> if is inside the ellipse
	 */
	private boolean isInsideCurrentEllipse(Ellipse ellipse, Ellipse nextEllipse){
		
		float axisA = Ellipse.getLength(ellipse.getAxisA());
		float axisB = Ellipse.getLength(ellipse.getAxisB());

		float majorAxis = axisA >= axisB? axisA : axisB;
		float minorAxis =  axisA >= axisB? axisB : axisA; 

		float rightX = ellipse.getCenter().getX()  + (majorAxis/2);
		float leftX = ellipse.getCenter().getX()  - (majorAxis/2);
		
		float upperY = ellipse.getCenter().getY()  - (minorAxis/2);
		float downY = ellipse.getCenter().getY()  + (minorAxis/2);

		if((nextEllipse.getCenter().getX() < rightX && nextEllipse.getCenter().getX() > leftX) &&
				(nextEllipse.getCenter().getY() < downY && nextEllipse.getCenter().getY() > upperY)){
			return true;
		}
		return false;
	}

}
